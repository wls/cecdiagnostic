/*Soubrutines to write parameter set in the shot file (common to RAD, RMC, CEC, RMD)*/
/* Correction: include ATTNIF parameter Lbarrera 23 Nov.2012*/
#include <malloc.h>
#include <stdio.h>
#include <string.h>

#include <inttypes.h>
#include <stdlib.h>
 

#ifdef DDWW8
#include "ansisfh8.h"
#else
#include "ansisfh.h"
#endif

#include "param.h"
#include "libece.h"
#include "util.h"
#include "eceradiometer.h"


#ifdef DDWW8
  int32_t err;
  uint32_t typ;
  uint32_t len;
  uint32_t n;
#else
  int err;
  int typ;
  int len;
  int n;
#endif
char msg[128];


/* write "methods" parameter set */
 int write_methods_sfh (int diaref, 
			  struct RadiometerSettings *pRadSet)
{
  int i,calibnum;
  float zlens_cm, freq_LO_GHz[MAX_IFGROUP], Attnif[MAX_IFGROUP];

  if (!pRadSet) return(0);

  n = MAX_IFGROUP;
  typ=2;
  for(i=0; i<n; ++i)
    freq_LO_GHz[i] = pRadSet->freq_LO[i] *1e-9;

  if ((err = sfhmodpar (diaref, "METHODS", "FREQLO", typ,
			   MAX_IFGROUP, (char *)freq_LO_GHz )))
	{
	sfherror (err, "(METHODS)");
        sprintf(msg, "Failed to write parameter set METHODS FREQLO");
        log_msg(ERROR, msg);
	err=-1;
	}
  
  zlens_cm = pRadSet->zlens * 100;
  typ=2;
  len=1;
  if ((err = sfhmodpar (diaref, "METHODS", "ZLENS", typ,
			   len, (char *) &zlens_cm )))
	{
	sfherror (err, "(METHODS)");
        sprintf(msg, "Failed to write parameter set METHODS ZLENS");
        log_msg(ERROR, msg); 
	err=-1;
	}

  typ=1;
  len=1;
  calibnum=pRadSet->calibsrc;
  if ((err = sfhmodpar (diaref, "METHODS", "CALIBSRC", typ,
			   len, (char *)&calibnum)))
	{
	sfherror (err, "(METHODS)");
        sprintf(msg, "Failed to write parameter set CALIBSRC");
        log_msg(ERROR, msg); 
	err=-1;
	}

  typ=2;
  for(i=0; i<n; ++i)
  Attnif[i] = pRadSet->attnif[i];
  len=MAX_IFGROUP;

  if ((err = sfhmodpar (diaref, "METHODS", "ATTNIF", typ,
			   len, (char *)Attnif )))
	{
	sfherror (err, "(METHODS)");
        sprintf(msg, "Failed to write parameter set METHODS ATTNIF");
        log_msg(ERROR, msg); 
	err=-1;
	}

  

  typ=1;
  len=1;
  if ((err = sfhmodpar (diaref, "METHODS", "IFGROUPS", typ,
			   len, (char *)&n)))
	{
	sfherror (err, "(METHODS)");
        sprintf(msg, "Failed to write parameter set METHODS IFGROUPS");
        log_msg(ERROR, msg); 
	err=-1;
	}

  
  char sideband_c[3];
  typ=6; //6=char
  len=n;
  for(i=0; i<n; ++i)
    {
    sideband_c[i] = pRadSet->sideband[i];  //'L' or 'U';
    if (pRadSet->calchannel[i]==1)sideband_c[i] = 'N'; //all with nocth filter are USB
   }
  if ((err = sfhmodpar (diaref, "METHODS", "SIDEBAND", typ,   
			   n, (char *)&sideband_c)))
	{
	sfherror (err, "(METHODS)");
        sprintf(msg, "Failed to write parameter set METHODS-SIDEBAND");
        log_msg(ERROR, msg); 
	err=-1;
	}
  typ=1;
  if ((err = sfhmodpar (diaref, "METHODS", "WAVEGUID", typ,
			   n, (char *)pRadSet->waveguide)))
	{
	sfherror (err, "(METHODS)");
        sprintf(msg, "Failed to write parameter set METHODS WAVEGUID");
        log_msg(ERROR, msg); 
	err=-1;
	}

  return(0);
} 


/* write "parms-A" parameter set */
int write_channel_settings_sfh(int diaref, 
			       struct RadiometerSettings *pRadSet)
{
  if (!pRadSet) return(0);

  if ((err = sfhmodpar (diaref, "parms-A", "f", 2,
			   MAX_CHAN, (char *)pRadSet->freqrf)))
	{
	sfherror (err, "(parms-A)");
        sprintf(msg, "Failed to write parameter set parms-A f");
        log_msg(ERROR, msg); 
	err=-1;
	}

 if ((err = sfhmodpar (diaref, "parms-A", "df", 2,
			   MAX_CHAN, (char *)pRadSet->bif)))
	{
	sfherror (err, "(parms-A)");
        sprintf(msg, "Failed to write parameter set parms-A df");
        log_msg(ERROR, msg); 
	err=-1;
	}

  if ((err = sfhmodpar (diaref, "parms-A", "Btot", 2,
			   MAX_CHAN, (char *)pRadSet->Btot)))
	{
	sfherror (err, "(parms-A)");
        sprintf(msg, "Failed to write parameter set parms-A Btot");
        log_msg(ERROR, msg); 
	err=-1;
	}

  if ((err = sfhmodpar (diaref, "parms-A", "IFGROUP", 1,
			   MAX_CHAN, (char *)pRadSet->ifgroup)))
	{
	sfherror (err, "(parms-A)");
        sprintf(msg, "Failed to write parameter set parms-A IFGROUP");
        log_msg(ERROR, msg); 
	err=-1;
	}

  if ((err = sfhmodpar (diaref, "parms-A", "gainma2", 2,
			   MAX_CHAN, (char *)pRadSet->gainma2)))
	{
	sfherror (err, "(parms-A)");
        sprintf(msg, "Failed to write parameter set parms-A gainma2");
        log_msg(ERROR, msg); 
	err=-1;
	}

  if ((err = sfhmodpar (diaref, "parms-A", "AVAILABL", 1,
			   MAX_CHAN, (char *)pRadSet->have)))
	{
	sfherror (err, "(parms-A)");
        sprintf(msg, "Failed to write parameter set parms-A AVAILABL");
        log_msg(ERROR, msg); 
	err=-1;
	}
 


  return(0);
} 

