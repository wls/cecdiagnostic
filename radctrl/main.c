/*
       main.c - radctrl main program

       Wolfgang Suttrop,   suttrop@ipp.mpg.de

       08-Jun-2010

       Changes: 
	*include option 'auto' to syncronize with RMC and set gains accorging to BT in automatic opearation
	  (Jun-2012 LBarrera)
        *With option 'automatic' sfh of RMC is disable and no modifications in RMC diagnostic (gains) after shot number distribution is possible

*/

#include <ctype.h>
#include <math.h>
#include <memory.h>
/* #include <setjmp.h> */
/* #include <stdint.h> */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef DDWW8
#include "ddwwansic8.h"
#else
#include "ddwwansic.h"
#endif

#include "libece.h"
#include "util.h"
#include "param.h"
#include "eceradiometer.h"

#include "radctrl.h"



#define LOGSTRLEN	255      /* max. line length in log file */

static  char   *defaultarg[2];
static void init_autoshot(void);  
static void do_autoshot(void);
static void do_autoshotRMC(void);      
#ifdef DDWW8
  uint32_t  shot;
  int32_t  error;
#else
  int shot;
  int error;
#endif

/* ==================== main program ===================== */


//static void init_autoshot(void);


int main (int argc, char **argv)
  {

  defaultarg[0] = "radctrl";
  defaultarg[1] = strdup("cmd");

  init_logging       ();
  init_coilunits     ();
  init_radiometer    ();

  init_rmcsfh        (); 
  init_rmdsf         ();
  init_optgain       ();

  init_radsettings   ();

  //init_rmcsfh        (); //11.01.13 It was set after radsettings!
  //init_optgain       ();
 // init_rmdsf         ();
  
  init_autoshot      ();
  
  
  

  if (argc>1)   prm_main (argc, argv);
  else          prm_main (2, defaultarg);

  leave_coilunits ();

  return(0);
  }  /* main() */






/* ------  run in sync with experiment RMC ------- */

static void do_autoshotRMC (void) 
{
#ifdef DDWW8
  int32_t  error;
  uint32_t shot;
#else
  int error;
  int shot;
#endif
 
  char msg[1024];

   shot = 0; error = 0;
  /* If autoshot set (automatic oparation) set receivers=bt*/
     prm_set("receivers", "bt");   //LBarrera
     sprintf(msg, "Set receivers according to Bt (automatic mode)");
     log_msg(INFO, msg);

 /* set mode=automatic*/
     prm_set("mode", "automatic");   //LBarrera
     sprintf(msg, "Set RMC sfh in automatic mode");
     log_msg(INFO, msg);
  /* check status of MODSFH (RMC): 0 enable (automatic), -1 disable (manual)*/
     
  /*Get the last shot number and get B from miracle: */
   ddlastshotnr_ (&error, &shot);

   sprintf(msg, "Checking the last distributed shot number, %d",shot);
   log_msg(INFO, msg);	
    if (error)
    {
      log_msg(ERROR, "Cannot obtain current shot number - will not write RMC");
      shot = -1;
    }
   set_from_miracle(shot);

    
} /*autoshot RMC*/












/* ------  run in sync with experiment (not wait for RMC)------- */

static void do_autoshot (void) 
{
#ifdef DDWW8
  int32_t  error;
  uint32_t shot;
#else
  int error;
  int shot;
#endif
 
  char msg[1024];

   shot = 0; error = 0;
  /* If autoshot set (automatic oparation) set receivers=bt*/
     prm_set("receivers", "bt");   //LBarrera
     sprintf(msg, "Set receivers according to Bt (automatic mode)");
     log_msg(INFO, msg);

 /* set mode=manual to switch off MODEX in RMC*/
     prm_set("mode", "manual");   //LBarrera
     sprintf(msg, "RMC should start after the radiometer settings");
     log_msg(INFO, msg);
     
     
 /* set mode=manual to switch off MODEX in RMC*/
     /*prm_set("ECRH105", "False");   //LBarrera
     sprintf(msg, "At least one ECRH gyrotron uses 105 GHz");
     log_msg(INFO, msg); */
     
     
 /* set gains=1*/
     prm_set("gains", "1/60:1");   //LBarrera
     sprintf(msg, "RMC gains set to 1");
     log_msg(INFO, msg);
  /* check status of MODSFH (RMC): 0 enable (automatic), -1 disable (manual)*/

 while(1)
    {
 /* wait for shot number distribution */

      shot = 0; error = 0;
      ddwait_ (&error, &shot); 

      if (error) 
      {
        sprintf(msg, "ddwait() returns error %d.", error);
        log_msg(ERROR, msg);
        return; 
      }

      sprintf(msg, "Shotnumber %d distributed", shot);
      log_msg(INFO, msg);

      set_from_miracle (shot);
  
}  /* while(1) */
    
} /*autoshot*/




#define GROUP "AUG"

static void init_autoshot(void)
{
  prm_group  (GROUP, "AUG synchronous operation"); 

  /*prm_define ("autoshot", NULL, NULL, 
  Change autoshot ->auto because there is a limit of character number
  in the editor of the sfh (to make radctrl compatible with RMC)
   */

  prm_define ("auto", NULL, NULL, 
              "wait for AUG shotnumbers (AFTER RMC HAS STARTED), obtain Bt and set standard hardware settings",
              GROUP, do_autoshotRMC); 

  /*prm_define ("NTM", NULL, NULL, 
              "wait for AUG shotnumbers (AFTER RMC HAS STARTED), obtain Bt and set standard hardware settings",
              GROUP, do_autoshotRMC);*/

  prm_define ("automatic", NULL, NULL, 
              "wait for AUG shotnumbers (not wait for RMC), obtain Bt and set standard hardware settings",
              GROUP, do_autoshot);  

}


/* end of main.c */
