/* optgain.c - find optimum gain values 
               for an expected temperature profile

   28-Feb-2012   lbarrera, wls
*/

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "libece.h"
#include "param.h"
#include "util.h"
#include "radctrl.h"


/* ------ from channel calibration factors, calculate 
          requested gain settings  ------------ */

static	void	calc_opt_gains (struct RadiometerSettings *pRadSet)
  {
    char *valstr, msg[256];
    int type, nbadchan = 0, c;
    enum tprofile_t {FLAT,LINEAR,PARABOLIC} tprofile;
    /* parameters with built-in defaults */
    float Rgeo = 1.65,
          Rmin = 1.15,
          Rmax = 2.15,
          Tmin = 1000.0,
          Tmax = 10000.0,
          Vmax = 9.0;

    if (prm_getval (prm_get("tprofile"), &valstr,  &type))
      {
	if (strcmp(valstr,"flat")==0)           tprofile=FLAT;
	else if (strcmp(valstr,"linear")==0)    tprofile=LINEAR;
	else if (strcmp(valstr,"parabolic")==0) tprofile=PARABOLIC;
      }
    else tprofile = FLAT;

    if ( tprofile!=FLAT && (!pRadSet || pRadSet->Bt < 1.0) )
      {
	sprintf(msg, 
	"Invalid Bt=%6.2f - switching to FLAT T profile to calculate optimum gains", 
		pRadSet->Bt);
	log_msg(WARNING, msg);
	tprofile = FLAT;
      }

    if (prm_getval (prm_get("Tmax"), &valstr,  &type))
      Tmax = (float) atof(valstr);

    if (prm_getval (prm_get("Vmax"), &valstr,  &type))
      Vmax = (float) atof(valstr);

    /* some parameters needed only if position-dependent T */
    if (tprofile != FLAT)
      {
	if (prm_getval (prm_get("Tmin"), &valstr,  &type))
	  Tmin = (float) atof(valstr);

	if (prm_getval (prm_get("Rgeo"), &valstr,  &type))
	  Rgeo = (float) atof(valstr);

	if (prm_getval (prm_get("Rmin"), &valstr,  &type))
	  Rmin = (float) atof(valstr);

	if (prm_getval (prm_get("Rmax"), &valstr,  &type))
	  Rmax = (float) atof(valstr);
      }

    switch(tprofile)
      {
      case FLAT:
	sprintf(msg,"Finding gains for FLAT T profile, Tmax = %5.0f eV", Tmax);
	break;
      case LINEAR:
	sprintf(msg,
		"Finding gains for LINEAR T profile, Tmin = %5.0f eV, Tmax = %5.0f eV",
		Tmin, Tmax);
	break;
      case PARABOLIC:
	sprintf(msg,
		"Finding gains for PARABOLIC T profile, Tmin = %5.0f eV, Tmax = %5.0f eV",
		Tmin, Tmax);
	break;
      }
    log_msg(DEBUG, msg);

    if (tprofile!=FLAT)
      { 
	sprintf(msg, "Bt=%6.2f, Rmin=%5.3f Rgeo=%5.3f Rmax=%5.3f m, ", 
		pRadSet->Bt, Rmin, Rgeo, Rmax);
	log_msg(DEBUG, msg);
      }

    /* loop through channels and calculate gains */
    for (c=0; c<MAX_CHAN; ++c)
      if (pRadSet->have[c])
	{
	  int nif = pRadSet->ifgroup[c]-1;   //Corrected!
	  float video_gain = pRadSet->gainpa[c] * pRadSet->gainma1[c] 
	        * ( (float) pow ((double) 10.0, (double) (-0.1* (pRadSet->attnif[nif]))) );
	  float denom = ELEMENTARY_CHARGE * video_gain * pRadSet->S[c]
	      * pRadSet->freqrf[c] * pRadSet->freqrf[c] * pRadSet->bif[c];
	  float calibFact, T;

	  if (denom <= 0)     /* cannot work if any of the other gains = 0 */
	    {//{++nbadchan;continue;} 
	       ++nbadchan;
		printf("bad chan c:%u\t, denom: %f\n",c+1,denom);
	        printf("gainma1: %f\t",pRadSet->gainma1[c]);
		printf("attnif: %f\t, nif: %u\n",pRadSet->attnif[nif],nif);
		printf("freqrf: %f\t",pRadSet->freqrf[c]);
		continue;}

	  calibFact = C2 / denom;

	  /* find max. target temperature T for this channel */
	  if (tprofile==FLAT)
	      T = Tmax;
	  else
	    {
	      float R, B = fabs(pRadSet->Btot[c]);
	      /* need cold resonance field */
	      if (B<0.5) {++nbadchan; printf("bad chan c%u\t, Bchan %f\n",c+1,B);continue;}  
	      /* radial position of channel (assume B=B0*R0/R)*/
	      R = pRadSet->Bt * Rgeo / B;
	      if (R<Rmin) T=Tmin;
	      else if (R>Rmax) T=Tmin;
	      else 
		{
		  if (tprofile==PARABOLIC)
		    {
		      float p1=(Tmin-Tmax)/((Rmax-Rgeo)*(Rmax-Rgeo));
		      float p2=(Tmin-Tmax)/((Rmin-Rgeo)*(Rmin-Rgeo));
		      float Roverlap = 2.0/3.0*Rmax;   /* 2nd and 3rd harmonic */ //Correction!
		      float Toverlap = Tmax+p2*(Roverlap-Rgeo)*(Roverlap-Rgeo);
		      if (R>Rgeo) T = Tmax + p1*(R-Rgeo)*(R-Rgeo);  // LFS
		      else if (R>=Roverlap)
			      T = Tmax + p2*(R-Rgeo)*(R-Rgeo);  // HFS
		      else T = Toverlap;
		    }
		  else /* linear */
		    {
		      if (R>Rgeo) T = Tmax - (Tmax-Tmin)*(R-Rgeo)/(Rmax-Rgeo);
		      else        T = Tmax - (Tmax-Tmin)*(Rgeo-R)/(Rgeo-Rmin); //Correction!!
		    }
		}
 	    //printf("chan c: %u\t, R: \t%f, T: \t%f\n",c,R,T);
	    //printf("calibFact: \t%f\n",calibFact);
	    }

	  /* write out optimum gain */
	  pRadSet->gainma2[c] = calibFact*Vmax/T;
	  printf("chan c: %u\tgainma2 : %f\t Rad: %f\t Tchan: %f\n",c+1,pRadSet->gainma2[c],pRadSet->Bt * Rgeo /fabs(pRadSet->Btot[c]),T);
	}

    if (nbadchan)
      {
	sprintf(msg, "Could not find optimum gain for %d channels", nbadchan);
	log_msg(INFO, msg);
      }


  }  /* calc_opt_gains */  



/* ------ parse channel interval and set gains
          manually ------- */

static	void	set_gains_manually (char *s, struct RadiometerSettings *pRadSet)
  {
    char *c;
    int   ch1, ch2;
    float gain;

    if ((c=strchr(s, '/')))   	/* interval start */
      {
      *c = '\000';
      ch1 = atoi(s);
      *c = '/';
      s = c+1;	/* skip '/' */
      }
    else ch1=-1;    /* default: only one channel (ch2) */

    if ((c=strchr(s, ':')))    	/* interval stop */
      {
        *c = '\000';
        ch2 = atoi(s);
        *c = ':';
        s = c+1;	/* skip ':' */
      }
    else ch2 = -1;

    if (ch1<0) ch1=ch2;   /* no '/', i.e. only one channel (ch2) */
    if (ch2<0) return;    /* no valid channel numbers nor gain */

    gain = (float) atof(s);	/* desired gain */
    if (pRadSet)
      {
	char msg[128];
	int i;
	if (ch1==ch2)
	  sprintf(msg, "Channel %d, main video amp gain: %3.0f", ch2, gain);
	else
	  sprintf(msg, "Channels %d to %d, main video amp gain: %3.0f", ch1, ch2, gain);
	log_msg(DEBUG, msg);

	for (i=ch1; i<=ch2; ++i)
	  pRadSet->gainma2[i-1] = gain;
      }
  }	/* set_gains_manually */





/* ------ parse parameters and find out how to set gains ----- */

void set_gains (struct RadiometerSettings *pRadSet)
{
  char *valstr;
  int type;
  PRM_HANDLE p;  

  p=prm_get("gains");
  while (p && prm_getval (p, &valstr,  &type))
    {
      if (strcmp(valstr, "default")==0)
	{ ; }     /* nothing to do as we start from defaults */
      else if (strcmp(valstr, "auto")==0)
	calc_opt_gains (pRadSet);
      else        /* expect explicit format, "ch1/ch2:gain" */
	  set_gains_manually(valstr, pRadSet);
    }
}  /* set_opt_gains */




/* ------------------- initialisation ------------------------- */

#define GROUP "OptimumGain"

void init_optgain     (void)
{
  prm_group  (GROUP, "Optimum gain settings for main amplifiers");
  prm_define ("gains", "default,auto,(string)", "default",
              "\tGains are default (constant), automatic (optimised), or manual (ch1/ch2:gain)",
              GROUP, NULL);
  prm_define ("tprofile", "flat,linear,parabolic", "flat",
              "\tExpected shape of temperature profile: flat (TMAX), linear or parabolice between TMIN (boundary) and TMAX (at RMAX)",
              GROUP, NULL);
  prm_define ("Tmax", "(float)", "10000",
              "\tMaximum temperature expected (eV)",
              GROUP, NULL);
  prm_define ("Tmin", "(float)", "1000",
              "\tMinimum temperature expected (eV)",
              GROUP, NULL);
  prm_define ("Rgeo", "(float)", "1.65",
              "\tGeometrical center radius (m) at which B=B0",
              GROUP, NULL);
  prm_define ("Rmin", "(float)", "1.15",
              "\tMinimum plasma major radius (m)",
              GROUP, NULL);
  prm_define ("Rmax", "(float)", "2.15",
              "\tMaximum plasma major radius (m)",
              GROUP, NULL);
  prm_define ("Vmax", "(float)", "9.0",
              "\tMaximum allowed ADC voltage (V)",
              GROUP, NULL);
}
/* init_optgain */
