/* rmcsfh.c   - edit RMC shot file header 
   08-Feb-2012  Wolfgang Suttrop
*/


/*
#include <string.h>
*/

#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef DDWW8
#include "ansisfh8.h"
#else
#include "ansisfh.h"
#endif


#include "libece.h"
#include "param.h" 
#include "util.h"

/* #include "eceradiometer.h" */

#include "radctrl.h"


/* Tables of possible gain values.
  Note gain x10 can be set in 2 ways: 100(=4) or 011(=3) */
#define NGAINS 7
static float gainma2[NGAINS]     = {1.0, 2.0, 5.0, 10.0, 20.0, 50.0, 100.0};
static float offsgains[NGAINS]    = {1.0, 1.0, 1.0,  1.0, 10.0, 10.0,  10.0};
static uint16_t gaincodes[NGAINS] = {0,   1,   2,    3,    5,    6,     7};
enum mode_sfh {AUTOMATIC,MANUAL} mode;


/* calculate the gain and offset settings for RMC.
   Returns:
   0: If successful
   -1: If no RMC shot file header defined and hence nothing to be done.
*/

int calc_rmc_gains(struct RadiometerSettings *pRadSet,
		   struct GainSettings *pGainSet)
{
  int type; char *valstr;
  uint16_t baseline_code;
  int c, n;
  int gain_statistics[NGAINS];


  if (!pRadSet) return(-1);

  if (prm_getval(prm_get("rmc_baseline"),&valstr,&type))
    baseline_code = atoi(valstr);
  else
    baseline_code = 200; //LBarrera correction
    //baseline_code = 2048;

  for (c=0; c<NGAINS; ++c) gain_statistics[c]=0;

  /* loop through radiometer channels */
  for (c=0; c<MAX_CHAN; ++c)
    if (pRadSet->have[c])
      {
        int   i=0;
        float gain = pRadSet->gainma2[c];
        int   offs;

        /* find nearest lower gain */
        while(i+1<NGAINS && gain>gainma2[i+1]) ++i;
        pRadSet->gainma2[c] = gainma2[i];  /* take note of actual setting */
        pGainSet->ch_gain[c] = i;   /* index, not value! */
	++gain_statistics[i];       /* and count */

        /* calculate offset correction output voltage */
        /*offs = ((baseline_code - 2048) / offsgains[i]) + 2048;
	*/
	//LBarrera Correction! the offset is added to the baseline of the card (arround 2048)
	offs=baseline_code/offsgains[i];
	if (offs>4095)   pGainSet->ch_offs[c] = 4095;
	else if (offs<0) pGainSet->ch_offs[c] = 0;
	else             pGainSet->ch_offs[c] = offs;	
      }
    else   /* defaults for uncalibrated channels */
      {
	pGainSet->ch_gain[c] = 0;   /* i.e., 1 */
	++gain_statistics[0];       /* and count */
	if (baseline_code>4095)   pGainSet->ch_offs[c] = 4095;
	else if (baseline_code<0) pGainSet->ch_offs[c] = 0;
	else                      pGainSet->ch_offs[c] = baseline_code;
      }

  for (c=0; c<NGAINS; ++c)
    if ((n = gain_statistics[c]))
      {
        char msg[128];
        sprintf(msg, "%d channels set to main amp gain %3.0f", n, gainma2[c]);
	log_msg(DEBUG, msg);
      }

  return(0);
}  /*calc_rmc_gains*/




int update_rmc_sfh(struct RadiometerSettings *pRadSet,struct GainSettings *pGainSet)
{
  PRM_HANDLE p; 
  char *sfhfn, msg[256], onam[9], pnam[9],*devstat,*valstr;
  devstat="MODSFH";
  int type, c, i;
  int status, status_W;

  if (prm_getval (prm_get("mode"), &valstr,  &type))
      {
	if (strcmp(valstr,"automatic")==0)      mode=AUTOMATIC;
	if (strcmp(valstr,"auto")==0)      mode=AUTOMATIC;
	else if (strcmp(valstr,"manual")==0)    mode=MANUAL;
      }

#ifdef DDWW8
  int32_t err, sfidp;
#else
  int err, sfidp;
#endif

  int ch, link, /* card, */ ADC;
  int32_t ival[30];
  float   multia01[30],  /* second calibration step */
          shiftb01[30];

  /* get path to shot file header */
  p=prm_get("rmc_shotfile_header");
  if (!prm_getval(p, &sfhfn, &type))
    {
      log_msg(INFO, 
      "No RMC sfh given (rmc_shotfile_header) - hardware gains will not be set.");      
      return(-1);
    }

  /* open shot file header */
  if ((err = sfhopen(sfhfn, &sfidp))) 
    {
      sfherror (err, "(sfhopen)");
      sprintf(msg, "Cannot open shot file header %s", sfhfn);
      log_msg(ERROR, msg); 
      return(-1);
    }

  sprintf(msg, "Updating shot file header %s", sfhfn);
  log_msg(INFO, msg);

 /* Read status of the sfh (the RMC program should wait for radctrl program to modify the sfh)*/
 /* LBarrera 19 Jun 2012*/
  if ((err = sfhrstatus (sfidp,devstat, &status)))
    {
	sfherror (err, "C-Routine");
	sprintf(msg, "Error reading the status of the sfh %u", err);
        log_msg(ERROR, msg); 
    }
  else
    {
	sprintf(msg, "sfhrstatus(%s) = %d\n", devstat, status);
        log_msg(INFO, msg); 
    }
    /* Change status if the operational mode is not correct*/
  printf ("sfhwstatus previously to check = %d\n", status);

  if(mode==AUTOMATIC)
    status_W=0;
  if(mode==MANUAL)
    status_W=-1;
  if (status_W != status)
  {
	
    if ((err = sfhwstatus (sfidp, devstat,status_W)))
      {
	sfherror (err, "C-Routine");
	sprintf(msg, "Error writing the status of the sfh %u", err);
	log_msg(ERROR, msg);
      }
    else
      {
	sprintf (msg,"sfhwstatus(%s) = %d\n", devstat, status_W);
	log_msg(INFO, msg);
      }	 	 
   }

    /*   change command line in rmc sfh to extend the operation window */
   if ((err = sfhwstatus (sfidp, devstat,status_W)))
      {
        sfherror (err, "C-Routine");
	sprintf(msg, "Error writing the status of the sfh %u", err);
        log_msg(ERROR, msg);
      }
    


  /* set gains and offset for all channels -
     loop through channels in groups of two, because
     two neighbouring channels (card=0 and card=1) are in one parameter and
     thus are written simultaneously */

  for (c=0; c<MAX_CHAN; c+=2)
    {
      ch = (c<30 ? c : c+2);      /* DAQ channel numbers 0..63 (skip 30,31,58,59) */
      ADC = (ch & 7) >> 1;        /* 0..3 */
      /*  card = ch & 1;  */      /* (ch % 2) 0..1, however this loop is unrolled */
      link = ch >> 3;             /* (ch / 8) 0..7 */

      sprintf(onam, "%s%i", "Verst", link);

      /* write gains */
      sprintf(pnam, "%s%i", "ADC", ADC);
      for (i=0; i<2; ++i)   /* force 32 bit integers */
	{
	  int g;
	  g = pGainSet->ch_gain[c+i];
	  if (g>=0 && g<NGAINS) ival[i] = gaincodes[g];     
	  else ival[i] = 0;
	}
      if ((err = sfhmodpar (sfidp, onam, pnam, 1, 2, (char *) ival))) 
	{
	sfherror (err, "(writeOffset)");
        sprintf(msg, "Failed to write gain codes for channel %i to %s", ch, sfhfn);
        log_msg(ERROR, msg); 
        continue;
	}

      /* write offsets */
      sprintf(pnam, "%s%i", "DAC", ADC);
      for (i=0; i<2; ++i)   /* force 32 bit integers */
	ival[i] = pGainSet->ch_offs[c+i];
      if ((err = sfhmodpar (sfidp, onam, pnam, 1, 2, (char *) ival))) 
	{
	sfherror (err, "(writeOffset)");
        sprintf(msg, "Failed to write offsets for channel %i to %s", ch, sfhfn);
        log_msg(ERROR, msg); 
        continue;
	}

    } /* for c */


  /* set calibration factors (second step) 
     so that calibrated readings correspond to amplifier input voltages 
     (not ADC voltages)
     Calibration data for 30 channels are written at once
     and there are two parameter sets with 30 channels each.
   */

  for (i=0; i<2; ++i)
    {
      sprintf(onam, "%s%i", "CAL-A", i+1);

      for (c=0; c<30; ++c)
	{
	  int ch = i*30 + c;
	  int g = pGainSet->ch_gain[ch];
	  float Uoffs = (pGainSet->ch_offs[ch] * 2.442e-3) -5.0 ;

	  if (g>=0 && g<NGAINS) multia01[c] = 1.0 / gainma2[g];     
	  else multia01[c] = 0; /* error */
	  //shiftb01[c]=0; /* Lbarrera check 20-03-2012, what is the actual voltage that we read, is the formula ok??*/
	  shiftb01[c] = (offsgains[g] * Uoffs) / gainma2[g];
	}

      if ((err = sfhmodpar (sfidp, onam, "MULTIA01", 2, 30, (char *) multia01)))
	{
	sfherror (err, "(MULTIA01)");
        sprintf(msg, "Failed to write MULTIA01 to %s", sfhfn);
        log_msg(ERROR, msg); 
        continue;
	}

      if ((err = sfhmodpar (sfidp, onam, "SHIFTB01", 2, 30, (char *) shiftb01))) 
	{
	sfherror (err, "(SHIFTB01)");
        sprintf(msg, "Failed to write SHIFTB01 to %s", sfhfn);
        log_msg(ERROR, msg); 
        continue;
	}
    }

  /* Write parameter-set (METHODS & PARAMS-A)*/ //LBarrera
   if ((err =write_methods_sfh (sfidp, pRadSet)))
	{
	sfherror (err, "(METHODS)");
        sprintf(msg, "Failed to write parameter set METHODS to %s", sfhfn);
        log_msg(ERROR, msg); 
	}


 /* Write parameter-set PARAMS-A*/  //LBarrera

  if ((err =write_channel_settings_sfh (sfidp, pRadSet)))
	{
	sfherror (err, "(parms-A)");
        sprintf(msg, "Failed to write parameter set parms-A to %s", sfhfn);
        log_msg(ERROR, msg); 
	}
 

  /* close shot file header */
  if ((err = sfhclose(sfidp))) 
    {
      sfherror (err, "(sfhclose)");
      sprintf(msg, "Cannot close shot file header %s", sfhfn);
      log_msg(ERROR, msg); 
      return -1;
    }	


  return(0);
}  /*update_rmc_sfh*/



#define GROUP "RMC"

void init_rmcsfh(void)
{
  prm_group  (GROUP, "RMC");

  prm_define ("rmc_shotfile_header", "(string)", "/shotfiles/DIAGS/RMC/RMC00000.sfh",   //LBarrera 
              "complete path to RMC shot file header. Empty: None to update",
              GROUP, NULL);  

  prm_define ("rmc_baseline", "(integer)", "1800",                                    //LBarrera it was set to "200" 
              "ADC output code (12-bit offset binary) for zero input voltage.",
              GROUP, NULL);  
      
 /*prm_define ("mode", "manual, automatic,(string)", "automatic", 
              "Set the RMC program manually or automatic: start the shot distribution without/with waiting for radctrl program)",
              GROUP, NULL); */
}
