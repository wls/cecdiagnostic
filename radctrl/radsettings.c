/* radsettings.c 
   control basic radiometer settings 
   22-Nov-2021
   added 105 GHz Mixer
   Modifications:
   *change wg LSB and USB for 101 Mixer: change enumeration for LO accordingly (Sept 2012, LBarrera)

   09-Jun-2010
   Modifications:
   *change wg LSB and USB for 101 Mixer: change enumeration for LO accordingly (Sept 2012, LBarrera)

Uses coil actuator unit (coilunit.c) for input/output


16-bit output port  (upper 4 bits in coilunit 1, lower 12 bits in coilunit 0)
     
Bit     control                                 "1"     "0" or power off
------------------------------------------------------------------------
1 (LSB) Ovrsiz wg switch 1 (128 GHz USB)        w/o     with notch filter
2           "            2 (133 GHz LSB)        on      off
3           "            3 (095 GHz USB)        on      off
4           "            4 (167 GHz USB)        on      off
5           "            5 (167 GHz LSB)        on      off
6           "            6 (101 GHz USB)        on      off
7           "            7 (101 GHz LSB)        on      off
8       SMI wg switch 1    (101 GHz)            LSB     USB
9       SMI wg switch 2    (not used)
10      IF SP6T coax switch  (ch 1-36)  Bit A   (see truth table below)
11      "         "     "        "      Bit B     "     "    "     "
12      IF SPDT coax switch 1 (channels 49-60)   133    95  GHz
13      IF SPDT coax switch 2 (not used)
14      IF SP4T coax switch  (ch 37-48) Bit A   (see truth table below)
15      "         "     "        "      Bit B     "     "    "     "
16      readout mode control                    mode 1  mode 0
------------------------------------------------------------------------



16-bit Input  port:
                                                  mode 0          mode 1
Bit     status                                  "1"     "0"     "1"    "0"
------------------------------------------------------------------------
1 (LSB) Ovrsiz wg switch 1 (128 GHz notch)
2           "            2 (133 GHz LSB)
3           "            3 (095 GHz USB)
4           "            4 (167 GHz USB)
5           "            5 (167 GHz LSB)
6           "            6 (101 GHz USB)
7           "            7 (101 GHz LSB)
8       SMI wg switch 1    (101 GHz)           /LSB     LSB     /USB    USB
9       IF SP6T coax switch (ch. 1-36)  Bit A   (see truth table below)
10      "         "     "        "      Bit B     "     "    "     "
11      "         "     "        "      Bit C     "     "    "     "
12      SPDT coax switch 1 (ch. 49-60)         /133     133      /95     95
13      SPDT coax switch 1 (not used)
14      IF SP4T coax switch (ch. 37-48) Bit A   (see truth table below)
15      "         "     "        "      Bit B     "     "    "     "
16      "         "     "        "      Bit C     "     "    "     "
------------------------------------------------------------------------
*/


#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#ifdef DDWW8
#include "ddwwansic8.h"
#else
#include "ddwwansic.h"
#endif

#include "libece.h"
#include "param.h"
#include "util.h"
#include "eceradiometer.h"
#include "miracle.h"
#include "radctrl.h"

#define GROUP "radsettings"


static struct RadiometerSettings RequestedSettings;


/* Control words for the LO_settings, as enumerated before Sept-2012:
   none, LO101L, LO101U, LO95U, LO133L, LO133U, LO128U, LO128N, LO167L, LO167U} */

/* Control words for the LO_settings, as enumerated after Sept-2012: change WG LSB and USB for 101 Mixer:
   changed in order of the enum LO_Connection in libece.h LBarrera 
   none, LO101U, LO101L, LO95U, LO133L, LO133U, LO128U, LO128N, LO167L, LO167U,LO91L,LO91U} */

/* Oversized RF switch (bits 0..6) and SMI monomodic waveguide switch (bit 7)
   settings for each receiver connection 
 * previous settings (<33724) are at the end of the file, change from mwillens
// Dec-2019 91 USB  from wg switch 6 is removed and switch too

Bit     control                                 "1"     "0" or power off
-------------------------------------------------------------------------
0 (LSB) Ovrsiz wg switch 1 (101 GHz LSB)        on      off
1           "            2 (133 GHz LSB)        on      off
2           "            3 (095 GHz USB)        on      off
3           "            4 (101S GHz USB)       on      off  *changed June-2015,use 105GHz Nov-2021
4           "            5 (128 GHz USB,Notch)  on      off
5           "            6 ------------         on      off  *changed Jan-2017,Dex
6           "            7 (91 GHz LSB)         on      off  *changed Jan-2019
7       SMI wg switch 1    (91 GHz)            USB     LSB  *
-------------------------------------------------------------------------

Notes: 
- When using 128U without notch filter, have to switch on 095U
as well to avoid clashes of the two oversized waveguide switches (code 0x05)
 * {"none", "101U", "101L", "95U", "133L", "133U", "128U", "128N", "167L", "167U", "101S", "91L", "91U"};
*/
/*added 0x08 for 101S June, 2015*/
// to avoid clash 101L and 95U have the same word and 133L & 128N as well  
// 101U -> 0x00,101L (3) 0x20-> 0x05
// 128N (8) 0x00 -> 0x12, 00010010
// 133L (5) 0x02 -> 0x12, 133U -> 0x00000000
// 95U (4) 0x04-> 0x05, 0x00000000
// change between 101L and 91U due to broken 101S
/*
Bit     control                                 "1"     "0" or power off
-------------------------------------------------------------------------
0 (LSB) Ovrsiz wg switch 1 (91 GHz LSB)         on      off
1           "            2 (133 GHz LSB)        on      off
2           "            3 (095 GHz USB)        on      off
3           "            4 (105 GHz USB)       on      off  * Dec-2019, defect 101 LO, use 105GHz Nov-2021
4           "            5 (128 GHz USB,Notch)  on      off
5           "            6 ------------         on      off  *changed Jan-2017,Dex
6           "            7 (101 GHz LSB)         on      off  *changed Jan-2019
7       SMI wg switch 1    (101 GHz)            USB     LSB  *
-------------------------------------------------------------------------
*/

static int ctrl_rf [MAX_LO_CONNECTIONS] = 
  {0x0, 0x0020, 0x00C0 ,  0x0005,  0x0012,  0x00, 0x00, 0x0012, 0x00, 0x00, 0x0008,  0x0005, 0x0000, 0x0008};
// added 105 mixer at the end
//   {0x0,  0x0000,  0x00C0,   0x0005,  0x0012,  0x00, 0x00, 0x0012, 0x00, 0x00, 0x0008,  0x0005, 0x0000};Jan 2019
//  {0x0,  0x0000,   0x0005,   0x0005,  0x0012,  0x00, 0x00, 0x0012, 0x00, 0x00, 0x0008, 0x00C0, 0x0020}; DEC 2019



/*  IF1 (ch 1-36), SP6T coax IF switch  
 Bit12  Bit 10   Bit 9   mixer selected 
------------------------------
 0       0       0     101 GHz
 0       0       1      95 GHz
 0       1       0     128 GHz
 0       1       1     105GHz, old:101S GHz
 1       0       0      91 GHz
------------------------------
  
  
 
*/
/*added 0x06 for 101S June, 2015*/
//{"none","101U","101L", "95U", "133L","133U","128U","128N","167L", "167U", "101S", "91L", "91U","105U"};
static int ctrl_if1 [MAX_LO_CONNECTIONS] = 
  { 0x0, 0x0000, 0x0000, 0x0200, 0x0,    0x0, 0x00,  0x0400, 0x00,  0x00,  0x0600, 0x1000, 0x1000, 0x0600 };


/*  IF2 (ch 37-48), SP4T coax IF switch  
Bit 14   Bit 13  mixer selected 
------------------------------
  0       0     101 GHz
  0       1     133 GHz
  1       0     128 GHz
  1       1     101S GHz
------------------------------
*/
/*added 0x06 for 101S June, 2015*/
static int ctrl_if2 [MAX_LO_CONNECTIONS] = 
  { 0x0, 0x0000, 0x0000, 0x0000, 0x2000,  0x2000,  0x4000, 0x4000, 0x6000, 0x6000, 0x6000, 0x0000, 0x0000, 0x6000 };

/*  IF3 (ch 49-60), SPDT coax IF switch  
Bit 11
------------------------------
   0            95 GHz
   1           133 GHz
------------------------------
*/
static int ctrl_if3 [MAX_LO_CONNECTIONS] = 
  { 0x0, 0x0, 0x0, 0x0, 0x0800, 0x0800, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0000, 0x0000, 0x0 };




/* check wg switch settings */
static int check_st_switch(char *desc, int ctrl, int is0, int is1, int mask)
{
  char msg[128];

  if ((is0^is1) & mask)
  {
    if ((is0^ctrl) & mask)
      {
	sprintf(msg, "%s at wrong end position.", desc);
	log_msg(ERROR, msg);
	return(1);
      }
  }
  else
    if (is0 & mask)
      {
        sprintf(msg, "%s not at an end position.", desc);
	log_msg(ERROR, msg);
	return(2);
      }
    else
      {
        sprintf(msg, "%s at both end positions?", desc);
	log_msg(ERROR, msg);
	return(4);
      }
  return(0);
}       /*check*/



/* ... use coilunits to set waveguide/IF switches ...  */

static int set_ifgroups (struct RadiometerSettings *pRadSet)
{
  int type, delay, 
      ctrl = 0,ctrlTmp = 0, is0, is1, 
      sp4t, sp6t, stat = 0;
  char msg[128], *valstr;

  if (!pRadSet) return(-1);  

  /* obtain settling time */
  if (prm_getval (prm_get("readback_delay"), &valstr,  &type))
    delay = atoi(valstr);
  else
    delay = 4;   /* seconds default, probably OK */


  /* assemble control word */
  // here changes to avoid collisions 
  ctrl =
    ctrl_rf [pRadSet->LO[0]] | ctrl_rf [pRadSet->LO[1]] | ctrl_rf [pRadSet->LO[2]]
  | ctrl_if1[pRadSet->LO[0]] | ctrl_if2[pRadSet->LO[1]] | ctrl_if3[pRadSet->LO[2]];
  sprintf(msg, "Radiometer control output: %08x", ctrl);
  log_msg(DEBUG, msg);

  /* set requested switch positions, mode = 0*/ 
  set_rad_switches     (ctrl & 0x7FFF);

  /* wait if asked to */
  if (delay>0)
  {
    if (delay>4)
    {
      sprintf(msg, "Waiting %d s for switches to settle.", delay);
      log_msg(INFO, msg);
    }
    sleep (delay);
  }

  /* read actual position, mode=0 */
  get_rad_indicators   (&is0);

  sprintf(msg, "Radiometer input (mode=0): %08x", is0);
  log_msg(DEBUG, msg);

  /* ditto, mode=1 */
  set_rad_switches     (ctrl | 0x8000);
  get_rad_indicators   (&is1);

  sprintf(msg, "Radiometer input (mode=1): %08x", is1);
  log_msg(DEBUG, msg);

  /* ethernet disconnect to force re-connect next time */
  coilunits_disconnect();


  /* check waveguide switches */
  stat |= check_st_switch("25 mm waveguide switch #1", ctrl, is0, is1, 0x0001);
  stat |= check_st_switch("25 mm waveguide switch #2", ctrl, is0, is1, 0x0002);
  stat |= check_st_switch("25 mm waveguide switch #3", ctrl, is0, is1, 0x0004);
  stat |= check_st_switch("25 mm waveguide switch #4", ctrl, is0, is1, 0x0008);
  stat |= check_st_switch("25 mm waveguide switch #5", ctrl, is0, is1, 0x0010);
  stat |= check_st_switch("25 mm waveguide switch #6", ctrl, is0, is1, 0x0020);
  stat |= check_st_switch("25 mm waveguide switch #7", ctrl, is0, is1, 0x0040);
  // monomode switch is removed in Dec 2019, mwillens, reinstalled for 101 GHz in Jan 2020
  stat |= check_st_switch("Monomode waveguide switch #1 (91 GHz)", ctrl, is0, is1, 0x0080);
  /* check coaxial IF switches */


/*
Truth table for SP6T coax switch status (channels 1-36)

Bit 10  Bit 9   Bit 8   mixer selected 
--------------------------------------
  0       0       0     none
  0       0       1     101 GHz
  0       1       0      95 GHz
  0       1       1     128 GHz
  1       0       0     105Ghz old:101S GHz
  1       0       1     91 GHz
  1       1       0       "
  1       1       1       "
-------------------------------------- */

  // select Bit8-Bit10
  sp6t = is0 & 0x0700;
  // shift bit 12 to 11, and combine ctrl to Bit 11, 10, 9
  ctrlTmp = (((ctrl & 0x1000) >>1 ) | (ctrl & 0x0600));
  if (!sp6t)
    {
      log_msg(ERROR, "IF1 coax switch (ch  1-36) is not connected to a mixer.");
      stat |= 2;
    } ///old if ((sp6t-0x0100) << 1 != (ctrl & 0x0600))
  // << 1 bitwise shift
  else if ((sp6t-0x0100) << 1 != (ctrlTmp))
    {
      log_msg(ERROR, "IF1 coax switch (ch  1-36) has wrong position.");
      stat |= 1;
    } 


/* 
Truth table for SP4T coax switch status (channels 37-48)
Bit 15  Bit 14  Bit 13   mixer selected 
--------------------------------------
  0       0       0     none
  0       0       1     101 GHz
  0       1       0     133 GHz
  0       1       1     128 GHz
  1       0       0     105 GHz old: 101S GHz
  1       0       1     invalid code
  1       1       0       "
  1       1       1       "
-------------------------------------- */

  sp4t = is0 & 0xE000;
  if (!sp4t)
    {
      sprintf(msg, "IF2 coax switch (ch 37-48) has undefined position.");
      log_msg(ERROR, msg);
      stat |= 2;
    }
  else if (sp4t-0x2000 != (ctrl & 0x6000))
    {
      sprintf(msg, "IF2 coax switch (ch 37-48) has wrong position.");
      log_msg(ERROR, msg);
      stat |= 1;
    }

  stat |= check_st_switch("IF3 coax switch (ch 49-60)", ctrl, is0, is1, 0x0800);

  if (stat & 1)
    log_msg(WARNING, "Some switches have wrong position - Remote control enabled?");

  if (stat & 2)
    log_msg(WARNING, "Some switches have undefined position - Air pressure OK?");

  return(stat);
}  /* set_ifgroups */







/* ............... command handlers ................ */ 

static void sethw (int shot)
{
  struct GainSettings gs;
 
  char msg[256];
  /* fill all dependencies and check consistency, in file util/eceradiometer.c*/
  if (parse_radiometer_parameters (0, &RequestedSettings)) 
    return;
 
  /* gain optimisation --- optgain.c */
  set_gains (&RequestedSettings);
  
  /*write METHODS parameters in sfh of RMC diagnostic*/
  /* set RMC shot file header and download as fast as possible */
  if (calc_rmc_gains(&RequestedSettings, &gs)==0)
    update_rmc_sfh(&RequestedSettings, &gs);

  /* write RMD shot file with calibration factors and METHODS PARAMETERS */
 
  sprintf(msg,  "Writing RMD shot %u",shot);
   
  log_msg(INFO, msg);
  if (shot>0)
    write_rmd_calib(shot, &RequestedSettings, NULL);

  /* set radiometer switches (we have more time when doing this last)*/
  sprintf(msg,  "Setting IF groups");
  if (set_ifgroups (&RequestedSettings))
    return;

}  /* sethw */


static void sethw_next(int shot, int no_shot)
{
/* Sets the hardware and writes RMD shot files for the NEXT shot and the 
 no_shot after that. S. Denk 20.05.15 */
  struct GainSettings gs;
  char msg[256];
  int i;
  /* fill all dependencies and check consistency, in file util/eceradiometer.c*/
  prm_set("mode", "manual");
  /*p = prm_get("mode");
  if (prm_getval(p, &s, &type)) 
  {
      sprintf(msg, "Set RMC sfh in %s mode",s);
      log_msg(INFO, msg);
  }*/  
  if (parse_radiometer_parameters (0, &RequestedSettings)) 
    return;
  /* gain optimisation --- optgain.c */
  set_gains (&RequestedSettings);
  
  /*write METHODS parameters in sfh of RMC diagnostic*/
  /* set RMC shot file header and download as fast as possible */
  if (calc_rmc_gains(&RequestedSettings, &gs)==0)
    update_rmc_sfh(&RequestedSettings, &gs);

  /* write RMD shot file with calibration factors and METHODS PARAMETERS */
 
  sprintf(msg,  "Writing RMD shot %u",shot);
  for(i=0;i < no_shot;i++) {
    log_msg(INFO, msg);
    if (shot+i>0)
      write_rmd_calib(shot+i, &RequestedSettings, NULL);
  }
  /* set radiometer switches (we have more time when doing this last)*/
  sprintf(msg,  "Setting IF groups");
  if (set_ifgroups (&RequestedSettings))
    return;

}  /* sethw_next */



/* ............... command handlers ................ */ 

static void do_sethw (PRM_HANDLE p)
{

#ifdef DDWW8
  int32_t error = 0;
  uint32_t shot = 0; 
#else
  int error = 0;
  int shot = 0; 
#endif
  
  ddlastshotnr_ (&error, &shot);
  if (error)
    {
      log_msg(ERROR, "Cannot obtain current shot number - will not write RMD");
      shot = -1;
    }  
  sethw(shot);
}  /* do_sethw */



// from Severin Denk
static void do_sethw_next (PRM_HANDLE p)
{
// Wrapper for sethw_next. S. Denk 20.05.15 
#ifdef DDWW8
  int32_t error = 0;
  uint32_t shot = 0; 
#else
  int error = 0;
  int shot = 0; 
#endif
int type, no_shot; char *str;
char command[100];

prm_getval(p, &str, &type);
no_shot = atoi(str);
ddlastshotnr_ (&error, &shot);
if (error)
  {
    log_msg(ERROR, "Cannot obtain current shot number - will not write RMD");
    shot = -1;
  }  
sethw_next(shot + 1, no_shot);

strcpy( command, "cp /shotfiles/DIAGS/RMC/RMC00000.sfh /shotfiles/DIAGS/RMC/RMC00000.sfh.withoutMOD" );
system(command);


}  /* do_sethw_next */


/*static void do_ECRH105 (PRM_HANDLE p)
{   
  char *valstr;
  int type;
  
  prm_getval(prm_get("receivers"), &valstr,  &type);
  prm_set("receivers", valstr);

} */   /* do_ECRH105 */ 

/* ......... set-up for given Bt field............ */ 

#define BT_BDRY 6    /* #Bt boundary values */
/* here is a change needed since 133L can only be opened together with 128L */ 
static int   Bt_LO [BT_BDRY+1][MAX_IFGROUP]
                       = { {LO91L,  LO101L, LO95U  }, //{LO91L,  LO101L, LO95U  },		
			 {LO95U,  LO101L, LO95U  }, //{LO101L, LO101S, LO95U  },
			 {LO101L, LO133L, LO95U  },  //{LO101L, LO101S, LO95U  },
			 {LO95U,  LO105U, LO133L }, //  {LO95U,  LO101S, LO133L }, 
			 {LO101U, LO128N, LO133L },//  {LO101S, LO128N, LO133L }, // change to 101S
			 {LO105U, LO128N, LO133L },//  added 105U
			 {LO128N, LO133L, LO133L }  //{LO128N, LO167L, LO133L} Correction 30 Okt 2012, not properly set before!
                      };


static char *Bt_LOString [BT_BDRY+1][MAX_IFGROUP]     //LBarrera
                         = { {"LO91L",  "LO101L", "LO95U" }, //{ {"LO91L",  "LO101L", "LO95U" },
			     {"LO95U", "LO101L", "LO95U" }, // {"LO101L", "LO101S", "LO95U" },
			    {"LO101L", "LO133L", "LO95U" },
			    {"LO95U",  "LO101U", "LO133L"}, 
			    {"LO101U", "LO128N", "LO133L"}, // change to 101S
			    {"LO105U", "LO128N", "LO133L"}, // change to 101S
			    {"LO128N", "LO133L", "LO133L"}  //{"LO128N", "LO167L", "LO133L"} Mixer 167 not working
                      };

static int   Bt_LO_105 [BT_BDRY+1][MAX_IFGROUP]
		    = { {LO128N ,  LO133L, LO133L},
			{LO128N,  LO133L, LO133L},
			{LO128N,  LO133L, LO133L},
			{LO128N,  LO133L, LO133L}, 
			{LO128N,  LO133L, LO133L},
			{LO128N,  LO133L, LO133L}, 
			{LO128N,  LO133L, LO133L}  
                      };


static char *Bt_LOString_105 [BT_BDRY+1][MAX_IFGROUP]     //LBarrera
= {  			     {"LO128N",  "LO133L", "LO133L"},  
			     {"LO128N",  "LO133L", "LO133L"},
			     {"LO128N",  "LO133L", "LO133L"},
			     {"LO128N",  "LO133L", "LO133L"}, 
			     {"LO128N",  "LO133L", "LO133L"},
			     {"LO128N",  "LO133L", "LO133L"},
			     {"LO128N",  "LO133L", "LO133L"}  //{"LO128N", "LO167L", "LO133L"} Mixer 167 not working
                      };

static float Btlimit[BT_BDRY] =   {1.93 , 1.97,  2.19,   2.385, 2.44,   2.67};  //set 2.25 to 2.18


/* field given as argument */
void set_field (float Bt)
{
  char msg[256];
  int j, i=0;
  char *valstr;
  int type;
  uint ECRH105 = 1;
  RequestedSettings.Bt = Bt;
  if (prm_getval (prm_get("ECRH105"), &valstr,  &type))
  {
    if (strcmp(valstr,"false") == 0) ECRH105 = 0;
  }
  else
  {
        sprintf(msg, "Warning error when loading paramter ECRH105 -> ECRH105 = True assumed");
        log_msg(WARNING, msg);
  }
  if(ECRH105)
  {
     sprintf(msg, "Using mixers shielded against 105 GHz.");
     log_msg(INFO, msg);
  }
  while(i<BT_BDRY && Bt>Btlimit[i]) ++i;
  sprintf(msg, "Set Bt=%f, interval %d", Bt, i);
  log_msg(DEBUG, msg);

  for (j=0; j<MAX_IFGROUP; ++j)
  {
      if(ECRH105)
        RequestedSettings.LO[j] = Bt_LO_105[i][j];
      else
        RequestedSettings.LO[j] = Bt_LO[i][j];
  }
  if(ECRH105)
      sprintf(msg, "Optimal mixer selection for this Bt and 105GHz ECRH : %s %s %s",
              Bt_LOString_105[i][0],
              Bt_LOString_105[i][1],
              Bt_LOString_105[i][2]);
  else
      sprintf(msg, "Optimal mixer selection for this Bt : %s %s %s",
              Bt_LOString[i][0],
              Bt_LOString[i][1],
              Bt_LOString[i][2]);          
  log_msg(INFO, msg);

} /* set_field */



/* field from command line argument */
static void do_setbt(PRM_HANDLE p)
{
  int type;
  char *valstr;
  float Bt;

  if (prm_getval(p, &valstr, &type))
  {
    Bt = fabs(atof(valstr));
    if (Bt>=1.3 && Bt<=3.4)    
      set_field(Bt);
    else
      {
	char msg[256];
	sprintf(msg, "Cannot set radiometer for Bt=%f", Bt);
	log_msg(ERROR, msg);
      }
  }
}  /*do_setbt*/




/* .............Bt from "miracle" server ............... */ 

void set_from_miracle (int shot)
{
   struct preview_parameters ppars;
   char msg[128];
   int error;

   error = miracle (&ppars); 
      switch (error)
      {
        case 0:   break;  /* ok */
        case 200:
        case 201:
          log_msg(ERROR, "No http access to Miracle server");
          return;
        case -1:  
          log_msg(ERROR, "Error parsing preview xml text");
          return;
        case -2:  
          log_msg(ERROR, "Error reading preview file");
          return;
        default:
          sprintf(msg, "miracle() returns error %d", error);
          log_msg(ERROR, msg);
          return;
      }

   sprintf(msg, 
         "shot=%d DP=%s Version=%s Bt=%5.3f T Ip=%5.3f MA  Ptot=%5.3f MW\n", 
         ppars.shot, ppars.DPFileName, ppars.DPVersion, ppars.Bt, 
         ppars.Ipi/1E6, ppars.Ptot/1E6);
   log_msg(INFO, msg);

   if (shot != ppars.shot)
     {
       sprintf(msg, "Mismatch of diag (%d) and miracle (%d) shot numbers",
	       shot, ppars.shot);
       log_msg(WARNING, msg);
     }

   set_field( fabs(ppars.Bt) );
   sethw(shot);   /* actually  */

}  /* set_from_miracle */


static void do_set_from_miracle (PRM_HANDLE p)
{
#ifdef DDWW8
  int32_t error = 0;
  uint32_t shot = 0; 
#else
  int error = 0;
  int shot = 0; 
#endif
  
  ddlastshotnr_ (&error, &shot);
  if (error)
    {
      log_msg(ERROR, "Cannot obtain current shot number - will not write RMD");
      shot = -1;
    }
  set_from_miracle(shot);
  
}  /* do_set_from_miracle */


/* ................ initialisation ..................*/

void init_radsettings     (void)
{

  set_radiometer_defaults     (&RequestedSettings);

  prm_group  (GROUP, "Radiometer control");

  prm_define ("sethw", NULL, NULL, 
              "actually setup hardware according to radiometer parameters",
              GROUP, do_sethw);

 // Assign function to event sethw_next. S. Denk 20.05.15
  prm_define ("sethw_next","(integer)", "0", 
              "actually setup hardware for next discharges according to radiometer parameters",
              GROUP, do_sethw_next);

  prm_define ("readback_delay", "(integer)", "4", 
              "allowance time (seconds) for switches to settle before checked",
              GROUP, NULL);

  prm_define ("setbt", "(float)", "2.5", 
              "standard radiometer settings for a given Bt",
              GROUP, do_setbt);

  prm_define ("miracle", NULL, NULL, 
              "Obtain Bt from Miracle server and actually set hardware",
              GROUP, do_set_from_miracle);
   
  prm_define ("mode", "manual, automatic,(string)", "automatic", 
              "Set the RMC program manually or automatic: start the shot distribution without/with waiting for radctrl program)",
              GROUP, NULL);
  
  prm_define ("ECRH105", "false, true,(string)", "false", 
              "If true, disable all receivers vulnerable to 105 GHz ECRH",
              GROUP, NULL);


}  /* init_radsettings */


/* Settings before Jan 2017
 * Oversized RF switch (bits 0..6) and SMI monomodic waveguide switch (bit 7)
   settings for each receiver connection 
Bit     control                                 "1"     "0" or power off
-------------------------------------------------------------------------
0 (LSB) Ovrsiz wg switch 1 (128 GHz USB)        w/o     with notch filter
1           "            2 (133 GHz LSB)        on      off
2           "            3 (095 GHz USB)        on      off
3           "            4 (105GHz USB)         on      off  *changed Nov-2022  old:(101SGHz USB)        on      off  *changed June-2015
4           "            5                      on      off
5           "            6 (101 GHz LSB)        on      off  *changed Sept-2012
6           "            7 (101 GHz USB)        on      off  *
7       SMI wg switch 1    (101 GHz)            USB     LSB  *
-------------------------------------------------------------------------

Notes: 
- When using 128U without notch filter, have to switch on 095U
as well to avoid clashes of the two oversized waveguide switches (code 0x05)
 * static int ctrl_rf [MAX_LO_CONNECTIONS] = 
  {0x0,  0xC0,    0x20,   0x04,  0x02,   0x02,    0x05,   0x00, 0x10, 0x08, 0x08};
 * none, LO101L, LO101U, LO95U, LO133L, LO133U, LO128U, LO128N, LO167L, LO167U, LO 101S
 */

