/*
       main.c - radctrl main program

       Wolfgang Suttrop,   suttrop@ipp.mpg.de

       08-Jun-2010

*/

#include <ctype.h>
#include <math.h>
#include <memory.h>
/* #include <setjmp.h> */
/* #include <signal.h> */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef DDWW8
#include "ddwwansic8.h"
#else
#include "ddwwansic.h"
#endif

#include "libece.h"
#include "util.h"
#include "param.h"
#include "eceradiometer.h"

#include "radctrl.h"



#define LOGSTRLEN	255      /* max. line length in log file */

static  char   *defaultarg[2];
//static void init_autoshot(void);        
#ifdef DDWW8
  uint32_t  shot;
  int32_t  error;
  static void init_autoshot(uint32_t  shot);   
#else
  int shot;
  int error;
  static void init_autoshot(int shot);   
#endif

/* ==================== main program ===================== */





int main (int argc, char **argv)
  {

  defaultarg[0] = "radctrl";
  defaultarg[1] = strdup("cmd");

  init_logging       ();
  init_coilunits     ();
  init_radiometer    ();

  init_radsettings   ();
  init_optgain       ();
  init_rmcsfh        ();  //Change order. First init_optgain and then rmcsfh...
  //init_rmdsf         ();
  
  /* If automatic oparation: set shotnumber=shot distributed by RMC*/
  if (argc>1) {
    shot=atoi(argv[2]);
    // prm_set("shotnumber", argv[2]);   //LBarrera
    // printf("Shot number distributed: %u",shot);  
  }
 
  init_autoshot      (shot);
  
  
  

  if (argc>1)   prm_main (argc, argv);
  else          prm_main (2, defaultarg);

  leave_coilunits ();

  return(0);
  }  /* main() */






/* ------  run in sync with experiment ------- */

static void do_autoshot (int shot) 
{
 
  char msg[1024];

  while(1)
    {
  /* If autoshot set (automatic oparation) set receivers=bt*/
     prm_set("receivers", "bt");   //LBarrera
     sprintf(msg, "Set receivers according to Bt (automatic mode)");
     log_msg(INFO, msg);

 /* set mode=automatic*/
     prm_set("mode", "automatic");   //LBarrera
     sprintf(msg, "Set RMC sfh in automatic mode");
     log_msg(INFO, msg);
  /* check status of MODSFH (RMC): 0 enable (automatic), -1 disable (manual)*/
     
      set_from_miracle (shot);
    }

} /*autoshot*/




#define GROUP "AUG"

static void init_autoshot(int shot)
{
  prm_group  (GROUP, "AUG synchronous operation"); 

  /*prm_define ("autoshot", NULL, NULL, 
  Change autoshot ->auto because there is a limit of character number
  in the editor of the sfh (to make radctrl compatible with RMC)
   */

  prm_define ("shotnumber", "(int)","0", 
              "shot number distributed by RMC",
              GROUP, NULL); 
  prm_define ("auto", NULL, NULL, 
              "wait for AUG shotnumbers, obtain Bt and set standard hardware settings",
              GROUP, do_autoshot(shot));  

}


/* end of main.c */
