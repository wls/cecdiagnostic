/* radctrl.h */


#ifndef uint16_t
#include <inttypes.h>
#endif

/* coilunits.c */
void  init_coilunits       (void);
void  leave_coilunits      (void);
int   coilunits_connect    (void);
void  coilunits_disconnect (void);
int   set_rad_switches     (int value);
int   get_rad_indicators   (int *value);

/* optgain.c */
void  init_optgain         (void);
void  set_gains            (struct RadiometerSettings *pRadSet);

/* radsettings.c */
void  init_radsettings     (void);
void  set_field            (float Bt);
void  set_from_miracle     (int shot);

/* rmcsfh.c */
struct GainSettings   /* per-channel DAQ settings */
{
  uint16_t ch_gain[MAX_CHAN];  /* index to tabulated gain codes and values  */
  uint16_t ch_offs[MAX_CHAN];  /* DAC code to generate correct offset */
};

int   calc_rmc_gains       (struct RadiometerSettings *pRadSet,
			    struct GainSettings *pGainSet);
int   update_rmc_sfh       (struct RadiometerSettings *pRadSet,
			    struct GainSettings *pGainSet);
void  init_rmcsfh          (void);

/* write_paramset.c */
int write_methods_sfh          (int diaref, 
			        struct RadiometerSettings *pRadSet);
int write_channel_settings_sfh (int diaref, 
			        struct RadiometerSettings *pRadSet);
