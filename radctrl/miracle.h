/* miracle.h */

struct preview_parameters 
  {
    int   shot;     /* shot number (last loaded shot) */
    char *DPFileName,   /* DP file name */
         *DPVersion;    /* DP version code */
    float Ipi,     /* plasma current (A) */
          ITF,     /* toroidal field coil current (A) */
          Bt,      /* toroidal field (T) */
          PNBI,    /* max. number of NBI sources */
          Ptot,    /* max. estimated auxiliary heating power */
          neGP;    /* max. requested density */
  };


int   miracle (struct preview_parameters *ppars);

