/* test_rmcgains.c 
   Test setting gains for RMC 
*/

#include <inttypes.h>
#include <string.h>

#include "libece.h"
#include "param.h"
#include "util.h"
#include "eceradiometer.h"
#include "radctrl.h"



struct RadiometerSettings RequestedSettings;


/* ............... command handlers ................ */ 

static void do_setsfh (PRM_HANDLE p)
{
  int c;

  struct GainSettings gs;

  /* test data */
  for (c=0; c<MAX_CHAN; ++c)
    {
      gs.ch_gain[c] = c % 7;
      gs.ch_offs[c] = 3900 - 10*c;
    }

  update_rmc_sfh(&RequestedSettings,&gs);

}  /* do_setsfh */






/* initalisation */

#define GROUP "test"
static  char   *defaultarg[2];

int main (int argc, char **argv)
  {

  defaultarg[0] = "test_rmcgains";
  defaultarg[1] = strdup("cmd");

  init_logging       ();
  init_coilunits     ();
  init_radiometer    ();
  init_radsettings   ();
  init_rmcsfh        ();

  prm_group  (GROUP, "test routines");

  prm_define ("setsfh", NULL, NULL, 
              "download gains to sfh",
              GROUP, do_setsfh);  

  if (argc>1)   prm_main (argc, argv);
  else          prm_main (2, defaultarg);

  return(0);
  }  /* main() */
