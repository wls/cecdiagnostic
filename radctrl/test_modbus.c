/* modbus unit test */

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

#include "modbus.h"


#define SLAVE    0x0


int main(void)
{
  /* uint8_t msg[MAX_MESSAGE_LENGTH]; */

        modbus_param_t mb_param;
	int i,j,k,ret, req[2]={ON, OFF};
        uint8_t coilstat[12], inpstat[12];


        /* open TCP connection */
        modbus_init_tcp(&mb_param, "128.1.1.10", 502);
	/*     modbus_set_debug(&mb_param, TRUE); */
      
        if (modbus_connect(&mb_param) == -1) {
                printf("ERROR Connection failed\n");
                exit(1);
        }


#ifdef REPORT_SLAVE_ID

	/* report slave ID */
        printf("Report slave id: ");
	ret = report_slave_id (&mb_param, SLAVE, msg);
        if (ret == 0) {
                printf("OK (no data returned)\n");
        } 
	else if (ret<0) {
	  printf("FAILED (error code %d, hex %x)\n", ret, ret);
                goto close;
        }
	else {
	  for (i=0; i<ret; ++i)
	    printf(" %02x ", msg[i]);
	  printf("\n");	  

	  for (i=11; i<ret; ++i)
	    printf("%c", (char) msg[i]);
	  printf("\n");	  
	}

#endif

	/* send a pattern */
	for (k=0; k<2; ++k)
	for (i=0; i<12; ++i)
	{
	  
	  ret = force_single_coil(&mb_param, SLAVE, i, req[k]);
          if (ret<0) {
	    printf("FAILED (error code %d, hex %x)\n", ret, ret);
            goto close;
	  }
	  sleep(1);


	  ret = read_coil_status(&mb_param, SLAVE, 0, 12, coilstat);
          if (ret<0) {
	    printf("FAILED (error code %d, hex %x)\n", ret, ret);
            goto close;
	  }
	  
	  printf("\nCoil status:  ");
	  for (j=0; j<12; ++j)
	    printf(" %1d", coilstat[j]);
	  printf("\n");

	  ret = read_input_status(&mb_param, SLAVE, 0, 12, inpstat);
          if (ret<0) {
	    printf("FAILED (error code %d, hex %x)\n", ret, ret);
            goto close;
	  }

	  printf("Input status: ");
	  for (j=0; j<12; ++j)
	    printf(" %1d", inpstat[j]);
	  printf("\n");
	  
        }


	/*	ret = force_multiple_coils(&mb_param, SLAVE, 0, 12, pat1); */

close:

        /* Close the connection */
        modbus_close(&mb_param);
        
        return 0;
}
