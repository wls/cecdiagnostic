/* miracle.c
   retrieve discharge parameter preview
   from SSR miracle server (via http, XML)

*/

#include <expat.h>
#include <stdio.h>
#include <string.h>

#include "http_lib.h"
#include "miracle.h"
#include "util.h"


/* This is where the miracle lives: */
//#define HTTP_SERVER "ssrpc4" retired
#define HTTP_SERVER "SSR-Mir"
#define HTTP_PORT   9090
#define HTTP_FILENAME "Miracle/ShotPreviewFS.xml"
// What does DCS3 stand for - the file is located at /Miracle/


#define BUFFSIZE 8192

/* expat parser <-> handler communication */
struct ParseContext
{
  int Depth;               /* indentation level */
  char valbuf[BUFFSIZE];   /* collects character data */
  int  valptr;             /* where to write in valbuf */
  int  invalue;            /* 1: inside a value element, recording data */
  int  afterwd;            /* 1: have 1 point after the segment watchdog time */
  struct preview_parameters *ppars;   /* the shot program to generate */
};


/* expat handlers */
static void start    (void *data, const char *el, const char **attr);
static void end      (void *data, const char *el);
static void charhndl (void *data, const XML_Char *s, int len);



int   miracle (struct preview_parameters *ppars)
{

  /* variables for http_lib */
  int  ret, lg;
  char typebuf[70];
  char msg[256];
  char *data=NULL, *filename=HTTP_FILENAME;

  /* variables for expat XML parser */
  struct ParseContext context;
  int done = 1;  /* true, i.e. parse in one go */


/* ---------- read preview xml file from Miracle server ------- */

  http_server = HTTP_SERVER;
  http_port = HTTP_PORT;

  ret = http_get(filename, &data, &lg, typebuf);
  if (ret!=200 && ret !=201) goto ret0;
  ret=0;

  sprintf(msg, "http_lib: res = %d, type='%s', lg=%d\n", ret, typebuf, lg);
  log_msg(DEBUG, msg);
  /*  fwrite (data,lg,1,stdout); */


/* --------- parse XML to extract discharge parameters -------- */

/* setup context variable for parser */
  context.ppars = ppars;
  context.valptr = 0;
  context.invalue = 0;

/* initialise XML parser */
  XML_Parser p = XML_ParserCreate(NULL);
  if (! p) {
    fprintf(stderr, "Miracle: Couldn't allocate memory for parser");
    ret= -1; goto ret0;
  }

  XML_SetElementHandler(p, start, end);
  XML_SetCharacterDataHandler(p, charhndl);
  XML_SetUserData(p, (void *) &context);

  if (XML_Parse(p, data, lg, done) == XML_STATUS_ERROR)
  {
    char msg[256];
    sprintf(msg, "Miracle parse error at line %d: %s",
              (int) XML_GetCurrentLineNumber(p),
              XML_ErrorString(XML_GetErrorCode(p)));
    log_msg(ERROR, msg);
    ret = -1; goto ret0;
  }
  XML_ParserFree(p);
 
ret0:
  if (data) free(data);
  return ret;
   
}  /* miracle() */





/* ---------------------  expat handlers ---------------- */

/* start element handler */

static void
start(void *data, const char *el, const char **attr)
{
  struct preview_parameters *ppars;

  if (!data)
  {
    log_msg(ERROR, "Miracle: Start handler called without context data");
    exit(-1);
  }

/* evaluate attributes of "Entry", this is all we need */

  if ( strcmp(el, "Entry")==0 
       && attr[0] && strcmp(attr[0],"name")==0 
       && attr[1] 
       && attr[2] && strcmp(attr[2],"value")==0 
       && attr[3] && strcmp(attr[3],"UNDEFINED")!=0 )
  {
    if (!(ppars=((struct ParseContext *) data)->ppars))
    {
      log_msg(ERROR, "Miracle: Context data lacks parameters pointer");
      exit(-1);
    }

    if      ( strcmp(attr[1],"Shot")==0 )     ppars->shot = atoi(attr[3]);
    else if ( strcmp(attr[1],"FileName")==0 ) ppars->DPFileName = strdup(attr[3]);
    else if ( strcmp(attr[1],"Version")==0 )  ppars->DPVersion = strdup(attr[3]);
    else if ( strcmp(attr[1],"ITF")==0 )      ppars->ITF = atof(attr[3]);
    else if ( strcmp(attr[1],"Bt")==0 )       ppars->Bt = atof(attr[3]);
    else if ( strcmp(attr[1],"max Ipi")==0 )  ppars->Ipi = atof(attr[3]);
    else if ( strcmp(attr[1],"max Ptot")==0 ) ppars->Ptot = atof(attr[3]);
    else if ( strcmp(attr[1],"max neGP")==0 ) ppars->neGP = atof(attr[3]);    
  }

}  /* start handler */


/* end element handler (not used) */

static void end(void *data, const char *el)
{
} /* end */


/* character data handler (not used) */

static void  charhndl (void *data, const XML_Char *s, int len)
{
} /* charhndl */

