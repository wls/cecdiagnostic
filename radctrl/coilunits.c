/* coilunits.c - coil control units 
   currently: MODBUS Acromag 983EN */


#include <memory.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "libece.h"

#include "param.h"
#include "util.h"

#include "radctrl.h"
#include "modbus.h"


#define GROUP "coilunits"

#define MODBUS_TCP_PORT 502
#define SLAVE    0x0

#define MAX_COILUNITS 4

/* global flag */
static int connected = 0;


static struct CoilUnits
  {
    int connected;
    char *addr;
    modbus_param_t mb_param;
  } coilunits[MAX_COILUNITS];


/* connect / disconnect */

void  coilunits_disconnect (void)
{
  int i;
  for (i=0; i<MAX_COILUNITS; ++i)
    if (coilunits[i].connected)
      {
	char msg[128];
	sprintf(msg, "Disconnecting %s", coilunits[i].addr);
	log_msg(DEBUG, msg);

	modbus_close(&(coilunits[i].mb_param));

	coilunits[i].connected = 0;
      }
  connected = 0;
}  /* coilunits_disconnect */



int coilunits_connect (void)
{
  PRM_HANDLE p;
  int modbus_debug = FALSE, type, i;
  char *val, *addr;

  coilunits_disconnect();

  p = prm_get("modbus_debug"); 
  if (prm_getval(p, &val, &type) && strcmp(val, "on")==0)
    modbus_debug = TRUE;

  p = prm_get("tcpaddr"); i=0;
  while (prm_getval(p,  &addr,  &type))
  {
    if (i>=MAX_COILUNITS)
    {
      char msg[128];
      sprintf(msg, "Cannot connect to %s (space reserved for max. %d connections)", 
	      addr, MAX_COILUNITS);
      log_msg(ERROR, msg);
      return(-1);
    }

    char msg[128];
    sprintf(msg, "Connecting to %s", addr);
    log_msg(DEBUG, msg);

    /* open TCP connection */
    modbus_init_tcp(&(coilunits[i].mb_param), addr, MODBUS_TCP_PORT);

    /* enable debugging if requested */
    modbus_set_debug(&(coilunits[i].mb_param), modbus_debug); 

    if (modbus_connect(&(coilunits[i].mb_param)) == -1) 
    {
      char msg[128];
      sprintf(msg, "Cannot make TCP connection to %s", addr);
      log_msg(ERROR, msg);
      return(-2);
    }

    coilunits[i].addr = addr;
    coilunits[i].connected = 1;
    ++i;
  }

  connected = 1;
  return(0); 
}  /* coilunits_connect */



/* get coil status (12 bits in one integer) for a single unit */
static int get_coil_status (int unitno, int *status)
{
  uint8_t coilstat[12];
  char msg[128];
  int ret, j;

  if (!connected && coilunits_connect())
    return(-1);

  if (unitno<0 || unitno>= MAX_COILUNITS)
  {
    sprintf(msg, "Unit %d outside available range (1 .. %d)", 
	    unitno+1, MAX_COILUNITS);
    log_msg(ERROR, msg);
    return(-2);
  }

  if (!coilunits[unitno].connected)
  {
    sprintf(msg, "Unit %d not connected", unitno+1);
    log_msg(ERROR, msg);
    return(-3);
  }

  ret = read_coil_status(&(coilunits[unitno].mb_param), SLAVE, 0, 12, coilstat);
  if (ret<0) 
  {
    sprintf(msg, "Cannot read coil status of unit %s, error %d (%x)", 
	    coilunits[unitno].addr, ret, ret);
    log_msg(ERROR, msg);
    return(-4);
  }

  ret = 0;
  for (j=11; j>=0; --j)
    ret = (ret<<1) | (coilstat[j] && 1);

  if (status) *status = ret;
  return(0);
}  /* get_coil_status */



/* get input status (12 bits in one integer) for a single unit */
static int get_input_status (int unitno, int *status)
{
  uint8_t inpstat[12];
  char msg[128];
  int ret, j;

  if (!connected && coilunits_connect())
    return(-1);

  if (unitno<0 || unitno>=MAX_COILUNITS)
  {
    sprintf(msg, "Unit %d outside available range (1 .. %d)", 
	    unitno+1, MAX_COILUNITS);
    log_msg(ERROR, msg);
    return(-2);
  }

  if (!coilunits[unitno].connected)
  {
    sprintf(msg, "Unit %d not connected", unitno+1);
    log_msg(ERROR, msg);
    return(-3);
  }

  ret = read_input_status(&(coilunits[unitno].mb_param), SLAVE, 0, 12, inpstat);
  if (ret<0) 
  {
    sprintf(msg, "Cannot read input status of unit %s, error %d (%x)", 
	    coilunits[unitno].addr, ret, ret);
    log_msg(ERROR, msg);
    return(-4);
  }

  ret = 0;
  for (j=11; j>=0; --j)
    ret = (ret<<1) | (inpstat[j] && 1);

  if (status) *status = ret;
  return(0);
}  /* get_input_status */



/* set individual coil */
static int set_coil (int unitno, int coilno, int status)
{
  char msg[128];
  int ret;

  if (!connected && coilunits_connect())
    return(-1);

  if (unitno<0 || unitno>=MAX_COILUNITS)
  {
    sprintf(msg, "Unit %d outside available range (1 .. %d)", 
	    unitno+1, MAX_COILUNITS);
    log_msg(ERROR, msg);
    return(-2);
  }

  if (coilno<0 || coilno>=12)
  {
    sprintf(msg, "Coil number %d outside available range (1 .. 12)", coilno+1);
    log_msg(ERROR, msg);
    return(-3);
  }

  if (!coilunits[unitno].connected)
  {
    sprintf(msg, "Unit %d not connected", unitno+1);
    log_msg(ERROR, msg);
    return(-4);
  }

  ret = force_single_coil(&(coilunits[unitno].mb_param), SLAVE, coilno, status);
  if (ret<0) 
  {
    sprintf(msg, "Cannot set status of unit %d (%s) coil %d, error %d (%x)", 
	    unitno+1, coilunits[unitno].addr, coilno+1, ret, ret);
    log_msg(ERROR, msg);
    return(-5);
  }

  return(0);
}  /* set_coil */



/* set all coils of one unit at once (bits 0..11 of "value") 
   and check if actually set */
static int set_multiple_coils (int unitno, int value)
{
  int ret, coilstat, i;

  /* set, one bit at a time */
  for (i=0; i<12; ++i)
  {
    if (value & (1<<i))
      ret = set_coil (unitno, i, ON);
    else
      ret = set_coil (unitno, i, OFF);
    if (ret) return (-1);
  }   

  /* check back */
  if (get_coil_status(unitno, &coilstat))
    return(-2);
  
  if ((coilstat & 0xFFF)!=(value & 0xFFF))
  {
    char msg[128];
    sprintf(msg, "Coil (unit %d, %s) data written: %03X received: %03X", 
	    unitno+1, coilunits[unitno].addr, value, coilstat);
    log_msg(ERROR, msg);
    return(-3);
  }

#ifdef MAYBE_NEEDED_LATER
  else
  {
    char msg[128];
    sprintf(msg, "Coil (unit %d, %s) data written and received: %03X", 
	    unitno+1, coilunits[unitno].addr, value);
    log_msg(DEBUG, msg);
  }
#endif

  return(0);
}  /* set_multiple_coils */



/* ............ exported entries .................. */


/* set all switches at once, using a 24 bit word */

int set_rad_switches (int value)
{
  if (set_multiple_coils(0, value & 0xfff)) 
    return(-1);
  if (set_multiple_coils(1, (value>>12) & 0xfff)) 
    return(-1);

  /* disable outputs on read-back units */

  /*
  if (set_multiple_coils(2, 0))
    return(-1);
  if (set_multiple_coils(3, 0))
    return(-1);
  */

  return(0);
}


/* get indicator data (24 bit) */

int get_rad_indicators (int *value)
{
  int inp1, inp2;

  if (get_input_status(2, &inp1)) 
    return(-1); 
  if (get_input_status(3, &inp2)) 
    return(-1); 
  
  if (value) 
    *value = (inp1 & 0xFFF) | (inp2 & 0xFFF)<<12;

  return(0);
}




/* ............ command handlers ................... */ 

static void do_connect(PRM_HANDLE p)
{ coilunits_connect(); }

static void do_disconnect(PRM_HANDLE p)
{ coilunits_disconnect(); }


static void print_iostatus(PRM_HANDLE p)
{
  int i;
  if (!connected && coilunits_connect())
    return;

  for (i=0; i<MAX_COILUNITS; ++i)
    if (coilunits[i].connected)
      {
	int  inp, coilstat;
	char msg[128];
	
	if (get_coil_status(i, &coilstat)) break;
	if (get_input_status(i, &inp)) break;

	sprintf(msg, "Control unit %d; coils = %03X input = %03X", i+1, coilstat, inp);
	log_msg(INFO, msg);
      }
} /* print_status */




static void do_setcoil (PRM_HANDLE p)
{
  char *valstr, msg[80];
  int type, unitno, coilno, value;

  if (!p || !prm_getval(p, &valstr, &type)) 
  {
    log_msg(ERROR, "Missing unit number (Usage: setcoil unitno coilno value)");
    return;
  }
  unitno = atoi(valstr);

  if (!p || !prm_getval(p, &valstr, &type)) 
  {
    log_msg(ERROR, "Missing coil number (Usage: setcoil unitno coilno value)");
    return;
  }
  coilno = atoi(valstr);

  if (!p || !prm_getval(p, &valstr, &type)) 
  {
    log_msg(ERROR, "Missing value (Usage: setcoil unitno coilno value)");
    return;
  }
  if (strcmp(valstr, "on")==0) value = ON;
  else                         value = OFF;

  if (!connected && coilunits_connect())
    return;

  sprintf(msg, "Setting unit=%d coil=%d to %s", unitno, coilno, valstr);
  log_msg(DEBUG, msg);

  set_coil (unitno-1, coilno-1, value);

} /*do_setcoil*/





/* initialisation */

void init_coilunits     (void)
{
  memset(&coilunits,   0, sizeof(coilunits));


  prm_group  (GROUP, "Coil control units");

  prm_define ("connect", NULL, NULL, 
              "establish connection to all peripheral units",
              GROUP, do_connect);
  prm_define ("disconnect", NULL, NULL, 
              "break connection to all peripheral units",
              GROUP, do_disconnect);
  prm_define ("iostatus", NULL, NULL, 
              "obtain and print coils outputs and status inputs",
              GROUP, print_iostatus);
  prm_define ("setcoil", "(integer),on,off", "", 
              "set individual output bit: unitno(1..4) coil(1..12) value(on,off)",
              GROUP, do_setcoil);
  prm_define ("tcpaddr", "(string)", "radctrl1,radctrl2,radctrl3,radctrl4", 
              "TCP addresses or node names of MODBUS units",
              GROUP, NULL);
  prm_define ("modbus_debug", "on,off", "off", 
              "When connecting, enable debug messages from MODBUS driver",
              GROUP, NULL);
}


/* termination */

void leave_coilunits(void)
{
  if (connected)
    coilunits_disconnect();
}


/* end of coilunits.c */

