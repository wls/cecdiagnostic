/*
   shots.c - handle request for a series of ASDEX Upgrade plasma shots
             (shot numbers explicitly entered or remotely distributed)

	     this is a subprogram useful for diagnostic evaluation 
	     programs to loop through any number of shot files

   uses param.c

   Wolfgang Suttrop, wls@ipp.mpg.de

   16-Jun-94	wls	new
   24-Apr-95	wls	option: fork() new process for each single shot
   25-Mar-2010  wls     simplified (no waiting for shot# nor diagnostics)
*/


#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "param.h"
#include "util.h"


#define VERBOSE


/* ---- static variables ---- */

static void (*shot_hook)(int shot) = NULL;    
/* pointer to function that handles single AUGD shot */


/* ------- process a single shot with given shot#  ------- */

static void single_shot (int shot)
{
  if (!shot_hook) return;
  (*shot_hook)(shot);
}	/*single_shot()*/



/* ------- handle request for a number of shots 
     (this will be called from inside params.c) ------- */

static void do_shots(PRM_HANDLE p)
  {
  int  type, s1, s2;
  char *shots, *cp2;

/* parse arguments and run the requested shot #'s */

  while (prm_getval(p, &shots, &type)) {

    switch (type) {

      case STRING:    /* expect shot # range: (integer)-(integer)  */

        if (shot_hook && isdigit(*shots)) {
          cp2=shots;
          while (isdigit(*cp2)) ++cp2;
	  single_shot(s1=atoi(shots));   /* 1st shot of range */
          if (*cp2=='-' && isdigit(* ++cp2)) {
            s2 = atoi(cp2);
            while (s1<s2) single_shot(++s1); /* other shots */
	    }
 	  }
	break;

      }

    }  /*while prm*/
  } /* do_shots*/



/* --------- initialize module --------------------- */


void init_shots (void (*shot_proc)(int shot)) 
{
  shot_hook = shot_proc;

  prm_group  ("shots", "commands to process entire shots");

  prm_define ("shot", "online,(string)", "", 
  "process shot, shot range (1st-last), or wait for online distribution",
              "shots", do_shots);
}


/* end of shots.c */
