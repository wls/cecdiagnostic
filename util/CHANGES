CHANGES for "ece2"
ASDEX Upgrade ECE radiometer software version 2

---------------------------------------------------------

CEC level-1 diagnostic data evaluation
(Version 2)

directory: cec/

08-Jun-2010  wls

Main changes vs. "ece1" as used from 1994-2009
(CEC, CED analysis version 1):

Simplifications compared with "ece1"
- No plotting
- Only one level-1 diagnostic is made at a time,
  i.e. either CEC or CED.
- Only one signal group is made (Trad-A).
  Fast signals go into a different diagnostic (CED)
  or edition 
- 4 "fast" RAD channels (SI-MI-E, SI-MI-F) ignored
- polychromator (MUM, multi-unit monochromator) 
  no longer supported (also: no chopper detection
  and baseline correction as this is only needed for MUM)
- Event detection not implemented (for now)
- Raw shot file signal groups statically defined
  (i.e. no dynamic parsing like in ece1pull.c)
- Evaluated time range determined only in "samplerate"
  (separate "tmin", "tmax" parameters dropped)
- Additional plasma parameters (Bt, Ip, etc.)
  not obtained anymore - not really needed
- no longer bound to libs: ecesf, eceusr, ececalc
  (required functionality now included in "util/" directory)
- simplified calibration subroutine, 
  only 1 "channel", no frequency interpolation
- Channels are no longer sorted by frequency, i.e. CEC channel
  numbers strictly correspond to hardware channel numbers


Improvements over "ece1"
- single program for RAD and RMA/RMB raw data
- multi-platform   (Sparc/x86; Solaris/Linux)
- compiles with all warnings on (-Wall)
- more consistent logging of errors, warnings,
  info and debug messages
- use (and rely on) ANSI-C: 
  * prototypes (including libddww headers)
  * enumerations
  (however still not C++)
- avoid static variables - code should be thread-safe (I think)


Changes in CEC, CED level-1 shot files:
- Dropped Trad-B, R-B, z-B, parms-B  (leaves only 1 signal group, "*-A")
- Dropped val-* validation trace
- "methods" parameter set now renamed "METHODS" (to match level-0 RAD shot file)
- Dropped "methods" parameters:
  CALIBRTN            always "4" (hot/cold source calibration)
  BSLNCOR, CHOPPER    Polychromator no longer supported 
  DENS-CO, OPTTHIN    not even pretended to check optical depth, cut-off
  F, DF               frequencies always from nominal RF frequencies
  R, dR, Z, dZ        no time-independent positions any more
  RZ_SRC, EQED        equilibrium diagnostic now a predecessor
- Added "METHODS" parameters:
  ZLENS	              lens position (m)
  IFGROUPS	      #receivers (front ends and IF groups) active (0..3)
  SIDEBAND            for each receiver: 'U' (USB), 'L' (LSB) or ' ' (no signal)
  WAVEGUID	      waveguide number for each receiver
  FREQLO	      LO frequency (Hz) for each receiver
  ATTNIF	      IF attenuation (dB) for each receiver
- Dropped "parms-A" parameters:
  Rs, zs              no time-independent positions any more
- Added "parms-A" parameters:
  IFGROUP	      #of IF group (receiver) each channel is connected to (0..3)
  calfact	      calibration factor (eV/V) used for each channel
  

-------------------------------------------------

radctrl - radiometer hardware control
(Version 2)

08-Jun-2010  wls

- revised version of original "radctrl" software
- interfaces to Acromag MODBUS I/O modules
- uses MODBUS software "library" in modbus.c




-------------------------------------------------
