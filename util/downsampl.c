/*
       downsampl.c - sample rate reduction for signals

       Low-pass filtering by symmetrical convolution with a tabulated sinc function 

       Sampling frequency can be reduced by factors
       which are integer powers of 2. This allows to use a single
       fixed kernel table for all reduction factors.
       Any other target sample rate will be rounded up.

       Wolfgang Suttrop,   Suttrop@ipp.mpg.de

       28-Oct-1994  new
       25-Mar-2010  wls  modernised
       02-Aug-2011  wls  "offset" needs to be "double"
       7-Mar-2013  lbarrera Change algorithm to find out if the signal is inverted or not (prvious one fails for RMD)
       11-Mar-2013 lbarrera get offset value in soubrutine get_offset_raw.
       19-Mar-2013 lbarrera change UMAX (RMC signal -10-->+10V, RAD signal -5-->+5V)
       03-Dec-2023 change offset tmax to +0.01s to have values for the new DAQ
Instructions:

There are 3 principal functions to call in order to process a complete
signal group:

1. Prior to actual filtering, the oversampling filter parameters
   are determined by a call to

   set_reduction()

   This function returns the number of data points after sample
   rate reduction. This information is required by the calling program
   to dynamically allocate the heap space required.

2. Then, the sample rate-reduced timebase is constructed by
   calling

   reduce_timebase()

3. Subsequently, each signal of the signal group is filtered by
   one call per signal (channel) to

   reduce_signal()

*/



#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "libece.h"
#include "param.h"
#include "util.h"

/* convolution kernel */
#include "coeff.c"

#define	GROUP	"downsampling"
#define	P_SAMPLERATE "samplerate"


#define OFFSET_TMAX  0.01  
#define OFFSET_TSIG  1.7


/* #define VERY_VERBOSE */
#define VERBOSE

#define EPSILON	 0.02
#define UMAX 9.98	/* Volts max. ADC input before overdrive detected --Definition in rawsignal.c*/

/* linked list of intervals with equidistant sampling */







/* ========= manage time segment list =========== */

/* create new time segment */

static struct timeseg *new_tseg() 
{
  struct timeseg *t;
  if (!(t = (struct timeseg *) calloc(1, sizeof(struct timeseg)))) 
  {
    log_msg(ERROR, "downsample.c: Not enough heap space to allocate time segment list."); 
    return(NULL);
  }
  return(t);
}	/* new_tseg*/




/* print time segment list for diagnostic purposes */

void print_tseglist(struct SampleRateSettings *srs, double *time) 
{
  struct timeseg *t;
  int	ia=0;

  if (!srs) return;
  /*  log_msg(DEBUG, "time segment list:"); */

  for (t = srs->tseglist; t; t=t->next) 
  {
    char msg[256];
    sprintf(msg, "  %9d-%9d: t=%7.4lf s - %7.4lf s  t_s= %6.2lf us red=%d",
	   ia, t->istop, time[ia], time[t->istop],
	   t->t_sample, t->red);
    log_msg(DEBUG, msg);
    ia = t->istop+1;
  }
}	/*print_tseglist*/



/* .................. clean up ................ */

void  free_sr_settings (struct SampleRateSettings *srs)
{
  struct timeseg *t, *t1;
  t = srs->tseglist;   /* remove time segment list */
  while (t) 
  {
    t1=t; t=t->next;
    free(t1);
  }
  srs->tseglist = NULL;
}



/* -----------  functionality soubroutines ----------- */

/* hierarchy:

                      set_reduction()
		             |
	  +------------------+------------------+
          |		     |			|
	  v		     v			v
   analyse_timebase()	 parse_srr()	 get_redsize()
                             |
	                     v
		      parse_interval()
                      define_interval()

*/


/* analyse a given timebase, create time segment list
   and enter sampling intervals into the list */


static	void	analyse_timebase (struct SampleRateSettings *srs, 
				  double *time, int ntime)
{
  struct  timeseg  *t, *t1;
  double  *tp, *ep, d, d1;
  int	  ia;

  if(!srs) return;

/* find index range for offset subtraction */
  srs->inull = ceilindex(time, OFFSET_TMAX, ntime);
  if (srs->inull>=ntime) srs->inull=0;

/* find index range to obtain average and find if the signal is inverted or not */
  srs->iaver_sig = ceilindex(time, OFFSET_TSIG, ntime);
  if (srs->iaver_sig>=ntime) srs->iaver_sig=0;

/* clear existing time segment list, if any */
  free_sr_settings(srs);

/* scan timebase and separate equidistant portions */
  tp=time; ep=time + ntime -1;

  while (tp<ep) 
  {
/* create new time segment */
    t = new_tseg();
    if (!t) return;
    ia = tp-time;

/* find end of equidistant time range */
    d = *(tp+1) - *tp;
    do 
    {
      d1=(*(tp+1) - *tp) / d;
      ++tp;
    } while ( tp<ep  &&  d1>1.0-EPSILON  &&  d1<1.0+EPSILON );

    t->istop = tp++ -time;
    t->t_sample =  1E6 * ( time[t->istop] - time[ia] )
                  / (t->istop - ia) ;

/* append new element at list tail */
    if (srs->tseglist)	{ t1->next = t; t1 = t; }
    else		  t1 = srs->tseglist = t;

  }	/* while (tp!=ep) */

}	/*analyse_timebase*/




/* ------------------------------------------------
   enter time interval from t1 [s] to t2 [s] with a
   sampling frequency of fs [kHz] into linked list
   ------------------------------------------------ */

static	int	define_interval (struct SampleRateSettings *srs,
				 double *time, int ntime, 
				 double t1, double t2, double fs)
  {
  struct timeseg  *t, *tn;
  int	 ia, ie, iia, iie, ib=0, rf, red, ks;

/* get time interval indices */

  ia = floorindex (time, t1, ntime);
  ie = ceilindex  (time, t2, ntime);

/* skip time segments that do not intersect [t1,t2] */

  for (t=srs->tseglist; t && ia > t->istop; t=t->next) ib = t->istop + 1;

/* go through all (partially) covered segments */
   
  while (t && ia < ie && ia <= t->istop) {

/* determine reduction factor */

    if (t->t_sample && fs>0.0) {
      rf = (int) (1.0E3 / (t->t_sample * fs) );
      if (rf > MAXRED) rf = MAXRED;  //MAXRED defined in util/coeff.c
      red = 1;
      while (rf >>= 1) red <<= 1;    /* currently restrict to powers of 2 */
      }
    else red = 0;	/* skip interval */

/* check if convolution exceeds index range. 
   If so, restrict interval */

    ks = NC/2*red;	/* kernel size */
    iia = ia;
    if (iia < ks) iia = ks;
    iie = ie;
    if (iie+ks >= ntime) iie = ntime -ks -1;

/* insert [segment-start, t1] if not empty */

    if (t && iia > ib) {
      tn=t;
      if (!(t=new_tseg())) return(-1);
      t->next = tn->next;
      t->istop = tn->istop;
      t->t_sample = tn->t_sample;
      t->red = tn->red;
      tn->next = t;
      tn->istop = iia-1;
      }

/* append remainder [t2,segment-end] segment if needed */

    if (ia < iie) {
      if (iie < t->istop) {
        if (!(tn=new_tseg())) return(-1);
        tn->next = t->next;   t->next = tn;
        tn->istop = t->istop; t->istop = iie;
        tn->t_sample = t->t_sample;
        tn->red = t->red;
        }
      t->red = red;
      }
    ia = ib = t->istop+1;
    t = t->next; 
    }	/*while*/

  return(0);
  }	/*define_interval*/




/* ------------------------------------------
   go through user defined sampling intervals 
   ------------------------------------------ */

/* parse user command `s' for time interval
   t10 .. t20 and enter into time segment list */

static	void	parse_interval (s, t1, t2, fs)
  char	*s;
  double *t1, *t2, *fs;
  {
  char	*c;

  if ((c=strchr(s, '/'))) {	/* interval start */
    *c = '\000';
    *t1= (double) atof(s);
    *c = '/';
    s = c+1;	/* skip '/' */
    }
  else *t1=0.0;

  if ((c=strchr(s, ':'))) {	/* interval stop */
    *c = '\000';
    *t2= (double) atof(s);
    *c = ':';
    s = c+1;	/* skip ':' */
    }
  else *t2 = 0.0;

  *fs = (double) atof(s);	/* sample rate */

  }	/*parse_interval*/




/* go through command list and parse event codes.
   call parse_interval for each event trigger */

static	int	parse_srr (struct SampleRateSettings *srs,
			   int shot, double *time, int ntime) 
  {
  PRM_HANDLE p;
  double t10, t20, fs;
  char	*s, msg[254];
  int	type;

#ifdef PARSE_EVENTCODES
  double td, t1, t2;
  float Ip, PECRH;
  char	*c;
  int   needdisr=0, disr=0;
#endif
        

/* find out what kinds of event we need to detect */

#ifdef PARSE_EVENTCODES
  p = prm_get(P_SAMPLERATE);
  while (prm_getval(p, &s, &type)) {
    if (strstr(s, "DISR") || strstr(s, "disr") ||
        strstr(s, "IP") || strstr(s, "ip") || strstr(s, "Ip")) 
        needdisr=1;
    }
  if (needdisr) {
    disr = detdisr(shot, "AUGD", &td, &Ip);
    if (disr < -1) return (-1);	/* severe error */
    }
#endif


/* go through samplerate specifiers and construct timebase */
  log_msg(INFO, "Samplerate specifications:");

  p = prm_get(P_SAMPLERATE);
  while (prm_getval(p, &s, &type)) {


/* parse event codes and determine event triggers: ... */

#ifdef PARSE_EVENTCODES

    if ((c=strchr(s, '?'))) 
    {
      *c = '\000';

/* ................ ECRH ? .................. */

      if (!strcmp(s, "ECRH") || !strcmp(s, "ecrh")) { 
        if (detecrh(shot, "AUGD", &t1, &t2, &PECRH) > 0) {
#ifdef VERBOSE
          fprintf(stderr, "ECRH (%5.0f kW max.) %6.4f .. %6.4f s\n", 
		  PECRH*1E-3, t1, t2);
#endif
          parse_interval (c+1, &t10, &t20, &fs);
          define_interval (time, ntime, t1+t10, t2+t20, fs);
          }
        }

/* .............. disruption? .............. */

      else if (!strcmp(s, "DISR") || !strcmp(s, "disr")) { 
        if (disr) {
#ifdef VERBOSE
          fprintf(stderr, "DISR %6.4f s\n", td);
#endif
          parse_interval (c+1, &t10, &t20, &fs);
          define_interval (time, ntime, td+t10, td+t20, fs);
          }
        }

/* ....... non-vanishing plasma current? ..... */

      else if (!strcmp(s, "IP") || !strcmp(s, "Ip") || 
	       !strcmp(s, "ip")) { 
#ifdef VERBOSE
        fprintf(stderr, "IP   %6.4f s\n", td);
#endif
        parse_interval (c+1, &t10, &t20, &fs);
        define_interval (time, ntime, t10, td+t20, fs);
        }

/* .......... ELMy H-mode? ....................*/

      else if (!strcmp(s, "HMOD") || !strcmp(s, "hmod")){ /* H mode? */
        parse_interval (c+1, &t10, &t20, &fs);
        if (init_hmod (shot, "AUGD")==0) {
          while (det_hmod (&t1, &t2) > 0) {
#ifdef VERBOSE
            fprintf(stderr, "HMOD %6.4f .. %6.4f s\n", t1, t2);
#endif
            define_interval (time, ntime, t1+t10, t2+t20, fs);
            }
          close_hmod();
	  }
        }


/* ......... anything unknown to us? ........... */

      else 
      {
	sprintf(msg, "samplerate: Unknown downsampling event %s", s);
        log_msg(ERROR, msg);
	continue;
      }

      *c = '?';
    }

    else 

#endif

/* no event code: absolute time! */
    {
      parse_interval (s, &t10, &t20, &fs);
      define_interval (srs, time, ntime, t10, t20, fs);

      sprintf(msg, "     %6.4f .. %6.4f s: %6.2f kHz", t10, t20, fs);
      log_msg(INFO, msg);
    }
   //Save information of the settings:
   // srs->info=msg;

  }	/* while getprm */

  return(0);
}	/*parse_srr*/



/* determine the size of sample rate-reduced signals */

static	int	get_redsize(struct SampleRateSettings *srs) 
{
  struct timeseg *t;
  int	len=0, ia=0, ie, r;

  if(!srs) return(0);

  for (t=srs->tseglist; t; t=t->next) 
  {
    ie = t->istop;
    if ((r=t->red))
      while (ia <= ie) {++len; ia+=r;}
    else
      ia = ie + 1;
  }
  return(len);
}


/* determine reduction parameters and return reduced signal length */

int	set_reduction (struct SampleRateSettings *srs,
		       int shot, double *time, int ntime)
{
  analyse_timebase(srs, time,ntime);
  log_msg(DEBUG, "Original time base equidistant segments:");
  print_tseglist  (srs, time);

  if (parse_srr(srs, shot, time, ntime)) return(0);
  log_msg(DEBUG, "Reduced time base downsampling segments:");
  print_tseglist  (srs, time);

  return(get_redsize(srs));
}	/*set_reduction*/



/* ---------- build sample rate-reduced time base ---------- */

void	reduce_timebase(struct SampleRateSettings *srs, 
			double *time, double *rtime)
{ 
  struct timeseg *t;
  double *s, *e;
  int	r;

  if (!srs) return;

  s=time;
  for (t = srs->tseglist; t; t=t->next) 
  {
    e = time + t->istop;
    if ((r=t->red))
      while (s <= e) {*rtime++ = *s; s += r;}
    else 
      s = e + 1;
  }    
}	/*reduce_timebase*/




/* ............. apply oversampling filter to signal trace  ....... */


void	reduce_signal (struct SampleRateSettings *srs, 
		       float *sig, float *rsig)
{ 
  struct timeseg *t;
  float  *s, *e,	/* pointer to signal trace */
         *s1, *s2,	/*    "    "    */
	 *cp, *ce,	/* pointer to coefficient array */
         norm, sum;	/* intermediate results */
	 
  double offset=0.0,	/* offset to remove (average over 0..inull-1) */  //Change double for float
  	averageinit=0.0; /*Average from t=OFFSET_TMAX to OFFSET_TSIG (0 to 1 sec)*/
  int	 sign=0,	/* channel sign 0=non-inverted, 1=inverted */
         il, 		/* interleave factor */
         r;

  if (!srs) return;

/* calculate offset in raw signal (average for t<OFFSET_TMAX) */

  e = (s=sig) + srs->inull;
  while (s<e) offset += *s++;
  if ((r=(e-sig))) offset /= r;
  sign = (offset > 2.0);     /* Volts (some channels have inverting cables) */
  
/* calculate average form OFFSET_TMAX to OFFSET_TSIG  (To check if the signal is inverted or not. The algorithm used to calculate it for RAD signals is not valid for RMC, as the baseline level of the RMC signal can be programmed to any value...*/

  e = (s=sig) + srs->iaver_sig;
  while (s<e) averageinit += *s++;
  if ((r=(e-sig))) averageinit /= r;
  sign = (offset> averageinit);     /* Volts (some channels have inverting cables) */
  

 /* e = (s=sig) + srs->inull;
  while (s<e) offset += *s++;
  if ((r=(e-sig))) offset /= r;
  sign = (offset > 2.0);     */ /* Volts (some channels have inverting cables) */



/* now go through signal */

  s=sig;
  ce = coeff + NCOEFF +1;

  for (t = srs->tseglist; t; t=t->next) 
  {
    e = sig + t->istop;

/* treat trivial cases first */

    if (!(r=t->red)) /* skip section */
    {		
      s = e+1; 
      continue;
    }

    if (r==1)   /* copy section */
    {			
      if (sign)
        while (s<e) 
	  if (*s < -UMAX || *s > UMAX) {*rsig++ = 0.0; ++s;}
	  else *rsig++ = offset - *s++;
      else
        while (s<e)
	  if (*s < -UMAX || *s > UMAX) {*rsig++ = 0.0; ++s;}
	  else	*rsig++ = *s++ - offset;
      continue;
    }

/* set table lookup interleave factor according to cut-off frequency */

    il = 2*MAXRED / r;

/* calculate norm */

    norm = *coeff;
    for (cp=coeff+il; cp<=ce; cp+=il)	norm += 2 * *cp;

/* filter by time domain symmetrical convolution */

    if (sign)		/* if clause moved out of loop for performance */
      for ( ; s<=e; s+=r) 
      {
	if (*s < -UMAX || *s > UMAX) 
	  {*rsig++ = 0.0;  /*overdrive*/
	  }
	else 
	{
          sum = *(s1= s2= s) * *coeff;
          for (cp=coeff+il; cp<=ce; cp+=il) sum += (*--s1 + *++s2) * *cp;
          *rsig++ = offset - sum/norm;
	}
      }  /*for s<e*/
    else
      for ( ; s<=e; s+=r) 
      {
	if (*s < -UMAX || *s > UMAX) *rsig++ = 0.0;  /*overdrive*/
	else 
	{
          sum = *(s1= s2= s) * *coeff;
          for (cp=coeff+il; cp<=ce; cp+=il) sum += (*--s1 + *++s2) * *cp;
          *rsig++ = sum/norm - offset;
	}
      }  /*for s<e*/

  }	/*for t*/

}	/*reduce_signal*/


/*Soubrutine to obtain offset and sign of the raw signal (needed to put the correct values of the calibration factors in RMD)*/

void get_offset_raw ( float *sig, int c,
			struct SampleRateSettings *srs, 
			struct RadiometerSettings *pRadSet)
{ 
  
  float  *s, *e;	/* pointer to signal trace */
  float offset=0.0,	/* offset to remove (average over 0..inull-1) */ 
	averageinit=0.0; 
  	
  int	 r, sign;

  if (!srs) return;

/* calculate offset in raw signal (average for t<OFFSET_TMAX) */

  e = (s=sig) + srs->inull;
  while (s<e) offset += *s++;
  if ((r=(e-sig))) offset /= r;
  pRadSet->offset[c]=offset;

/* calculate sign of the calibration factor (+1 if the signal is not inverted, -1 if the signal is inverted)*/

  e = (s=sig) + srs->iaver_sig;
  while (s<e) averageinit += *s++;
  if ((r=(e-sig))) averageinit /= r;
  
  if(offset> averageinit) 
	sign=-1;
  else sign=+1;     /* Volts (some channels have inverting cables) */
  pRadSet->sign[c]=sign;  

} /*get_offset_raw*/



/* ................ initialization ................. */


void	init_downsampling() 
{

  prm_group  (GROUP, "sample rate reduction parameters");

  prm_define (P_SAMPLERATE, "(string)", "0.0/7.4:1.0",
    "\tfixed/event triggered fs (kHz): { [eventcode?][tbeg/][tend:]fs } ",
              GROUP, NULL);

}	/* init_downsampling */


