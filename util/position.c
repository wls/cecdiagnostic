/* position.c - calculate (R,z) of ECE cold resonance from Btot on sightline */	
/* Modifications
	* Include harmonic =array (N_IFGROUP) LBarrera Sept 2012*/

#include <malloc.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "libece.h"
#include "param.h"
#include "util.h"
#include "eceradiometer.h"


#define GROUP "position"



/* ..... geometrical line-of-sight for a wave guide ................ */


#define MAX_LOS 3
#define R_MAX 2.25
#define R_MIN 1.10


/* ............. field components along sight line .................*/

static int field_los (int shot, float time, char *exper, char *diag, int *edition,
		      struct LineOfSight *los, float Bvac)

{
  float BR[N_LOS], Bz[N_LOS], Bt[N_LOS], pflux[N_LOS], Jp[N_LOS];


  if (bcomponents (shot, time, exper, diag, edition,
		   los->n, los->R, los->z,
		   Bt, BR, Bz, pflux, Jp)==SF_ERROR)
  {
    char msg[256];
    sprintf(msg, "Error in kkrzbrzt for t=%6.3f s, %s:%s %d(%d)", 
	    time, exper, diag, shot, *edition);
    log_msg(ERROR, msg); 
    return(-1);
  }

  /* total field with all corrections */
  btot_los (shot, los->n, los->R, los->z,
	    Bt, BR, Bz, Bvac, los->Btot);

  return(0);
}  /* field_los */



/* ..... exported : .................. */

/* given resonant Btot for each channel, calculate channel position */

int   calculate_positions (int shot,
			   struct RadiometerSettings *rs,
			   struct ChannelPositions *cp,
			   struct ChannelData *pChanData)
{
  struct LineOfSight los [MAX_IFGROUP];    /* one per IF group */

  char   *edstr = "0", msg[256], *valstr;
  int    diaref, type, edition, Bvacedition,
    i, nt,nall, k, c;
  float *alltime;

  if (!cp) return(-99);    /* nothing expected to do*/

  cp->have_equilibrium_Rz = cp->have_static_Rz = 0;

  /* obtain geometrical sightlines 
     (one for each IF group (waveguide), i.e. no RF frequency dependence) */
  for (k=0; k < MAX_IFGROUP; ++k)
    if (rs->waveguide[k])
      geo_los ( shot, rs->waveguide[k], rs->zlens, 
	        los[k].n = N_LOS, los[k].R, los[k].z );


  prm_getval (prm_get("rz"),  &valstr,  &type);
  if (strcmp(valstr, "bt")==0)

  /* fixed central Bt */
    {
      float Bt;
      prm_getval (prm_get("bt"),  &valstr,  &type);
      Bt = fabs(atof(valstr));

      for (k=0; k < MAX_IFGROUP; ++k)
        if (rs->waveguide[k])
	  {
	  for (i=0; i<N_LOS; ++i)
	     los[k].Btot[i] = Bt*R0/los[k].R[i];
	  }      

      for (c=0; c<MAX_CHAN; ++c)
        if (rs->have[c])
        {
	  k = rs->ifgroup[c] - 1;  /* sightline no. = IF group no.*/
	  linintf (los[k].n, los[k].Btot, los[k].R, 1, &(rs->Btot[c]), &(cp->Rs[c]));
	  linintf (los[k].n, los[k].Btot, los[k].z, 1, &(rs->Btot[c]), &(cp->zs[c]));
        }

      rs->RZ_SRC = strdup(valstr);
      rs->EQEXP= "n.a.";
      rs->EQED = 0;

      cp->have_static_Rz = 1;
      return(0);
    }


  /* B from equilibrium */

  prm_getval (prm_get("eqexp"),  &(cp->eqexp),  &type);
  prm_getval (prm_get("eqdiag"), &(cp->eqdiag), &type);
  prm_getval (prm_get("eqedtn"), &edstr, &type);
  edition = atoi(edstr);

  /* obtain equilibrium time base */
  if (sf_open_read (cp->eqexp, cp->eqdiag, &shot, &edition, &diaref) == SF_ERROR)
  {
    sprintf(msg, "Cannot open equilibrium shot file, %s:%s %d(%d)", 
	    cp->eqexp, cp->eqdiag, shot, edition);
    log_msg(ERROR, msg); 
    return(-1);
  }

  /* these may have been determined from defaults */
  cp->eqedition = edition;
  cp->eqshot = shot;

  if (sf_signal_ntpoints(diaref, "time", &nall))
  {
    log_msg(ERROR, "Cannot determine number of equilibrium time points."); 
    sf_close_read(diaref);
    return(-2);
  }
  
  alltime = (float *) calloc(nall, sizeof(float));
  if (!alltime) 
  {
    sprintf(msg, "Cannot allocate %d floats for equilibrium time base.", nall);
    log_msg(ERROR, msg); 
    sf_close_read(diaref);
    return(-3);
  }

  if (sf_read_signal_float (diaref, "time", nall, alltime))
  {
    log_msg(ERROR, "Cannot read equilibrium time base."); 
    sf_close_read(diaref);
    return(-4);
  }

  /* close equilibrium shot file (libkk will open it again) */
  sf_close_read(diaref);

  nt = nall;
  /* find last equilibrium time needed for ECE data */
  if (pChanData && pChanData->time && pChanData->ntimes)
    for (i=0; i<nall; i++)
      {
	if(alltime[i] >= pChanData->time[pChanData->ntimes-1])
	  {
	    nt = i+1;
	    break;
	  }
      }
  
  cp->ntimes = nt;
  cp->time = (float *) calloc(nt, sizeof(float));
  if (!cp->time) 
  {
    sprintf(msg, "Cannot allocate %d floats for short equilibrium time base.", nt);
    log_msg(ERROR, msg); 
    sf_close_read(diaref);
    return(-5);
  }
  

  for (i=0; i<nt; i++){
    cp->time[i] = alltime[i];
  }

  //free complete equilibrium time base
  free(alltime);


  /* print equilibrium data */
  sprintf(msg, "Truncated Equilibrium %s:%s %d(%d)  %d times (%6.3f .. %6.3f s)", 
	  cp->eqexp, cp->eqdiag, cp->eqshot, cp->eqedition,
	  cp->ntimes, cp->time[0], cp->time[cp->ntimes-1]);
  log_msg(INFO, msg); 

  //return(-4);

  edstr = "0";
  prm_getval (prm_get("Bvacexp"),  &(cp->Bvacexp),  &type);
  prm_getval (prm_get("Bvacdiag"), &(cp->Bvacdiag), &type);
  prm_getval (prm_get("Bvacedtn"), &edstr, &type);
  Bvacedition = atoi(edstr);

  

  /*Read the vacuum field from MBI and map it to the used timebase of the Equilibrium*/
  cp->Bvac = (float *) calloc(nt, sizeof(float));

  if (!cp->time) 

  {
    sprintf(msg, "Cannot allocate %d floats for Bvac array", nt);
    log_msg(ERROR, msg); 
    sf_close_read(diaref);
    return(-5);
  }

 
  /*Read the vacuum field from MBI and map it to the used timebase of the Equilibrium*/
  // (cp->Bvacexp), (cp->Bvacdiag)
  if ( get_bvac(shot, cp->Bvacexp, cp->Bvacdiag, &Bvacedition, nt, cp->time, cp->Bvac) )
   {
     sprintf(msg, "Failed to read MBI data for the Bvac");
     log_msg(ERROR, msg); 
     return(-5);
   }
    

  /* allocate memory for (R,z) time traces */
  for (c=0; c<MAX_CHAN; ++c)
    if (rs->have[c])
    {
      cp->R[c] = (float *) calloc(nt, sizeof(float));
      if (!cp->R[c])
      {
	sprintf(msg, "Failed to allocate %d floats for R position channel %d.", nt, c+1);
	log_msg(ERROR, msg); 
	return(-5);
      }
      cp->z[c] = (float *) calloc(nt, sizeof(float));
      if (!cp->z[c])
      {
	sprintf(msg, "Failed to allocate %d floats for z position channel %d.", nt, c+1);
	log_msg(ERROR, msg); 
	return(-5);
      }
    }
  
  /* loop time steps */   
  for (i=0; i<nt; ++i)
  {
    /* total field along each sightline */
    for (k=0; k < MAX_IFGROUP; ++k)
      if (rs->waveguide[k])
        if (field_los (shot, cp->time[i], cp->eqexp, cp->eqdiag, 
		       &edition, &(los[k]),cp->Bvac[i]))
	  return(-7);

    /* interpolate positions for per-channel cold resonant field */
    for (c=0; c<MAX_CHAN; ++c)
      if (rs->have[c])
      {
	k = rs->ifgroup[c] - 1;  /* sightline no. = IF group no.*/
	linintf (los[k].n, los[k].Btot, los[k].R, 1, &(rs->Btot[c]), cp->R[c]+i);
	linintf (los[k].n, los[k].Btot, los[k].z, 1, &(rs->Btot[c]), cp->z[c]+i);
      }
    
  }  /* loop time steps */

  /* remember where position data came from */
  rs->RZ_SRC = cp->eqdiag;
  rs->EQEXP = cp->eqexp;
  rs->EQED = cp->eqedition;
  
  cp->have_equilibrium_Rz = 1;
  return(0);  
}






/* clean up position data structure */

void   free_positions (struct ChannelPositions *cp)
{
  int i;
  if (!cp) return;
  if (cp->time) free(cp->time);
  for (i=0; i<MAX_CHAN; ++i)
  {
    if ((cp->R)[i]) free((cp->R)[i]);
    if ((cp->z)[i]) free((cp->z)[i]);
  }
}


/* ........... initialise ........... */

void  init_position	(void)
{
  prm_group  (GROUP, "Channel position parameters");

  prm_define ("harmonic", "1,2,3,4,ref", "2,2,2",
              "\tECE harmonic number for any IF group (3 arguments); ref = use reference shot file",
              GROUP, NULL);


  prm_define ("rz", "eq,bt", "eq", 
              "Position (R,z) from: eq)uilibrium or bt) at plasma centre",
              GROUP, NULL);
  prm_define ("bt", "(float)", "", 
              "Toroidal field value (T) at vessel centre",
              GROUP, NULL);

  prm_define ("Bvacdiag", "MBI,MAY,(string)", "MBI", 
              "equilibrium diagnostic",
              GROUP, NULL);
  prm_define ("Bvacexp", "AUGD,(string)", "AUGD", 
               "experiment code to get Bvac",
              GROUP, NULL);
  prm_define ("Bvacedtn", "(integer)", "0", 
              "Bvac shot file edition",
              GROUP, NULL);

  prm_define ("eqdiag", "FPP,EQI,EQE,EQB,EQH,EQZ,IDE,EQR", "EQI", 
              "equilibrium diagnostic",
              GROUP, NULL);
  prm_define ("eqexp", "AUGD,(string)", "AUGD", 
              "equilibrium experiment code",
              GROUP, NULL);
  prm_define ("eqedtn", "(integer)", "0", 
              "equilibrium shot file edition",
              GROUP, NULL);

}

/* end of position.c */
