/* rad_patches.c - signal description and patches for RAD shot files 
   26-Mar-2010 wls 
   Modifications:
    *changed LSB and USB for mixer 101: change meaning of radiometer control accorgingly (Sept. 2012, LBarrera)

   Feb  2013 Include description RMC diagnostic*/


#include <stdio.h>
#include "libece.h"
#include "util.h"
#include "eceradiometer.h" 
//#include "cec.h"


/* -------------------------
   RMC signals from shot ...
   struct RawDiagnostic defined in libece.h

   ------------------------- */

struct RawSigGroup S2= 
{
  NULL, "Trad-A2", "D-Sio", 30, 1, 30
};

struct RawSigGroup S1= 
{
  &S2, "Trad-A1", "D-Sio", 30, 1, 0
};


struct RawSigGroup SZ= 
{
  NULL, "Trad-A", "D-DTACQ", 60, 1, 0
};



struct RawDiagnostic RMC_1 =
{
 // n_ifgroups=3, enum switchinfosrc {sw_NONE, sw_RAD} switch_info_source=sw_RAD
  NULL, "RMC", 3, sw_RAD, &S1
};


struct RawDiagnostic RMZ_1 =
{
 // n_ifgroups=3, enum switchinfosrc {sw_NONE, sw_RAD} switch_info_source=sw_RAD
  NULL, "RMZ", 3, sw_RAD, &SZ
};


/* ---------------------------------
   return RMC setup for a given shot 
   --------------------------------- */

struct RawDiagnostic *rmc_setup(int shot)
{
  return(&RMC_1);
}

/* ---------------------------------
   new RMZ diagnostic 21.11.24
   --------------------------------- */

struct RawDiagnostic *rmz_setup(int shot)
{
  return(&RMZ_1);
}



/* -------------------------
   RAD signals from shot ...
   ------------------------- */

struct RawSigGroup SI_MI_D = 
{
  NULL, "SI-MI-D", "D-RAD-D", 15, 1, 45
};

struct RawSigGroup SI_MI_C = 
{
  &SI_MI_D, "SI-MI-C", "D-RAD-C", 15, 1, 30
};

struct RawSigGroup SI_MI_B = 
{
  &SI_MI_C, "SI-MI-B", "D-RAD-B", 15, 1, 15
};

struct RawSigGroup SI_MI_A = 
{
  &SI_MI_B, "SI-MI-A", "D-RAD-A", 15, 1, 0
};

struct RawDiagnostic RAD_1 =
{
  NULL, "RAD", 3, sw_RAD, &SI_MI_A
};



/* ---------------------------------
   return RAD setup for a given shot 
   --------------------------------- */

struct RawDiagnostic *rad_setup(int shot)
{
  return(&RAD_1);
}

/* -------------------------------------
   patch radiometer and channel settings
   (if RAD shots file parameters wrong)
   ------------------------------------- */

void rad_patches(int shot, 
		 struct RadiometerSettings *rs)
{

/* attnif was set to 20 dB but sfh says 30 dB from 16- thru 19-Apr-96 */
  if (shot>=7859 && shot<=7899) 
    rs->attnif[2] = rs->attnif[3] = 20.0;

/* 19-Jun-97 to 26-Jun-97:
   attnif of 1. group was set to 10 dB but sfh says 20 dB 
   attnif of 2. group was set to 16 dB but sfh says 26 dB 
   attnif of 3. group was set to 16 dB but sfh says 26 dB */  
  if (shot>=8880 && shot<=8965)
  {
    rs->attnif[1] = 10.0;
    rs->attnif[2] = rs->attnif[3] = 16.0;
  }

/* 13-NOV-97 # 9528 : 
   attnif of 2. group was set to 30 dB but sfh says 20 dB 
   attnif of 3. group was set to 30 dB but sfh says 20 dB */
  if (shot==9528) 
    rs->attnif[2] = rs->attnif[3] = 30.0;

/* 17 Sept 2012: rachange WG for LSB and USB for mixer 101*/

}  /* rad_patches */




/* ------------------------------
   subroutines for mixer settings 
   ------------------------------ */

static void set_lo_95 (int shot, int ctrl, int nif,
		       struct RadiometerSettings *rs)
{
  --nif;
  rs->freq_LO[nif] = 95.0e9;      /* Hz */
  rs->waveguide[nif]=10;        /* always on 10 */
  rs->sideband[nif]= 'U';         /* always USB */
  rs->LO[nif] = LO95U;
  rs->calchannel[nif] = 0;      /* no notch filter */
  if ((ctrl & 0x0004 ) == 0) {
      rs->LO[nif] = none;
      rs->sideband [nif] = ' ';
      rs->waveguide[nif] = 0;
    }  
  
}       /* set_lo_95 */



static void set_lo_101(int shot, int ctrl, int nif,
		       struct RadiometerSettings *rs)
{
  --nif;
  rs->freq_LO[nif] = 101.0e9; /* Hz from 101 */
  rs->calchannel[nif] = 0;

  /* frontends with oversized wg switches moved from wg 12 to wg 9 */
  /* 17.6.2017 mwillens, 101L is now on waveguide 12 again */
  if (shot<7823) rs->waveguide[nif] = 12;
  else if (shot>=7823 && shot<=33724) rs->waveguide[nif] = 9;
  else if (shot>33724 && shot<=36954) rs->waveguide[nif] = 12;
  else if (shot>36954)  rs->waveguide[nif] = 11;

 /* 101L was out of order in this week */
  if ((shot>36906 && shot<=37001)){
    log_msg(WARNING,
		    "101 GHz was not available during this time");
	  return;
	rs->LO[nif] = none;
	rs->sideband[nif] = ' ';
	rs->waveguide[nif] = 0;
	return;
  }
 /* 13-Feb ... 22-Mar-2012 monomode wg switch was not read out correctly
    - need to find sideband from oversized wg switches */
  if ((shot>=27510 && shot<=27620)|| shot>36954)
    {
      if (ctrl & 0x0040)   /* oversized wg switch 6 (101LSB) on */     
	{
	  rs->sideband [nif] = 'L';
	  rs->LO[nif] = LO101L;
	}
      else 
	{
	  rs->sideband [nif] = 'U';  
	  rs->LO[nif] = LO101U;
	}
      return;
    }

 /* normally we decide sideband based on monomode waveguide switch settings
    except 1-JUL-1997 when RAD was set to 101U, 101U, 133L  
    NEW CONFIGURATION: WG 6 and 7 connected to 101 LSB and USB after Sept 2012
      instead of 101 USB and LSB. Therefore change in the meaning of the monomode wg switch LBarrera*/
  // use this configuration for campaign 2020, due to broken 101S

  if (shot<=28523)
    {  /*Before Sept 2012 and after Jan 2020*/
      /*LSB*/  
      if (ctrl & 0x0080 && shot!= 9001) 
	{
	  rs->sideband [nif] = 'L';
	  rs->LO[nif] = LO101L;
	  if ((ctrl & 0x0040) == 0)
	    log_msg(WARNING,
		    "25mm wg switch 7 leaves 101 GHz LSB without a signal.");
	  return;
	}

 /* USB */
      rs->sideband [nif] = 'U';  
      rs->LO[nif] = LO101U;
      if (shot < 7823  &&  (ctrl & 0x0002) == 0)
	log_msg(WARNING,
		"WARNING: 25mm wg switch 2 leaves 101 GHz USB without a signal.");
      else if ((ctrl & 0x0060) != 0x0020) 
	log_msg(WARNING,
		"25mm wg switches 6/7 leave 101 GHz USB without a signal.");
      return;
      
    }
  
  if (shot>=28523 && shot<=33724) {
    if (ctrl & 0x0080) /*After Sept 2012*/
      {
    /* USB After Sept 2012*/	
	rs->sideband [nif] = 'U';
	rs->LO[nif] = LO101U;
	if ((ctrl & 0x40) == 0)  
	  log_msg(WARNING,
		  "25mm wg switch 7 leaves 101 GHz USB without a signal."); /*?????????? What is this control meaning now*/
	return;
      }

    /* LSB After Sept 2012*/
    rs->sideband [nif] = 'L';  
    rs->LO[nif] = LO101L;
    /*?????Ctrl meaning now?*/
    if ((ctrl & 0x0060) != 0x0020) 
      log_msg(WARNING,
	      "25mm wg switches 6/7 leave 101 GHz USB without a signal."); /*?????Ctrl meaning now?*/
  }
  
  /* only LSB After Jan 2016, mwillens, Jan 2020 switch waveguide in 36954*/
  if (shot>=33724 && shot<=36954) {
    /* waveguide switch 1*/
    if ((ctrl & 0x0001) == 1){
      rs->sideband [nif] = 'L';  
      rs->LO[nif] = LO101L; 
    }
     
  } 

    
   

 
}       /* set_lo_101 */



static void set_lo_128(int shot, int ctrl, int nif,
		       struct RadiometerSettings *rs)
{
  --nif;
  rs->freq_LO[nif] = 128.0e9;       /* Hz */
  rs->sideband[nif]= 'U';           /* always USB */
  rs->LO[nif] = LO128U;
  if (shot < 8724) rs->waveguide[nif]=10; 
  if (shot>=7823 && shot<=33724)  rs->waveguide[nif]=12;
  else  rs->waveguide[nif]=3;  /* mwillens */
  
  //mwillens
  if (shot<=33724){
    if ((ctrl & 0x0001) == 0) 
      {
	rs->LO[nif] = LO128N;
	rs->sideband[nif]= 'N';           /* wg switch say notch filter */
	rs->calchannel[nif] = 1;          /* use notch filter calibration */
	log_msg(INFO,
		"25mm wg switch 1 route through notch filter.");
      }
    else
      {
	rs->LO[nif] = LO128U;
	rs->sideband[nif]= 'U';           /* USB, oherwise */
	rs->calchannel[nif] = 0;
      }
  } else {
    rs->LO[nif] = LO128N;
    rs->sideband[nif]= 'N';           /* USB, oherwise */
    rs->calchannel[nif] = 0;
    if ((ctrl & 0x0010) == 0) log_msg(WARNING,
         "25mm wg switch 5 leaves 128 GHz mixer without a signal.");
  }

}       /* set_lo_128 */



static void set_lo_133(int shot, int ctrl, int nif,
		       struct RadiometerSettings *rs)
{
  --nif;
  rs->freq_LO[nif] = 133.0e9; /* Hz */
  rs->calchannel[nif] = 0;

  if (shot < 7823) 
  {   /* oversized wg sw determines sideband */
    rs->waveguide[nif] = 9;
    if      (ctrl & 0x0040) 
      {
	rs->sideband[nif] = 'L';
        rs->LO[nif] = LO133L;
      }
    else if (ctrl & 0x0020) 
      {
	rs->sideband[nif] = 'U';
	rs->LO[nif] = LO133U;
      }
    else  log_msg(WARNING,
      "25mm wg switches 6,7 leave 133 GHz mixer without a signal.");
  }
  else 
  {                 /* LSB only after #7823 */
    rs->sideband[nif]= 'L'; 
    rs->LO[nif] = LO133L;
    if (shot<8724) rs->waveguide[nif]=10; /* no automatic waveguide switch */ 
    else 
    { 
      rs->waveguide[nif]=4;
      if ((ctrl & 0x0002) == 0) log_msg(WARNING,
         "25mm wg switch 2 leaves 133 GHz mixer without a signal.");
    }
  }
}       /* set_lo_133 */



static void set_lo_167(int shot, int ctrl, int nif,
		       struct RadiometerSettings *rs)
{
  --nif;
  rs->freq_LO[nif] = 167.0e9; /* GHz */
  rs->waveguide[nif] = 9;
  rs->calchannel[nif] = 0;

  /*monomode switch is manually operated,
    oversized wg switches determine sideband */
  if      ((ctrl & 0x0070)==0x0010) 
    {
      rs->sideband[nif] = 'L';
      rs->LO[nif] = LO167L;
    }
  else if ((ctrl & 0x0078)==0x0008) 
    {
      rs->sideband[nif] = 'U';
      rs->LO[nif] = LO167U;
    }
  else log_msg(WARNING,
    "25mm wg switches 4,5 leave 167 GHz mixer without a signal.");
}       /* set_lo_167 */



/* new mixer on 22.11.21 from #39795 */
static void set_lo_105 (int shot, int ctrl, int nif,
		       struct RadiometerSettings *rs)
{
  --nif;
  rs->LO[nif] = LO105U;
  rs->freq_LO[nif] = 104.403e9;      /* Hz 105.02 to 104.403 */
  //new waveguide for Mixer
  rs->waveguide[nif]=11; 

  rs->sideband[nif]= 'U';         /* always USB */
  rs->calchannel[nif] = 0;          /* use notch filter calibration */
  // switch 5 is not important anymore 0x0078-> 0x0068
  if ((ctrl & 0x0068)!=0x0008)
          log_msg(WARNING,
        "25mm wg switches 4 leave new 105 GHz mixer without a signal.");
  /*oversized wg switches determine sideband */
  // Mixer new and not available
  if (shot<39793){
      rs->LO[nif] = none;
      rs->sideband [nif] = ' ';
      rs->waveguide[nif] = 0;
  }

}       /* new mixer on 12.6.13 */




/* new mixer on 12.6.13 from #31788 */
static void set_lo_101S (int shot, int ctrl, int nif,
		       struct RadiometerSettings *rs)
{
  --nif;
  rs->LO[nif] = LO101S;
  rs->freq_LO[nif] = 100.92e9;      /* Hz */
  //new waveguide for Mixer
  if (shot<33733){
    rs->waveguide[nif]=9; }         /* always on 10 */
  else{
    rs->waveguide[nif]=11; }

  rs->sideband[nif]= 'S';         /* always USB */
  rs->calchannel[nif] = 0;          /* use notch filter calibration */
  // switch 5 is not important anymore 0x0078-> 0x0068
  if ((ctrl & 0x0068)!=0x0008)
          log_msg(WARNING,
        "25mm wg switches 4 leave new 101 GHz mixer without a signal.");
  /*oversized wg switches determine sideband */
  // Mixer broke, no valid calibration
  if (shot>36826){
      rs->LO[nif] = none;
      rs->sideband [nif] = ' ';
      rs->waveguide[nif] = 0;
  }

}       /* new mixer on 12.6.13 */





/* new mixer on 20.3.17 from #33733 */
static void set_lo_91 (int shot, int ctrl, int nif,
		       struct RadiometerSettings *rs)
{
  --nif;
  rs->freq_LO[nif] = 91.00e9;      /* Hz */
  if ( shot<36954 ){
    rs->waveguide[nif]=11;          /* always on 10 */
  } else if ( shot>=36954 ) {
    rs->waveguide[nif]=12; 
  }
  rs->calchannel[nif] = 0;          /* use notch filter calibration */

  if (shot < 36799) { // monomode switch is out since Jan, 2019, mwillens


    //SMI wg switch 1  on left
    if ((ctrl & 0x0080)==0 ) {
      rs->sideband [nif] = 'U';
      rs->LO[nif] = LO91U;
     // Bit 7 is zero
      if ((ctrl & 0x0060) != 0x0020) 
	log_msg(WARNING,
	       "25mm wg switches 6/7 leave 91 GHz USB without a signal says rad_patches.."); /*?????Ctrl meaning now?*/
      return;
    }
    else {
      rs->sideband [nif] = 'L';  
      rs->LO[nif] = LO91L;
      /*?????Ctrl meaning now?*/
      if ((ctrl & 0x40) == 0)  
	log_msg(WARNING,
		"25mm wg switch 7 leaves 91 GHz LSB without a signal says rad_patches.");
 
    }
  } else if (shot >= 36799 && shot<=36954 )  {  // no monomod switch anymore
    rs->sideband [nif] = 'L';  
    rs->LO[nif] = LO91L;
    /*?????Ctrl meaning now?*/
    if ((ctrl & 0x40) == 0)  
      log_msg(WARNING,
		"25mm wg switch 7 leaves 91 GHz LSB without a signal says rad_patches.");


  } else if ( shot>36954 ){
    rs->sideband [nif] = 'L';  
    rs->LO[nif] = LO91L;
    if ((ctrl & 0x0001) == 0)
      log_msg(WARNING,
		"25mm wg switch 1 leaves 91 GHz LSB without a signal says rad_patches.");
  } 

  // if (shot>=33724 && shot<=36954) {
  //  /* waveguide switch 1*/
  //  if ((ctrl & 0x0001) == 1){
  //    rs->sideband [nif] = 'L';  
  //    rs->LO[nif] = LO101L; 
  //  }
     


}       /* new mixer on 30.3.17 */




/* ----------------------------------------------
   read and evaluate radiometer control word 
   from RAD shotfile
   (coded mixer and IF assignments)
   ---------------------------------------------- */

int rad_switchsettings (int shot, int diaref, struct RadiometerSettings *rs)
{
  char msg[256];

  /* read control word (1 word signal in RAD shot file */
  int ctrl;
  int error = sf_read_signal_int (diaref, "SI-CTRL", 1, &ctrl);
  if (error) return(error);

  //something went wrong in the ECE setting in this discharge
  // mwillens switched the setting before the word was written
  if (shot==35113){
    ctrl=0xffff9b1a;
  }

  sprintf(msg, "Radiometer control word = %x.", ctrl);
  log_msg(DEBUG, msg);

  if ((ctrl & 0x8000)==0) 
  {
    log_msg(ERROR, "SI-CTRL word error flag set - undefined radiometer settings.");
    return(-1);
  }

  /* SP4T  coax switch position. */
  switch (ctrl & 0x6000) 
  {
    case 0x2000:
      if (shot < 8724) 
      {
        log_msg(ERROR,"Unknown position code of SP4T coax switch.");
        return(-1);
      }
      set_lo_101(shot, ctrl, 2, rs);
      break;

    case 0x4000:
      if (shot < 7823)  {set_lo_101(shot, ctrl, 1, rs); break;}
      if (shot < 8724)  {set_lo_167(shot, ctrl, 1, rs); break;}
                        {set_lo_133(shot, ctrl, 2, rs); break;}
      break;

    case 0x6000:
      if (shot < 7823)  {set_lo_133(shot, ctrl, 1, rs); break;}
      if (shot < 8724)  {set_lo_101(shot, ctrl, 1, rs); break;}
                        {set_lo_128(shot, ctrl, 2, rs); break;}
      break;

    case 0x0000:
      if (shot >= 39795) {set_lo_105(shot, ctrl, 2, rs); break;}
      if (shot >= 31788) {set_lo_101S(shot, ctrl, 2, rs); break;}
      if (shot >= 8724) {set_lo_167(shot, ctrl, 2, rs); break;}
      if (shot >= 7823) {set_lo_128(shot, ctrl, 1, rs); break;}

    default:
      log_msg(ERROR,"Unknown position code of SP4T coax switch.");
      return(-1);
  }

/* SPDT #1 coax switch */

  switch (ctrl & 0x0800) 
  { 
    case 0x0800:        
      if (shot < 8724) {set_lo_133(shot, ctrl, 2, rs); break;}
                       {set_lo_133(shot, ctrl, 3, rs); break;}
    case 0x0000:
      if (shot < 7823)  {set_lo_101(shot, ctrl, 2, rs); break;}
      if (shot < 8724)  {set_lo_167(shot, ctrl, 2, rs); break;}
                        {set_lo_95 (shot, ctrl, 3, rs); break;}
      break;
  }

/* SPDT #2 coax switch - used only between 7823 and 8724 */

  if (shot >= 7823  && shot < 8724)
  {
    switch (ctrl & 0x1000) 
    {
      case 0x1000: {set_lo_133(shot, ctrl, 3, rs); break;}
      case 0x0000: {set_lo_128(shot, ctrl, 3, rs); break;}
    }
  }

/* SP6T  coax switch - not installed before 8724 */
  if (shot >= 8724)
  {
    // select Bit8-Bit10
    switch (ctrl & 0x0700)
    {
      case 0x0100: {set_lo_101(shot, ctrl, 1, rs); break;}
      case 0x0200: {set_lo_95 (shot, ctrl, 1, rs); break;}
      case 0x0300: {set_lo_128(shot, ctrl, 1, rs); break;}
      /*mwillens replace 167 with set_lo_101S*/
      case 0x0400: {
	if(shot >= 39795)
	  {set_lo_105(shot, ctrl, 1, rs); break;} //new mixer:
	else
	  {
                if(shot >= 31788)
                        {set_lo_101S(shot, ctrl, 1, rs); break;}
                else    {set_lo_167(shot, ctrl, 1, rs); break;}
	  }
      }
       	//new mixer:
      case 0x0500: { 
	if(shot >= 33733){
	  set_lo_91(shot, ctrl, 1, rs); break;}
	else
	  {break;}
      }	
	break;
    /* higher codes not yet used */
    }
  }

  return(0);
} 

