/* utiltest.c */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "libece.h"

#include "param.h"
#include "util.h"
#include "eceradiometer.h"


/* ........... logging tests .............. */

static void do_logerror(PRM_HANDLE p)
{
  int type; char *msg;
  while (prm_getval(p, &msg, &type)) 
    log_msg(ERROR, msg);
}

static void do_logwarning(PRM_HANDLE p)
{
  int type; char *msg;
  while (prm_getval(p, &msg, &type)) 
    log_msg(WARNING, msg);
}

static void do_loginfo(PRM_HANDLE p)
{
  int type; char *msg;
  while (prm_getval(p, &msg, &type)) 
    log_msg(INFO, msg);
}

static void do_logdebug(PRM_HANDLE p)
{
  int type; char *msg;
  while (prm_getval(p, &msg, &type)) 
    log_msg(DEBUG, msg);
}

static void do_logtime(PRM_HANDLE p)
{
  log_timestamp();
}




/* ........... shotfile tests .............. */


static void process_shot (int shot) 
{
  int type, edition, diaref;
  char *experiment, *diag, *str, msg[256];
  PRM_HANDLE p;

  p=prm_get("experiment");
  if (!(prm_getval(p, &experiment, &type)))
  {
    log_msg(ERROR, "experiment code missing");
    return;
  }

  p=prm_get("diag");
  if (!(prm_getval(p, &diag, &type)))
  {
    log_msg(DEBUG, "diagnostic name missing");
    return;
  }

  p=prm_get("edition");
  if (!(prm_getval(p, &str, &type)))
  {
    log_msg(DEBUG, "diagnostic name missing");
    return;
  }
  edition=atoi(str);

  sprintf(msg, "Open   %s:%s %d (%d)\n", experiment, diag, shot, edition);
  log_msg(INFO, msg);


  if (sf_open_read(experiment, diag, &shot, &edition, &diaref)==SF_ERROR)
  {
    sprintf(msg, "Opened %s:%s %d (%d)\n", experiment, diag, shot, edition);
    log_msg(INFO, msg);

    sf_close_read(diaref);
  }
  
} /* process_shot*/



static struct RadiometerSettings RadSet;


void do_test_radsettings (PRM_HANDLE p)
{
  int type; char *str;
  while (prm_getval(p, &str, &type))
  {
    int shot; char msg[64];
    shot = atoi(str);
    sprintf(msg, "Processing shot %6d", shot);
    log_msg(INFO, msg);

    if (parse_radiometer_parameters (shot, &RadSet))
      log_msg(ERROR, "Unable to parse radiometer settings");

    if (write_rmd_calib(shot, &RadSet, NULL /*no shift*/ ))
      log_msg(INFO, "RMD shot file not written");
  
  }
}  /* do_test_radsettings */



void do_print_settings(PRM_HANDLE p)
{
  int i, type;
  char *valstr;
  int print_calfact=0, print_freqif=0,
      print_bif=0, print_freqrf=0,
      print_gainpa=0, print_gainma1=0,
      print_gainma2=0, print_gainld=0, 
      print_bvd=0;

  while (prm_getval(p, &valstr, &type))
  {
    if (strcmp(valstr,"freqif")==0) print_freqif=1;
    if (strcmp(valstr,"bif")==0) print_bif=1;
    if (strcmp(valstr,"freqrf")==0) print_freqrf=1;
    if (strcmp(valstr,"gainpa")==0) print_gainpa=1;
    if (strcmp(valstr,"gainma1")==0) print_gainma1=1;
    if (strcmp(valstr,"gainma2")==0) print_gainma2=1;
    if (strcmp(valstr,"gainld")==0) print_gainld=1;
    if (strcmp(valstr,"bvd")==0) print_bvd=1;
    if (strcmp(valstr,"calfact")==0) print_calfact=1;
  }

  for (i=0; i<MAX_CHAN; ++i)
    if (RadSet.have[i])
      {
	printf("%02d", i+1);
	if (print_freqif)  printf(" fIF=%6.3f GHz",   RadSet.freqif[i]/1e9);
	if (print_freqrf)  printf(" fRF=%7.3f GHz",   RadSet.freqrf[i]/1e9);
	if (print_bif)     printf(" bIF=%3.1f GHz",   RadSet.bif[i]/1e9);
	if (print_gainpa)  printf(" gainpa=%3.0f",    RadSet.gainpa[i]);
	if (print_gainma1) printf(" gainma1=%3.0f",   RadSet.gainma1[i]);
	if (print_gainma2) printf(" gainma2=%3.0f",   RadSet.gainma2[i]);
	if (print_gainld)  printf(" gainld=%3.0f",    RadSet.gainld[i]);
	if (print_bvd)     printf(" bvd=%5.1f kHz",   RadSet.bvd[i]);
	if (print_calfact) printf(" calf=%9.2f eV/V", RadSet.calfact[i]);
	printf("\n");
      }
}  /* do_print_settings */



/* ............... main ..................... */

static char   *defaultarg[2];


int main (int argc, char **argv)
  {
  defaultarg[0] = "utiltest";
  defaultarg[1] = strdup("cmd");

  prm_group  ("logging_tests", "logging tests");
  prm_define ("logerror", "(string)", "", 
              "\tlog an error message",
              "logging_tests", do_logerror);
  prm_define ("logwarning", "(string)", "", 
              "\tlog a warning message",
              "logging_tests", do_logwarning);
  prm_define ("loginfo", "(string)", "", 
              "\tlog an informative message",
              "logging_tests", do_loginfo);
  prm_define ("logdebug", "(string)", "", 
              "\tlog a debugging message",
              "logging_tests", do_logdebug);
  prm_define ("logtime", "(void)", "", 
              "\tlog time stamp",
              "logging_tests", do_logtime);

  prm_group  ("shotfile_tests", "shotfile access tests");
  prm_define ("experiment", "(string)", "AUGD", 
              "\texperiment code of next shot file to open",
              "shotfile_tests", NULL);
  prm_define ("diagnostic", "(string)", "FPC", 
              "\tedition number of next shot file to open",
              "shotfile_tests", NULL);
  prm_define ("edition", "(integer)", "0", 
              "\tedition number of next shot file to open",
              "shotfile_tests", NULL);


/* ....... initialize subordinate modules ................. */

  init_logging  ();
  init_shots    (process_shot);

  init_radiometer ();
  init_rmdsf      ();

  prm_define ("radiometer_settings", "(integer)", "0", 
              "\tobtain radiometer settings for a reference shot number",
              "eceradiometer", do_test_radsettings);

  prm_define ("print_settings", 
      "freqif,bif,freqrf,gainpa,gainma1,gainma2,gainld,bvd,calfact", "calfact", 
              "\tpint selected channel-dependant parameter(s)",
              "eceradiometer", do_print_settings);
  
  set_radiometer_defaults(&RadSet);


/* ........ execute the parameter shell command loop ...... */

  if (argc>1)   prm_main (argc, argv);
  else          prm_main (2, defaultarg);

/* .................... de-initialize ..................... */

  return(0);
  }  /* main() */

