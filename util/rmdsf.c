/* rmdsf.c   - edit RMD shot file header and write RMD shot
   22-Feb-2012  Wolfgang Suttrop
*/



#include <inttypes.h>
#include <math.h>
#include <memory.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#ifdef DDWW8
#include "ansisfh8.h"
#else
#include "ansisfh.h"
#endif


#include "param.h"
#include "libece.h"
#include "util.h"
#include "eceradiometer.h" 


/* ---------------------------------------------------------------
   set calibration factors (eV / ADC input volts)
   Calibration data for 30 channels are written at once
   and there are two parameter sets with 30 channels each.
------------------------------------------------------------------ */
/*#ifdef DDWW8
  int32_t err;
#else
  int err;
  #endif*/

static int write_calfacts(int diaref, struct RadiometerSettings *rs)
{
  int i;

  for (i=0; i<2; ++i)
    {
      float   multia00[30], shiftb00[30],calfactchan;
      char    onam[9];
      int     c;

      for (c=0; c<30; ++c)
	{
	  int   ch = i*30 + c;
	  calfactchan=rs->calfact[ch];
	  if(calfactchan > 0) 
		multia00[c] = -1.*calfactchan;  /* ADC input -to- eV RMC allways negative, new ADC now positive 2021, since jan 11 2022 negative again*/
	  else multia00[c] = calfactchan;
	  //shiftb00[c] = rs->offset[ch];  Correction 20 March 2013 SHIFT=+offset*calfact  (calfact <0)
	  shiftb00[c] = rs->offset[ch]*rs->calfact[ch];
	 // if (shift) shiftb00[c] = shift[ch];   /* if measured */
	 // else       shiftb00[c] = 0.0;         /* if not */
	}

      //sprintf(onam, "%s%i", "eCAL-A", i+1);
      snprintf(onam, 9,"%s%i", "eCAL-A", i+1);
      if (sf_write_parm_float  (diaref, onam, "MULTIA00", 
				30, multia00))
	{
	  log_msg(ERROR, "Failed to write MULTIA00. closing"); 
	  sf_close_write (diaref);
	  return(-1);
	}

      if (sf_write_parm_float  (diaref, onam, "SHIFTB00", 
				30, shiftb00))
	{
	  char msg [125];
	  sprintf(msg, "Failed to write SHIFTB00. siftb00= %f. closing",shiftb00[c]);
           log_msg(ERROR, msg);
	  //log_msg(ERROR, "Failed to write SHIFTB00"); 
	  sf_close_write (diaref);
	  return(-1);
	}
  
    }
  return(0);
}   /* write_calfacts */

static int write_calfacts_beforeShot(int diaref, struct RadiometerSettings *rs, 
			  float shift[MAX_CHAN])
{
  int i;

  for (i=0; i<2; ++i)
    {
      float   multia00[30], shiftb00[30];
      char    onam[9];
      int     c;

      for (c=0; c<30; ++c)
	{
	  int   ch = i*30 + c;
	  float calfact = 0.0;
	  if (rs) 
	    {
	      int nif = rs->ifgroup[ch] - 1;
      /* gainma2 and gainld absorbed in RMC bits-to-volts calibration */
	      float denom=0.0,
		video_gain = rs->gainpa[ch] * rs->gainma1[ch] //* rs->gainma1[ch]
	         * ( (float) pow ((double) 10.0, 
				  (double) (-0.1*rs->attnif[nif])) );
	      denom = ELEMENTARY_CHARGE * video_gain * rs->S[ch]
		* rs->freqrf[ch] * rs->freqrf[ch] * rs->bif[ch];

	      if (rs->S[ch] && denom>0) 
		calfact = -1.*C2 / denom;  //Change 20 Marz (signal RMC always negative), now Dec 2021 positive, jan 2022 negative
	    }

	  multia00[c] = calfact;    /* ADC input -to- eV */
	  if (shift) shiftb00[c] = shift[ch];   /* if eCALmeasured */
	  else       shiftb00[c] = 0.0;         /* if not */
	}

      snprintf(onam, 9,"%s%i", "eCAL-A", i+1);

      if (sf_write_parm_float  (diaref, onam, "MULTIA00", 
				30, multia00))
	{
	  log_msg(ERROR, "Failed to write MULTIA00. Closing"); 
	  sf_close_write (diaref);
	  return(-1);
	}

      if (sf_write_parm_float  (diaref, onam, "SHIFTB00", 
				30, shiftb00))
	{
	  log_msg(ERROR, "Failed to write SHIFTB00. Closing"); 
	  sf_close_write (diaref);
	  return(-1);
	}

    }
  return(0);
}   /* write_calfacts_beforeShot */

//soubrutine to write only calib. factors (not process shot, downsampling, r-z-A...)
// Usefull to set calibration factors before the shot, for real time analysis
int write_rmd_calib(int shot,
	       struct RadiometerSettings *rs, 
	       float shift[MAX_CHAN])
{
  PRM_HANDLE p;
  char msg[256], *diag="RMD", *exper;
  int type, diaref, stat, 
      nshot = shot, 
      edition = 0;
#ifdef DDWW8
  int32_t err;
#else
  int err;
#endif
  
  p = prm_get("rmd_experiment");
  if (!prm_getval(p, &exper, &type)) exper="AUGD";

 

  stat = sf_open_write(exper, diag, &nshot, &edition, &diaref);
  if (stat==SF_ERROR) 
    {
      sprintf(msg, "cannot open to write %s:%s %d (%d) diaref=%d. ERROR=%d", 
	      exper, diag, nshot, edition,diaref, SF_ERROR);
      log_msg(ERROR, msg);
      sf_close_write (diaref);
      return(-1);
    }

  sprintf(msg, "Writing to %s:%s %d (%d) diaref=%d", exper, diag, nshot, edition,diaref);
  log_msg(INFO, msg);

  if (write_calfacts_beforeShot(diaref, rs, shift))
  {
    //sfherror (err, "(Calib.Factors)"); no error message 
    sprintf(msg, "Failed to write Calibration Factors");
    log_msg(ERROR, msg); 	
    sf_close_write (diaref);
    return(-2);
  }

  /* Write parameter-set (METHODS & PARAMS-A)*/ //LBarrera
   if ((err =write_methods (diaref, rs)))
	{
	sfherror (err, "(METHODS)");
        sprintf(msg, "Failed to write parameter set METHODS");
        log_msg(ERROR, msg); 
	//sf_close_write (diaref);
    	//return(-3);
	}


 /* Write parameter-set PARAMS-A*/  //LBarrera
 /* Note: not all the parameters are written (i.e. parms-A shift after shot)*/
  if ((err =write_channel_settings (diaref, rs)))
	{
	sfherror (err, "(parms-A)");
        sprintf(msg, "Failed to write parameter set parms-A");
        log_msg(ERROR, msg); 
	//sf_close_write (diaref);
   	//return(-4);
	}

  /* Closinf file*/
   sprintf(msg, "Closing %s (%s) %d",diag,exper,nshot);
  log_msg(INFO, msg);	
  sf_close_write (diaref);
  return(0);
}

/* ---------------------------------------------------------------------
   write an RMD shot file 

   (a) linear calibration
   MULTI00    radiation temperature (eV) / main amp input voltage (V)
   SHIFT00    offset compensation (if measurement available after a shot) 
                 (parameter "shift" contains value per channel 0..59)
              or zero (if RMD is written before the shot)
	         (parameter "shift" is null pointer [NULL])
  Note: Te(eV)=MULTI00*Traw(V)+SHIFT00

   (d) METHODS, parms-A parameters   (a la RAD, CEC)
      (to be implemented later, using write_methods() 
      and write_channel_settings in cec/writecec.c )

   (c) position of channels (compatible with CEC shot file format)
      R-A, z-A, time-A
      (to be implemented later, using write_channel_positions() of 
      cec/writecec.c)

   --------------------------------------------------------------------- */

int   write_rmd        (int shot, 
			 struct RadiometerSettings *rs,
			 struct ChannelPositions *pChanPos,
			 struct ChannelData *pChanData,
			 struct SampleRateSettings *SRSet)
{
  PRM_HANDLE p;
  char msg[256], *diag="RMD", *exper;
  int type, diaref, stat, 
      nshot = shot, 
      edition = 0;
#ifdef DDWW8
  int32_t err;
#else
  int err;
#endif
  
  p = prm_get("rmd_experiment");
  if (!prm_getval(p, &exper, &type)) exper="AUGD";

  stat = sf_open_write(exper, diag, &nshot, &edition, &diaref);
  if (stat==SF_ERROR) 
    {
      sprintf(msg, "cannot open to write %s:%s %d (%d). ERROR = %d",  //include error 15 Jan 2013
	      exper, diag, nshot, edition,SF_ERROR);
      log_msg(ERROR, msg);
      return(-1);
    }

  //sprintf(msg, "Writing to %s:%s %d (%d)", exper, diag, nshot, edition);
  sprintf(msg, "Writing to %s:%s %d -%d (%d)", exper, diag, nshot, shot,edition);
  log_msg(INFO, msg);
 
  

  /* Write parameter-set (METHODS & PARAMS-A)*/ //LBarrera

   if ((err =write_methods (diaref, rs)))
	{
	sfherror (err, "(METHODS)");
        sprintf(msg, "Failed to write parameter set METHODS");
        log_msg(ERROR, msg); 
	}


 /* Write parameter-set PARAMS-A*/  //LBarrera
  if ((err =write_channel_settings_rmd (diaref, rs)))
	{
	sfherror (err, "(parms-A)");
        sprintf(msg, "Failed to write parameter set parms-A");
        log_msg(ERROR, msg); 
	}


 /* Write calibration factors eCAL eV/V */  //LBarrera
  if ((err =write_calfacts (diaref, rs)))
	{
	sfherror (err, "(eCAL)");
        sprintf(msg, "Failed to write calibration factors RMD. Closing file"); //11.01.13 LBarrera
        log_msg(ERROR, msg); 
	sf_close_write (diaref);
    	return(-5);
	}

 /* writing position information per channel will go here Feb 2013*/
  if ((err=write_channel_positions(diaref, rs, pChanPos)))
  {
	sfherror (err, "(Channel position)");
        sprintf(msg, "Failed to write channel position");
        log_msg(ERROR, msg); 
	sf_close_write (diaref);
        return(-6);
  }
  //Write shot_list for EQUIL:
 if (write_sflist_equil(diaref, shot, rs))
  {
	sfherror (err, "(Channel SF list)");
        sprintf(msg, "Failed to write SFLIST with equilibrium");
        log_msg(ERROR, msg); 
        sf_close_write (diaref);
        return(-7);
  }
 


/* writing calibrated data  Feb 2013*/
if (write_channel_data(diaref, rs, pChanData))
  {
    sfherror (err, "(Channel data)");
    sprintf(msg, "Failed to write Trad");
    log_msg(ERROR, msg); 	
    sf_close_write (diaref);
    return(-8);
  }

/*Closing file*/
  sprintf(msg, "Closing %s (%s) %d",diag,exper,nshot);
  log_msg(INFO, msg);	
  sf_close_write (diaref);
  return(0);
}






#define GROUP "RMD"

void init_rmdsf(void)
{
  prm_group  (GROUP, "RMD");

  prm_define ("rmd_experiment", "(string)", "AUGD", 
              "Experiment code for RMD (calibration) shot file",
              GROUP, NULL);  
}
