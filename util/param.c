/*
	param.c - Verwaltung von Programmparametern & Benutzerdialog
		  
	Wolfgang Suttrop, suttrop@ipp.mpg.de

	13.09.93  wls  neu
	22.02.94  wls  auch unvollstaendige Parameternamen werden erkannt
	               bzw. mit <TAB> vervollstaendigt,
	               Kommandozeileneditor
	01.05.94  wls  Endung ".(Programmname)" fuer skripts
	               Startup-skript ".(Programmname)"
	16.10.94  wls  "show\n" zeigt alle Parameter
	10.05.95  wls  <TAB> nach (Parametername) + '=' holt
	               Werte zum Editieren in die Kommandozeile
	25.03.10  wls  cleanup, removed old platforms
	16.06.10  wls  user-defined parameter check (prm_define_check())
	               error() uses log_msg() (logging.c, include "util.h")

*/


#include <ctype.h>
#include <stdlib.h>

#ifdef sun
#include <floatingpoint.h>
#endif

#include <malloc.h>
#include <memory.h>
#include <string.h>
#include <stdio.h>

#include "util.h"
#include "param.h"


#define COMMAND_EDITOR	   /*compile with interactive editing aid*/




/* ======= Parameterliste als binaerer Baum ========= */

/* --------- Datenstruktur der Parameterliste ------- */


typedef	struct dataelement {
	  struct  dataelement	*next;	/* link to successor element */
	  char		val[VALSIZE];	/* data or type value string */
	  int           type_code;      /* type identification code */
	  } de;

static	struct	listelement {
	  struct listelement *left,  *right;  /* tree branches */
	  char		   *name,	/* identifying name */
	  		   *help,	/* explaining string */
	  		   *group;	/* parameter group name */
	  void		   (*action)();	/* semantic action */
          int              (*usercheck)();  /* parameter checking function */
	  struct dataelement *values,	/* chain of succesive values */
	  		   *types,	/* chain of alternative data types */
	  		   *current;    /* current value for prm_get */
	  }  	*parameters = NULL,	/* list of parameters */
	  	*groups = NULL;		/* list of groups */


/* --------- search for (existing) element --------- */
	

#define NO_MATCH  32767
#define AMB_MATCH 32766

static struct listelement *find_prm(name, list, diff) 
  char	  *name;
  struct  listelement *list; 
  int     *diff;  
  {
  struct  listelement *llist, *rlist;
  int	  order, dif, ldif, rdif;
  char    *p, *q;
  
  if (list) {
    if ((order = strcmp(list->name, name))) {    /* not identical */

/* is search pattern compatible with current node? */

      if (*(p=list->name) == *(q=name)) {
	dif = strlen(p);
        while (dif && (*p++ == *q)) {--dif; ++q;}

	if (! *q) {  /* search pattern is included in node label */

	  /* examine left branch to possibly find a closer match */

          llist = find_prm(name, list->left, &ldif);
          if (ldif==0)              {*diff = ldif; return(llist);}
	  else if (ldif==AMB_MATCH) {*diff=AMB_MATCH; return(NULL); }

          /* note: the right branch is only searched to find
	     possible ambiguities. It CANNOT yield a better match! */

          rlist = find_prm(name, list->right, &rdif);

/* yes: return closest subnode if no ambiguity exists */

	  if (rdif==AMB_MATCH || dif==ldif || dif==rdif
	      || (ldif!=NO_MATCH && rdif==ldif) )
	             {*diff=AMB_MATCH; return(NULL); }

          if (dif<ldif && dif<rdif)  
	                 {*diff=dif;  return(list);}
          if (ldif<rdif) {*diff=ldif; return(llist);}
          else	         {*diff=rdif; return(rlist);}
	  }
        }

/* no: descend in binary tree */

      if (order>0) return(find_prm(name, list->left,  diff));
      else	   return(find_prm(name, list->right, diff));

      }
    else   *diff=0;        /* identical match */
    }
  else   *diff=NO_MATCH;   /* empty node: no match */
  return(list);
  }	/*find_prm*/



/* ---------- insert new list element -------------- */
	
static	struct	listelement  *new_prm(name, list) 
  char	  *name;
  struct  listelement **list; {
  int	  dif;
  
  if (*list) {
    if ((dif = strcmp((*list)->name, name))) {
      if (dif>0) 
        return (new_prm(name, &((*list)->left)));
      else 
        return (new_prm(name, &((*list)->right)));
      }
    }
  else if (( (*list) = (struct listelement *) 
	     malloc(sizeof(struct listelement)) )) {
    (*list)->name = name;
    (*list)->left =   (*list)->right = NULL;
    (*list)->values = (*list)->types = 
    (*list)->current = NULL;
    }   
  return(*list);
  }	/*new_prm*/


/* ------ traverse list & act on each element ------ */

static	void	traverse (list, f) 
  struct  listelement  *list; 
  void	(*f)();  {
  
  if (list) {
    traverse(list->left, f);
    (*f)(list);
    traverse(list->right, f);
    }
  }	/*traverse*/


/* =========== output error messages =============== */

static void error(char *msg1, char *msg2)
{
  char msg[256];
  sprintf(msg, "%s%s\n", msg1, msg2);
  log_msg(ERROR, msg);
}

/* ========== maintain data/type chains ============ */

/* ----------- follow type & data chain ------------ */

static void follow (fp, chain)
  FILE *fp;
  struct dataelement *chain; {
  while (chain) {
    if (chain->val) fprintf(fp, "%s", chain->val);
/*  
    switch (chain->type_code) {
      case VOID:    fprintf(fp, " <void>"); break;
      case LITERAL: fprintf(fp, " <lit>"); break;
      case STRING:  fprintf(fp, " <string>"); break;
      case INTEGER: fprintf(fp, " <integer>"); break;
      case FLOAT:   fprintf(fp, " <float>"); break;
      }
*/
    chain = chain->next;
    if (chain) fprintf(fp, ",");
    }
  }	/*follow*/


/* ----------- discard data/type chain -------------- */

static	void	discard (list)
  struct dataelement *list; {
  struct dataelement *p;
  
  while ((p=list)) {
    list = p->next;
    free(p);
    }
  }	/*discard*/


/* ---- parse argument string & build data/type chain ---- */

static	int  parse(str, check, types, result)

  char	*str;  		       /* argument string */
  int	(*check)();	       /* hook to validity checking procedure */
  struct dataelement *types,   /* type list to pass on to check() */
        **result;              /* resulting value list */
  {
  char	*argum;
  struct dataelement *first, *p, *np;
  int	len;

  if (!str) {
    *result = NULL;
    if (check && (*check)(NULL, types)) return(0);
    return(1);
    }

  first = p = NULL;
  
  while (*str) {
    while (*str == ' ') str++;		/* isolate next argument */
    argum = str;
    while (*str && (*str!=' ') && (*str!=',') 
                && (*str!='\n') ) str++;
    len = str-argum;
    if (len>VALSIZE-1) len=VALSIZE-1;
    					/* insert in chain */
    if ((np = (struct dataelement *)
	 malloc(sizeof(struct dataelement)))) {
      np->next = NULL;
      strncpy(np->val, argum, len);
      *(np->val + len) = '\0'; 
      if (check && (*check)(np, types)) {  /* check validity */
        discard(first);			   /* if wrong: discard list .. */
        return(0);			   /* .. and exit */
        }
      }	 /* if malloc */
    if (p)	p->next = np;
    else	first = np; 
    p = np;
    
    if (*str) ++str;			/* skip separator */
    }	/* while (*str) */
/*
  printf("parse: "); follow(stdout, first); printf("\n");
*/
  *result = first;                      /* successful exit */
  return(1);
  }	/* parse */
  

/* ======== type definition and validation ========= */


/* -------- check & define type identifier --------- */

static int check_type (np, tl)
 
  struct dataelement *np, *tl; {
  if (np) {
    if(!np->val) return(-1);   /*no value: error!*/
    if (strcmp(np->val, "(void)")==0)
      np->type_code = VOID;
    else if (strcmp(np->val, "(string)")==0)
      np->type_code = STRING;
    else if (strcmp(np->val, "(integer)")==0)
      np->type_code = INTEGER;
    else if (strcmp(np->val, "(float)")==0)
      np->type_code = FLOAT;
    else np->type_code = LITERAL;
    }
  return(0);
  }    /* check_type */


/* ------ check if a value is of valid type -------- */

static int check_value (np, tl) 
  struct dataelement *np, *tl; {
  char   *ts, *vs;

/* allow for empty value list if no types present */
  if (!np && !tl) return(0);

/* go through typelist and check if one type matches */    
  while (tl) {
    if (np) np->type_code = tl->type_code;  /*tentatively*/
    switch (tl->type_code) {
      case VOID:    if (!np || *(np->val)=='\0') return(0);
                    break;
      case LITERAL: if (np && strcmp(tl->val, np->val)==0) return(0);
   		    break;
      case STRING:  return(0);    /* everything accepted as a string */
      case INTEGER: if (np) {
	              strtol((vs=np->val), &ts, 10);
		      if (ts != vs) return(0);  /*ok*/
		      }
                    break;
      case FLOAT:   if (np) {
	              strtod((vs=np->val), &ts);
		      if (ts != vs) return(0);  /*ok*/
                      }
                    break;
      }
    tl = tl->next;
    } /*while tl*/
      
/* no match: fall through to error exit */
  return(-1); 

  }    /* check_value */



/* ======== define parameters and groups =========== */


/* -------------- define parameters ---------------- */


/* define a parameter with action() and check() procedures 

   int check (struct dataelement *np, struct datalement *tl); 
   returns 0 if OK and !=0 if inconsistent argument *np */
void	prm_define_check (char *name, char *types, 
			  char *defaults, char *help,
			  char *group, void (*action)(), int (*check)())
{
  struct listelement *p;

  if ((p = new_prm(name, &parameters))) 
    {
    if (!parse(types, check_type, NULL, &(p->types))) 
      error ("prm_define: invalid typelist for ", name);
    if (!parse(defaults, check_value, p->types, &(p->values))) 
      error ("prm_define: invalid defaults for ", name);
    p->help = help;
    p->group = group;
    p->action = action;
    p->usercheck = check;
    }
  else error("prm_define: cannot define parameter ", name);
}  /*prm_define_check*/



/* define a parameter with action() and default (value) check */
void	prm_define ( char *name, char *types, char *defaults, 
		     char *help, char *group, void (*action)() )
{
  prm_define_check (name, types, defaults, help, group, action, NULL);
}	/*prm_define*/



/* ----------------- define groups ----------------- */

void	prm_group (name, help)
  char   *name, *help; {
  struct listelement *p;
  if ((p = new_prm(name, &groups))) p->help = help;
  else error("cannot define group: ", name);
  }	/*prm_group*/


/* ======= set & retrieve parameter values ========= */


/* -------------- set parameter value -------------- */

int	prm_set(name, value)
  char	*name, *value; {
  struct listelement *p;
  struct dataelement *nv;
  int diff;

  if ((p = find_prm (name, parameters, &diff))) {

    if (parse (value, check_value, p->types, &nv)) {
      
      if (p->usercheck)
	{
	struct listelement p1;  /* copy to test new parameters */
	memcpy(&p1, p, sizeof(p1));	
	p1.current = p1.values = nv;
	if ( (*(p->usercheck))((PRM_HANDLE) &p1) )
	  {
	    error(name, " : wrong parameter(s) (ignored)");
	    /* leave previous setting unchanged, skip action */
	    return(0);
	  }	  
	}
      discard (p->values);
      p->values = nv;
      if (p->action) {   /* call semantic function */
        p->current = p->values;
        (*(p->action))((PRM_HANDLE) p);
        }
      return(1);
      }
    else {
      error ("invalid assignment to ", p->name);
      if (p->types) {
        fprintf(stderr, "legal values: ");
        follow(stderr, p->types);
        fprintf(stderr, "\n");
        }
      }
    }  /*if found*/
  else
    if (diff==AMB_MATCH)
      error(name, " : ambiguous");
    else
      error(name, " : not found");
  return(0);
  }	/*prm_set*/


/* -------------- get parameter values ------------- */

int  prm_getval(p, value, type) 
  PRM_HANDLE p; 
  char  **value; 
  int    *type;  {
  struct dataelement *dp;

  if (p && (dp=((struct listelement *)p)->current)) {
    if (value) *value = dp->val;
    if (type)  *type  = dp->type_code;
    ((struct listelement *)p)->current = dp->next;
    return(1);
    }
  return(0);
  }	/*prm_getval*/


PRM_HANDLE prm_get(name) 
  char	*name; {
  struct listelement *p;

  int diff;
  if ((p = find_prm (name, parameters, &diff))) {
    p->current = p->values;
    return((PRM_HANDLE) p);
    }
  if (diff==AMB_MATCH)
      error(name, " not found (ambiguous)");
    else
      error(name, " not found");
  return(NULL);
  }	/*prm_get*/


/* ================ command parser ================= */

static  void  parse_cmd (command)
  char *command; {

  char  ch, *cmd, *keywd, *argums;
  int	diff = 0;

  cmd = command;
  if (!cmd || !*cmd) return;

  while(isspace(*cmd)) ++cmd;

  if (isalpha(*cmd)) {	/*literal command or parameter name*/
    keywd=cmd;
    while(isalnum(ch = *cmd) || ch=='_') cmd++;
    *cmd = '\0';

    if ((ch == '=') || (ch == ' ')) {      /*args follow*/
      ++cmd;
      while (isspace(*cmd)) ++cmd;
      argums = cmd;
      }
    else argums = NULL;

/* assume keywd is an internal command/parameter */
    if (find_prm(keywd, parameters, &diff)) 
      {
      prm_set (keywd, argums);
      }

/* it may also be a script file to run */
    else if (prm_runscript(keywd, argums))
      {
/* or nothing at all */
      if (diff==AMB_MATCH)
        error ("ambiguous: ", keywd);
      else
        error (keywd, " not found");
      }

    }	/*if isalpha*/
  }	/*parse_cmd*/


/* ========= command line editor ================= */

#ifdef COMMAND_EDITOR

#define HISTORYLEN  4192     /* size of history buffer in characters */

/* console output characters */

#define BEL  7
#define NUL  0

/* keyboard control characters */

#define INSERT  0
#define BEGIN   1   /* ^A  beginning of line */
#define END     5   /* ^E  end of line */
#define LEFT    2   /* ^B  back char */
#define RIGHT   6   /* ^F  forward char */
#define UP     16   /* ^P  previous line */
#define DOWN   14   /* ^N  next line */
#define BS	8
#define TAB 	9   /* complete keyword */
#define CR     13
#define ESC    27
#define DEL   127


static  int  	insertmode = 1;
static	char	history[HISTORYLEN],
                *histhead = history,
                *histtail = history;

static	char	*hnext(p)
  char *p;
  {
  ++p;
  if (p-history >= HISTORYLEN) return(history);
  return(p);
  }  /*hnext*/  

static	char	*hprev(p)
  char *p;
  {
  --p;
  if (p < history) return(history+HISTORYLEN-1);
  return(p);
  }  /*hprev*/  


static  void  edit_line(s, l)
  char *s;
  int   l;
  {
  int  c, diff;
  char cuc, *p, *q, *q1, *pm, *hist;
  struct listelement *prm;
  struct dataelement *val;

  system("stty -echo raw");
  p = pm = s;
  hist = histhead;

  while ((c=getchar())!=CR) {

/* convert VT100 ESC sequences into single control characters */

    if (c==ESC && (c=getchar())==91)
      switch (c=getchar()) {
        case 65: c=UP;    break;
        case 66: c=DOWN;  break;
        case 67: c=RIGHT; break;
        case 68: c=LEFT;  break;
	case 50:
	  if ((c=getchar())==126) c=INSERT;
	  else if ((c=getchar())==52 && (c=getchar())==55
		&& (c=getchar())==122) c=INSERT;
	default:
	  putchar(BEL); continue;
        }

/* interprete single control characters */

    switch (c) {
      case INSERT: insertmode = !insertmode; break;
      case BS:
      case DEL:		/* delete back character */
        if (p==s) break;
	--p;
	putchar((char) BS); 

	if (insertmode) {
	  q=p; --pm;
	  while (q<pm) {
	    putchar(*q = *(q+1)); ++q;
	    }
	  putchar(' '); putchar((char) BS);
	  while (q>p) {
	    putchar((char) BS); --q;
	    }
	  }
	else {
	  if (p+1 == pm) --pm;
	  else *p = ' ';
	  putchar(' '); putchar((char) BS);
	  }
        break;

      case LEFT:         /* go back one character */
        if (p==s) break;
	--p;
	putchar((char) BS);
        break;

      case RIGHT:        /* advance one character */
        if (p<pm) putchar(*p++);
        break;

      case BEGIN:        /* goto beginning of line */
        while (p>s) {
	  putchar((char) BS);
	  --p;
	  }
        break;

      case END:         /* goto end of line */
        while (p<pm)
	  putchar(*p++);
        break;

      case UP: 		/* scroll history backward */
	q1 = hist;
	if (q1!=histtail)
          q1 = hprev(q1);		/* previous line NUL character */
	if (q1!=histtail)
	  q1 = hprev(q1);		/* previous line last character */
	while (q1!=histtail && *q1)     /* skip to first char */
	  q1 = hprev(q1);
	if (q1==hist) break;
	if (!(*q1)) q1 = hnext(q1);
	hist = q1;
	goto replace;

      case DOWN:	     /* scroll history forward */
	q1 = hist;
	while (q1!=histhead && *q1) 	/* skip to last character */
	  q1 = hnext(q1);
	if (q1!=histhead && !(*q1))	/* skip NUL character */
	  q1 = hnext(q1);
	if (q1==histhead && *q1) q1=hprev(q1);
	hist = q1;

replace:                     
	while (p>s) {  	     /* goto begin of line */
	  putchar((char) BS);
	  --p;
	  }
	q1 = hist;           /* copy current line from history buffer */
	while (*q1 && p+1<s+l) {
	  putchar(*p++ = *q1);
	  q1 = hnext(q1);
          }
	q=p;
	while (q<pm) {	     /* delete remainder of old line */
	  putchar(' '); ++q;
	  }
	while (q>p) {
	  putchar((char) BS); --q;
	  }
	pm = p;
	break;

      case TAB:
	q=p; while(q>s && *(q-1) !=' ') --q;  /* beginning of word */

	/* if last character is a '=' then edit parameter value */

	if (p>s && *(p-1) == '=') {
	  *(p-1) = '\000';   	      /* end of keyword */
	  if ((prm = find_prm(q, parameters, &diff)) &&
	       diff != AMB_MATCH) {
	    *(p-1) = '=';
	    val = prm->values;
	    while (val) {
	      q1 = val->val;
	      diff = strlen(q1) + (val->next != NULL);

	      if (insertmode) {   /* create space for 'diff' characters */
	        if (pm+diff >= s+l) diff = s+l-pm-1;
	        q = (pm += diff);
	        while (q>p) { 
	          *q = *(q-diff); --q;
	          }
	        }  /* if insertmode */

	      while (*q1 && p+1<s+l && pm+1<s+l) {   /* copy value string */
	        putchar (*p++ = *q1++);
	        if (p>pm) pm = p;
	        }

	      if (val->next) {		    /* insert parameter separator */
		putchar(*p++ = ',');
	        if (p>pm) pm = p;
		}

	      val=val->next;	/* goto next value */
	      }
	    }
	  else {		/*parameter name not found*/
	    *(p-1) = '=';
	    putchar ((char) BEL);
	    }
	  break;
	  }

	/* not a '=' : search & complete keyword */

	cuc = *p; *p = '\000';   	      /* end of word */
	if ( (prm = find_prm(q, parameters, &diff)) !=NULL  || 
	     ((q>s) &&  (prm = find_prm(q, groups, &diff)) !=NULL) ) {
	  if (diff==AMB_MATCH) {
	    *p = cuc;
	    putchar ((char) BEL);
	    }
	  else {
	    q1 = prm->name;
	    while (*q && *q==*q1) {++q; ++q1;}  /* skip part already entered */
	    *p = cuc;

	    if (insertmode) {   /* create space for 'diff' characters */
	      if (pm+diff >= s+l) diff = s+l-pm-1;
	      q = (pm += diff);
	      while (q>p) { 
	        *q = *(q-diff); --q;
	        }
	      }  /* if insertmode */

	    while (*q1 && p+1<s+l && pm+1<s+l) {   /* insert missing chars */
	      putchar (*p++ = *q1++);
	      if (p>pm) pm = p;
	      }

	    if (p+1<s+l && pm+1<s+l) {   /* add a blank ' ' */
	      putchar (*p++ = ' ');
	      if (p>pm) pm = p;
	      }
	    
	    goto refresh;
	    }
	  }
	else {
	  putchar((char) BEL);
	  *p = cuc;
	  }
	break;

      default:          /* insert printing character */
        if (p+1 < s+l) {      /* buffer large enough? */
	  if (insertmode) {   /* create space */
	    q = pm+1<s+l ? pm++ : pm-1;
	    while (q>p) { 
	      *q = *(q-1); --q;
	      }
	    }
          *p++ = (char) c;
          putchar(c);
	  if (p>pm) pm = p;
          }
        else putchar((char) BEL);
	if (!insertmode) break;

refresh:              /* refresh remainder of line */
	if ((q=p)<pm) {
	  while (q<pm) putchar(*q++);
	  while (q>p)  {
	    putchar((char) BS); --q;
	    }
	  }

      }   /* switch c */
    }   /* while !CR */

  *pm = (char) NUL;
  system("stty echo -raw");
  putchar ('\n');

/* enter new command into history buffer */

  if (pm>s) {	/*non-empty*/
    q=s;
    while (q<=pm) {	/* copy including NUL character */
      *histhead = *q++;
      histhead = hnext(histhead);
      if (histhead==histtail) { /* history buffer full */
	while (*histtail) histtail = hnext(histtail);  /* remove oldest line */
	if (!(*histtail)) histtail = hnext(histtail);
        }
      }
    }

  } /*edit_line*/

#endif   /*COMMAND_EDITOR*/


/* ======== actions for default commands ========= */

static	char	*program_name;


/* ----------- interactive dialog --------------- */

static	int	stay;

static  void  do_quit (pars)
  struct dataelement *pars; {
  stay = 0;
  }

static  void  do_cmd (p)  
  PRM_HANDLE p; 
  {
  char	buffer[BUFSIZE];
  int	oldstay;
  
  oldstay = stay;  /* save */
  stay = 1;
  do {
    printf("(%s) ", program_name);

#ifdef COMMAND_EDITOR
    edit_line(buffer, BUFSIZE);
#else
    gets(buffer);
#endif

    parse_cmd(buffer);
    } while (stay);
  stay = oldstay;
  }	/*do_cmd*/


/* ------------ process script file ---------------- */


int prm_runscript(char *name, PRM_HANDLE args)
{
  FILE *fp;
  char	buffer	[BUFSIZE],
        fn	[PATHSIZE];

  sprintf(fn, "%s.%s", name, program_name);
  
  if ((fp = fopen(fn, "r"))) {
    while (fgets(buffer, BUFSIZE, fp))	parse_cmd(buffer);
    fclose(fp);
    return(0);
    }
  return(-1);
}  /*prm_runscript*/



static	void	do_exec (p)
  PRM_HANDLE p; 
  {
  char	*name;
  int   type;
  if (!prm_getval(p, &name, &type) || prm_runscript(name, p))
    error("cannot open: ", name);
  }	/*do_exec*/ 


/* ---- save parameter values into "exec"-utable script file ------ */

static	FILE	*save_file;


static	void	save_hook(lp)
  struct listelement *lp; {
  if (!(lp->action)) {
    fprintf(save_file, "%s=", lp->name);
    follow(save_file, lp->values); 
    fprintf(save_file, "\n");
    }
  }	/*save_hook*/


static	void  do_save(p)
  PRM_HANDLE p; 
  {
  char	fn [PATHSIZE],
        *fnp;
  int   fnt;

  if (prm_getval(p, &fnp, &fnt)) 
    sprintf(fn, "%s.%s", fnp, program_name);
  else
    sprintf(fn, ".%s", program_name);
  
  if ((save_file = fopen(fn, "w"))) {
    traverse(parameters, save_hook);
    fclose(save_file);
    }
  else 
    error("cannot write on ", fn);
  }	/*do_save*/ 


/* --------------- exit program ------------------ */

static	void	do_exit (p)
  PRM_HANDLE p; { 
  exit(0);
  }	/*do_exit*/



/* -------------------- help ----------------------- */

static	void	grouphelp_hook(lp)
  struct listelement *lp; {
  printf("%s", lp->name);
  if (lp->help)   printf("\t\t%s", lp->help);
  else            printf("\t\t(no further information available)");
  printf("\n");
  }	/*grouphelp_hook*/


static	char	*helpgroup;

static	void	parhelp_hook(lp)
  struct listelement *lp; {
  if (strcmp(lp->group, helpgroup) == 0) {
    printf("%s", lp->name);
    if (lp->action) printf("*");
    if (lp->types)   { printf("\t"); follow(stdout, lp->types); }
    else	    printf("\t\t");
    printf("\t%s\n", lp->help);
    }
  }	/*parhelp_hook*/



static	void	do_help(p)
  PRM_HANDLE p; { 
  char   *topic; 
  struct listelement *lp, *gp;
  int    topt, diff;

  if (!prm_getval(p, &topic, &topt)) topic=NULL; 
  printf("\n");
  
  if (topic && *topic) {
  
/* ..................... group name ? ...................... */

    if ((gp = find_prm(topic, groups, &diff))) {
      printf("Parameters/commands in group: ");
      grouphelp_hook(gp);
      printf("\n");
      helpgroup = gp->name;
      traverse (parameters, parhelp_hook);
      }
      
/* ................... parameter name ? .................... */
      
    else if ((lp = find_prm(topic, parameters, &diff))) {
      printf("parameter: %s", lp->name);
      if (lp->action) printf("*");
      printf("\ntype(s):   "); follow(stdout, lp->types); 
      printf("\nvalues:    "); follow(stdout, lp->values); 
      printf("\nsemantics: %s\n", lp->help);   
      }
    else error(topic, ": help topic not found");
    printf("\n");
    }

/* ...... no specific request: general information ......... */     

  else {
    printf("Defined command or parameter groups are:\n\n");
    traverse(groups, grouphelp_hook);  
    printf("\nFor a list of commands and parameters in a group type:\n");
    printf("\n\thelp (group-name)\n");
    printf("\nTo obtain information on a particular command or");
    printf(" parameter type:\n\n\thelp (cmd-or-prm-name)\n\n");
    }
  }	/*do_help*/
  

/* -------------------- show ----------------------- */


static	void	show_all_hook(lp)
  struct listelement *lp; {
  if (!(lp->action)) {
    fprintf(stdout, "%s=", lp->name);
    follow(stdout, lp->values); 
    fprintf(stdout, "\n");
    }
  }	/*show_all_hook*/


static	void	show_hook(lp)
  struct listelement *lp; {

  if (strcmp(lp->group, helpgroup) == 0)
      show_all_hook(lp);

  }	/*show_hook*/
  


static void     do_show (p)
  PRM_HANDLE p; {
  struct listelement *gp, *lp;
  char *parval;
  int  partype, diff;

  if (prm_getval(p, &parval, &partype)) {	/*parameter given*/
    do {
      if ((gp = find_prm(parval, groups, &diff))) {  /*list group*/
        helpgroup = parval;		/*list all members*/
        traverse (parameters, show_hook);
        }
      else if ((lp = find_prm(parval, parameters, &diff))) { /*parameter*/
        helpgroup = lp->group;
        show_hook(lp);       
        }
      else error(parval, " not found");  
      } while (prm_getval(p, &parval, &partype));
    }
  else 	/* no parameters given, print complete list */
    traverse (parameters, show_all_hook);
  }     /*do_show*/



/* ============ command line shell ================= */

void	prm_main(argc, argv)
  int	argc;
  char	**argv; {
  char	*group = "prog",
        *p, *p1;


/* strip path from program name  */

  p = p1 = *argv;
    while (*p1) {
      while (*p1 && *p1!='/') ++p1;
      if (*p1) {++p1; p=p1;}
      }
  program_name = p;
  
/* define default & program flow commands/parameters */

  prm_group (group, "program flow commands");

  prm_define ("exit", NULL, NULL,     
  		"\t\texit program", group, do_exit);
  prm_define ("quit", NULL, NULL,     
  		"\t\tleave interactive mode", group, do_quit);
  prm_define ("help", "(string)", NULL,    
  		"[[=]topic]\tparameter & command info", group, do_help);
  prm_define ("show", "(string)", NULL,    
  		"[[=]parameter]\tshow parameter value", group, do_show);
  prm_define ("exec", "(string)", *argv,    
  		"([=]path)\texecute batch file", group, do_exec);
  prm_define ("save", "(string)", *argv,    
  		"([=]path)\tsave parameters as batch file", group, do_save);
  prm_define ("cmd",  "(string)", *argv, 
  		"[[=]prompt]\tenter interactive command mode", 
  		group, do_cmd);

/* try to execute startup file */

  prm_runscript ("", NULL);

/* then go through all command line parameters */

  while (--argc) parse_cmd(*++argv);     

  }	/*prm_main*/


/* ------------- test main program -------------- */


#ifdef TEST

#define GROUP "plas"

main(argc, argv) 
  int  argc;
  char **argv; {

  PRM_HANDLE p; 
  
  prm_group (GROUP, "sources of plasma parameters");
  
  prm_define ("p2", "JAU,UVD,(float)", "JAU, UVD, 2.5",  
  		"toroidal magnetic field", GROUP, NULL);
  prm_define ("p2p2", "JAU,UVD,(float)", "800.0", 
  		"total plasma current", GROUP, NULL);
  prm_define ("p3", "JAU,UVD,(float)", "12.0", 
  		"xxx", GROUP, NULL);

  prm_main (argc, argv);

  do_cmd(p);
  }	/*main*/

#endif
