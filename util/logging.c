/*
   logging.c - log messages on different levels

   Wolfgang Suttrop, wls@ipp.mpg.de

   23-Feb-94   new
   25-Mar-2010 modernised

*/

#include <stdio.h>
#include <string.h>
#include <time.h>

#include "param.h"
#include "util.h"

#define MSGLEN 256

/* actually print a logged massage */

static void print_msg(FILE *fp, char *prefix, char *msg)
{
  fprintf(fp, "%s %s\n", prefix, msg);
}



static char *msg_type_name[4] = 
  {"error", "warning", "info", "debug"};

static char *msg_prefix[4] = 
  {"ERROR  ", "WARNING", "INFO   ", "DEBUG  "};

/* ANSI ecape sequences - may or may not work */
static char *msg_prefix_color[4] = 
  {"\e[31mERROR  \e[0m",    /* red */
   "\e[33mWARNING\e[0m",    /* orange */
   "\e[32mINFO   \e[0m",    /* green */
   "\e[35mDEBUG  \e[0m"};   /* dark magenta */


/* ........ log a message ....... */

void log_msg(enum msg_type mt, char *msg)
{
  PRM_HANDLE p;
  char *s;
  int type;
  char  *logfile;

  /* print to screen if needed */
  p = prm_get("log_to_screen");
  while (prm_getval(p, &s, &type)) 
  {
    if (strcmp(msg_type_name[mt],s)==0)
      print_msg(stdout, msg_prefix_color[mt], msg);
  }

  /* append to logfile if needed */
  p = prm_get("logfile");
  if (prm_getval(p, &logfile, &type)) 
  {
    FILE *fp;
    if (logfile && *logfile) 
    {
      if ((fp=fopen(logfile, "a+"))) 
      {
	p = prm_get("log_to_file");
	while (prm_getval(p, &s, &type)) 
	{
	  if (strcmp(msg_type_name[mt],s)==0)
	    print_msg(fp, msg_prefix[mt], msg);
        }
      fclose(fp);
      }
      else 
        print_msg(stdout, 
		  "PANIC - Cannot append to log file:", logfile);
    }

  }
}  /* log_msg */



void log_timestamp (void)
{
  char msg[MSGLEN];
  long actime;

  if (time(&actime)== -1) 
  {
    log_msg(ERROR, "Failed to determine system time.");
    return;
  }

  sprintf(msg, "%.24s", ctime(&actime));
  log_msg(INFO, msg);
} /* log_timestamp */




void init_logging(void)
{
  prm_group  ("logging", "logging controls");

  prm_define ("logfile", "(void),(string)", "default.log", 
              "\tname of disk file or none",
              "logging", NULL);

  prm_define ("log_to_file", 
	      "(void),error,warning,info,debug", 
	      "error,warning,info", 
              "\tmessage types to log to disk file",
              "logging", NULL);

  prm_define ("log_to_screen", 
	      "(void),error,warning,info,debug", 
	      "error,warning,info", 
              "\tmessage types to print on screen",
              "logging", NULL);
}

