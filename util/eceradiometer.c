/* eceradiometer.c 

   evaluate ECE radiometer parameters
   (common part, for radctrl, CEC, ...)


   09-Jun-2010  Wolfgang Suttrop
   Modifications: 
	Change wg LSB and USB for mixer 101. (Sept-2012, LBarrera)
	Include harmonic number (one per IF group or reciver) (Sept-2012, LBarrera)
 
   16-Jan-2016  Matthias Willensdorfer
   Modifications: 
   Jan-2020  Matthias Willensdorfer
   Modifications:
        added 101 USB mixer

   Nov-2021
   added 105GHz Mixer

 First  
 
*/

#include <math.h>
#include <memory.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "libece.h"
#include "param.h"
#include "util.h"
#include "eceradiometer.h"




/* ------------- consistency checks ------------------------- */


/* subroutine: check LO connections. Report errors (and return non-zero) */

// added forbidden_if2 and 3 to allow more than one forbidden If
                                             // 1        2       3       4      5        6       7       8       9      10      11     12      13    14
                                             //"none", "101U", "101L", "95U", "133L", "133U", "128U", "128N", "167L", "167U", "101S", "91L", "91U", "105U"
static int forbidden_if [MAX_LO_CONNECTIONS] = {-1,      2,      2,      1,      0,      0,      0,      2,      0,      0,     2,    1,      1,     2};
static int forbidden_if2 [MAX_LO_CONNECTIONS] ={-1,      2,      2,      1,      0,      1,      1,      2,      1,      1,     2,    2,      2,     2};
static int forbidden_if3 [MAX_LO_CONNECTIONS] ={-1,      2,      2,      1,      0,      2,      2,      2,      2,      2,     2,    2,      2,     2};
/*static char *LOstring [MAX_LO_CONNECTIONS] = 
  {"none", "101L", "101U", "95U", "133L", "133U", "128U", "128N", "167L", "167U"};*/
/*After Sept-2012, change wg 6-7 Mixer 101 USB and LSB*/
static char *LOstring [MAX_LO_CONNECTIONS] = 
  {"none", "101U", "101L", "95U", "133L", "133U", "128U", "128N", "167L", "167U", "101S", "91L", "91U", "105U"};

static int check_receivers (enum LO_Connection lo[MAX_IFGROUP])
{
  int i, j, ret = 0;
  char msg[256];
  char *valstr;
  int type;
  uint ECRH105 = 1;
 
  if (prm_getval (prm_get("ECRH105"), &valstr,  &type))
  {
    if (strcmp(valstr,"false") == 0) ECRH105 = 0;
  }
  else
  {
        sprintf(msg, "Warning error when loading paramter ECRH105 -> ECRH105 = True assumed");
        log_msg(WARNING, msg);
  }
  for (i=0; i<MAX_IFGROUP; ++i)
  {

    if (lo[i] == none)
    {
      sprintf(msg, "IF group %d not connected", i+1);
      log_msg(WARNING, msg);
    }
    // 128U, 101S is not in use anymore, mwillens, 91U too
    if ((lo[i] == LO167L) || (lo[i] == LO167U)|| (lo[i] == LO128U) || (lo[i] == LO91U) || (lo[i] == LO101S))
    {
      sprintf(msg, "Mixer %s not in use",
	      LOstring[lo[i]]);
      log_msg(ERROR, msg);
      lo[i] = none;
      ret = -1;
    }   
    // check forbidden if settings
    if (i==forbidden_if[lo[i]] || i==forbidden_if2[lo[i]] || i==forbidden_if3[lo[i]] )
    {
      sprintf(msg, "IF group %d cannot be connected to mixer %s",
	      i+1, LOstring[lo[i]]);
      log_msg(ERROR, msg);
      lo[i] = none;
      ret = -1;
    }
    
    if (ECRH105)
    {
          // Do not use these Mixers, when 105GHz is active, add 101L because it is linked to 95U, mwillens       
      if ( (lo[i]==LO95U) || (lo[i]==LO91L) || (lo[i]==LO101S) || (lo[i]==LO101U))
      {
        sprintf(msg, "Mixer %s in IF group %d must not be used with 105GHz ECRH",
              LOstring[lo[i]],i+1);
        log_msg(ERROR, msg);
        lo[i] = none;
        ret = -1;
      }
    }
    
    for (j=0; j<i; ++j)  /* conflicts with other units? */
    {
               // new Mixer is conflicting with 101S
      if (    (lo[i]==LO101L || lo[i]==LO101U || lo[i]==LO105U )
	   && (lo[j]==LO101L || lo[j]==LO101U || lo[j]==LO105U)
	   && (lo[i] != lo[j]) )
               // new Mixer is conflicting with 101S
	//     if (    (lo[i]==LO91L || lo[i]==LO91U || lo[i]==LO101S)
	//   && (lo[j]==LO91L || lo[j]==LO91U || lo[j]==LO101S)
	//   && (lo[i] != lo[j]) )
      {
	sprintf(msg, "Cannot use mixers %s (IF%1d) and %s (IF%1d) simultaneously",
		  LOstring[lo[j]], j+1, LOstring[lo[i]], i+1);
        log_msg(ERROR, msg);
	lo[i] = none;
	ret = -2;
      }
    }    
  }
  return(ret);
} /*check_receivers*/




/* subroutine: LO/sideband string to enumeration variable */

static enum LO_Connection string_to_lo (char *str)
{
  enum LO_Connection lo;
  /* go until the last entry which is on 22.11.21 LO105U*/
  for (lo=none; lo<=LO105U; ++lo)
    if (strcmp(str,LOstring[lo])==0)
      return(lo);
    
  return(none);
}  /* string_to_lo */





/* parse user parameters and set radiometer accordingly 
   returns:
   0 if everything set and at least some channels useful 
   -1 if an error occurred and settings are not useful
   -2 if no calibration factors found but otherwise channels are useful */

int parse_radiometer_parameters (int shot, struct RadiometerSettings *pRadSet)
{
  
  int i, type, nbadchan,
      error, 
      refmask = 0;  /* items to get from reference shot file */
  char *valstr, msg[1024], fn[512];
  PRM_HANDLE p;
  struct caldata cal;
 
  if (!pRadSet) return(-1);
	
  /* miscellaneous (semi-fixed) radiometer settings */
  prm_getval (prm_get("receivers"), &valstr, &type);
  if (strcmp(valstr, "ref")==0)
    refmask |= ECEREF_WAVEGUID 
	    | ECEREF_ATTNIF   | ECEREF_IFGROUP
	    | ECEREF_FREQIF   | ECEREF_VGAIN
            | ECEREF_VBANDW;
 
 
  /* calibration source */
  prm_getval (prm_get("calibsrc"), &valstr, &type);
  if (strcmp(valstr, "ref")==0)
    refmask |= ECEREF_CALIBSRC;
  else
    pRadSet->calibsrc = atoi(valstr);


  /* lens position */
  prm_getval (prm_get("zlens"), &valstr, &type);
  if (strcmp(valstr, "ref")==0)
    refmask |= ECEREF_ZLENS;
  else
    pRadSet->zlens = atof(valstr) * 0.01;  /* cm -> m */

  /* assignment of receivers to IF groups */
  p=prm_get("receivers");
  for(i=0; i<MAX_IFGROUP && prm_getval (p,&valstr,&type); ++i)
  {
    if(strcmp(valstr,"ref")==0)
      {
        refmask |= ECEREF_RECEIVER;
        goto l999;
      }
    else if(strcmp(valstr,"bt")==0)
      { 
        log_msg(DEBUG, "receiver (LO) settings according to Bt requested");
	/* receivers already set in radctrl */
        goto l999;
      }
    else
      pRadSet->LO[i] = string_to_lo(valstr);
  }

  for (; i<MAX_IFGROUP; ++i)   /* if less than MAX_IFGROUP bands given */
    pRadSet->LO[i] = none;

 l999:

  /* Harmonic number (one per IF group or mixer) LBarrera Sept 2012*/
   /* assignment of harmonic number to IF groups */
  p=prm_get("harmonic");
  for(i=0; i<MAX_IFGROUP && prm_getval (p,&valstr,&type); ++i)
  {
    if(strcmp(valstr,"ref")==0)
      {
        refmask |= ECEREF_HARMONIC;
        goto labelh;
      }
    else
     pRadSet->harmonic[i] = atoi(valstr);
	
  }

  for (; i<MAX_IFGROUP; ++i)   /* if less than MAX_IFGROUP bands given */
    pRadSet->harmonic[i] = 2;  /* Second harmonic by default*/
    
 labelh:

 
  /* read reference shot file if needed */
  if (refmask)
  {
    char   *edstr = "0", *refshotstr="0", *exper, *diagn;
    int    type, edition, refshot;

    sprintf(msg, "Obtain from reference:%s%s%s%s%s%s%s%s%s%s", 
	    refmask & ECEREF_CALIBSRC ? " CALIBSRC" : "",
	    refmask & ECEREF_HARMONIC ? " HARMONIC" : "",
	    refmask & ECEREF_ZLENS    ? " ZLENS" : "",
	    refmask & ECEREF_WAVEGUID ? " WAVEGUID" : "",
	    refmask & ECEREF_RECEIVER ? " RECEIVER" : "",
	    refmask & ECEREF_ATTNIF   ? " ATTNIF" : "",
	    refmask & ECEREF_IFGROUP  ? " IFGROUP" : "",
	    refmask & ECEREF_FREQIF   ? " FREQIF" : "",
	    refmask & ECEREF_VGAIN    ? " VGAIN"  : "",
	    refmask & ECEREF_VBANDW   ? " VBANDW" : "" );
    log_msg(DEBUG, msg);

    prm_getval (prm_get("refexp"),  &exper, &type);
    prm_getval (prm_get("refdiag"), &diagn, &type);
    prm_getval (prm_get("refedtn"), &edstr, &type);
    edition = atoi(edstr);
    prm_getval (prm_get("refshot"), &refshotstr, &type);

    if (strcmp(refshotstr,"same")==0)
      refshot = shot;
    else
      refshot = atoi(refshotstr);

    sprintf(msg, "Reference %s:%s %d(%d)", exper, diagn, refshot, edition);
    log_msg(INFO, msg);

    error = read_ece_settings(exper, diagn, &refshot, &edition, refmask, pRadSet);
    if (error)
    {
      sprintf(msg, "Failed to read reference item(s):%s%s%s%s%s%s%s%s%s%s", 
	    error & ECEREF_CALIBSRC ? " CALIBSRC" : "",
	    error & ECEREF_HARMONIC ? " HARMONIC" : "",
	    error & ECEREF_ZLENS    ? " ZLENS" : "",
	    error & ECEREF_WAVEGUID ? " WAVEGUID" : "",
	    error & ECEREF_RECEIVER ? " RECEIVER" : "",
	    error & ECEREF_ATTNIF   ? " ATTNIF" : "",
	    error & ECEREF_IFGROUP  ? " IFGROUP" : "",
	    error & ECEREF_FREQIF   ? " FREQIF" : "",
	    error & ECEREF_VGAIN    ? " VGAIN"  : "",
	    error & ECEREF_VBANDW   ? " VBANDW" : "" );
      log_msg(ERROR, msg);
    }
  }  /* if refmask */

  error = check_receivers (pRadSet->LO);
  /* don't quit on errors - just warn.
   new radctrl settings are checked before,
   and on old shot files there is nothing we can do */

  sprintf(msg, "Selected receivers: %s %s %s",
	  LOstring[pRadSet->LO[0]],
	  LOstring[pRadSet->LO[1]],
	  LOstring[pRadSet->LO[2]] );
  log_msg(INFO, msg);

  /* IF groups settings in libece/frequencies.c*/
  setup_if_parameters (pRadSet);

  for (i=0; i<MAX_IFGROUP; ++i)
    {
      enum LO_Connection lo = pRadSet->LO[i];
      sprintf(msg, 
        "IF %d: %s,loNr %d waveguide %2d f_LO=%3.0f %cSB Calchannel=%1d IF attn=%4.1f dB", 
	i+1, LOstring[lo],lo, pRadSet->waveguide[i], pRadSet->freq_LO[i]/1e9,
	pRadSet->sideband[i], pRadSet->calchannel[i]+1, pRadSet->attnif[i]);
      log_msg(DEBUG, msg);
    }

  /* frequencies for each channel */
  calc_channel_frequencies (pRadSet, &nbadchan);

  if (nbadchan)
    {
      sprintf(msg, 
	"No frequency settings for %d IF channels", nbadchan);
	log_msg(WARNING, msg);
    }

  /* abort if no calibration number defined */
  if (pRadSet->calibsrc == 0)
    {
      log_msg(WARNING, "No calibration number set (calibsrc=0) hence calibration factors cannot be determined");
      return(-2);
    }

  /* calibration file path */
  p = prm_get("calibpath");
  if (!(prm_getval(p, &valstr, &type)))
  {
    log_msg(ERROR, "No calibration path set (calibsrc)");
    return(-2);
  }

  /* build calibration file name */
  sprintf(fn, "%s/calrad.%d", valstr, pRadSet->calibsrc);

  sprintf(msg, "Calibration file: %s", fn);
  log_msg(DEBUG, msg);

  /* read calibration file and tabulate data */
  error = read_cal_file(fn, &cal);
  if (error != LIBECE_OK)
    {
      switch(error)
	{
	case LIBECE_CALIBRATION_NOT_FOUND:
	  sprintf(msg, "Calibration file %s not found", fn);
	  break;
	case LIBECE_CALIBRATION_UNEXPECTED_EOF:
	  sprintf(msg, "Unexpected end-of-file in %s", fn);
	  break;
	case LIBECE_CALIBRATION_WRONG_FORMAT:
	  sprintf(msg, "Unexpected end-of-file in %s", fn);
	  break;
	case LIBECE_CALIBRATION_CHANNELS_OVERFLOW:
	  sprintf(msg, "Too many calibration channels in %s (check MAX_CAL_CHAN in libece.h)", fn);
	  break;
	case LIBECE_CALIBRATION_INCONSISTENT_CHNO:
	  sprintf(msg, "Inconsistent channel number in %s", fn);
	  break;
	case LIBECE_CALIBRATION_TOO_FEW:
	  sprintf(msg, "Too few entries for a channel in calibration file %s", fn);
	  break;
	}
      log_msg(ERROR, msg);
      return(-2);
    }

  if (pRadSet->calibsrc != cal.version)
  {
    sprintf(msg, "Inconsistent calibration numbers: request=%d file=%d", 
	    pRadSet->calibsrc, cal.version);
    log_msg(ERROR, msg);
    return(-2);
  }

  /* calculate calibration factors for each channel */
  nbadchan = 0;
  if (calibration_factors(&cal, pRadSet, &nbadchan))
    return(-2);

  if (nbadchan)
  {
    sprintf(msg, "%d channels switched off for lack of calibration (ecerad).", nbadchan);
    log_msg(INFO, msg);
  }
  
  return(0);
}  /* parse_radiometer_parameters */

/* ............... command handlers ................ */ 

static int do_check_receivers (PRM_HANDLE p)
{
  int i, type;
  char *valstr;
  enum LO_Connection lo[MAX_IFGROUP];
  
  for (i=0; i<MAX_IFGROUP; ++i)
  {
    if (prm_getval(p, &valstr, &type))
    {
      if (strcmp(valstr, "bt")==0) 
	return(0);

      if (strcmp(valstr, "ref")==0) 
	{
	  int j;
	  for (j=0; j<MAX_IFGROUP; ++j) lo[j]= none;
	  return(0);   /* nothing to check now, read reference first */
	}
      else
	{
	  lo[i] = string_to_lo(valstr);
          
	}
    }
    else lo[i] = none;
  }
  return( check_receivers(lo) );
}

void set_harmonic (struct RadiometerSettings *pRadSet)  /*set_harmonic*/
{
   int i, type;
   char *valstr;
   PRM_HANDLE p;

 /* Harmonic number (one per IF group or mixer) LBarrera Sept 2012*/
   /* assignment of harmonic number to IF groups */
  p=prm_get("harmonic");
  for(i=0; i<MAX_IFGROUP && prm_getval (p,&valstr,&type); ++i)
  {
     pRadSet->harmonic[i] = atoi(valstr);	
  }

  for (; i<MAX_IFGROUP; ++i)   /* if less than MAX_IFGROUP bands given */
    pRadSet->harmonic[i] = 2;  /* Second harmonic by default*/
    
} /*set_harmonic*/

/* .................... initialisation ........................ */

#define GROUP1 "eceradiometer"
#define GROUP2 "refshotfile"
#define GROUP3 "calibration"

void init_radiometer     (void)
{
  prm_group  (GROUP1, "ECE radiometer settings");
  prm_define ("harmonic", "1,2,3,4,ref", "2, 2, 2",
              "\tECE harmonic number for any IF group (3 arguments); ref = use reference shot file",
              GROUP1, NULL);

  /* we define do this here but only in 'radctrl'.
     'CEC' works only with old shots hence radiometer settings are known.

  prm_define ("bt", "miracle,(float)", "miracle",
              "\tuse toroidal field to set up radiometer (miracle = query CODAC server)",
              GROUP1, NULL);
  */

  prm_define ("zlens", "ref,(float)", "ref",
              "\tlens position (cm), ref = obtain from reference shot file",
              GROUP1, NULL);
  // remove 91U on Dec2019 by mwillensdfg
  // prm_define_check 
  //          ("receivers", "none,ref,bt,101L,95U,133L,128N,101S,91L", "none", 
  //           "connect IF units 1-3 with mixers/sidebands (3 arguments) or ref shot or bt value (1 argument)",
  //           GROUP1, NULL, do_check_receivers);
  // temporarily for 17.12.
  prm_define_check 
            ("receivers", "none,ref,bt,101U,101L,95U,133L,128N,91L,105U", "none", 
             "connect IF units 1-3 with mixers/sidebands (3 arguments) or ref shot or bt value (1 argument)",
             GROUP1, NULL, do_check_receivers);
  prm_define ("radsettings", "ref,default", "default",
              "\tget freqif,attnif,bif,gains,bvd from reference or by default",
              GROUP1, NULL);
  
   
  prm_group  (GROUP2, "Reference ECE shot file");
  prm_define ("refdiag", "RAD,CEC,CED", "RAD", 
              "ECE reference shot file diagnostic",
              GROUP2, NULL);
  prm_define ("refexp", "AUGD,(string)", "AUGD", 
              "ECE reference shot file experiment code",
              GROUP2, NULL);
  prm_define ("refshot", "same,(integer)", "same", 
              "ECE reference shot number: explicitly given or same as currently worked on",
              GROUP2, NULL);
  prm_define ("refedtn", "(integer)", "0", 
              "ECE reference shot file edition",
              GROUP2, NULL);


  prm_group  (GROUP3, "Calibration parameters");
  prm_define ("calibsrc", "(integer),ref", "ref",
              "\t#of calibration to be used",
              GROUP3, NULL);
  prm_define ("calibpath", "(string)", CALIBRATION_PATH,
              "\tpath to calibration file",
              GROUP3, NULL);
  


}


/* end of eceradiometer.c */
