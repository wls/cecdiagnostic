/*Soubrutines to write parameter set in the shot file (common to RAD, RMC, CEC, RMD)*/
/*Modifications:
	Sept-2012  harmonic=array(N_IFGROUP) LBarrera 
*/

#include <malloc.h>
#include <memory.h>
//#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#include "param.h"
#include "libece.h"
#include "util.h"
#include "eceradiometer.h"
//#include "cec.h"

/*#ifdef DDWW8
  int32_t err;
#else
  int err;
  #endif*/
//char msg[128];




/* write "methods" parameter set */

int write_methods (int diaref, 
			  struct RadiometerSettings *pRadSet)
{
  int i, n;
  float zlens_cm, freq_LO_GHz[MAX_IFGROUP], Attnif[MAX_IFGROUP];
  int calibnum, eqedit, harm[MAX_IFGROUP];
  char msg[256];

  if (!pRadSet) return(0);

  n = MAX_IFGROUP;
  calibnum=pRadSet->calibsrc;
  eqedit=pRadSet->EQED;
  int error=0;
  if (sf_write_parm_int   (diaref, "METHODS", "CALIBSRC", 
			   1, &calibnum)) error=error+1;

  zlens_cm = pRadSet->zlens * 100;
  if (sf_write_parm_float (diaref, "METHODS", "ZLENS", 
			   1, &zlens_cm)) error=error+1;
  if (sf_write_parm_int   (diaref, "METHODS", "IFGROUPS", 
			   1, &n ))   error=error+1;
  if (sf_write_parm_char  (diaref, "METHODS", "SIDEBAND", 
			   n, pRadSet->sideband))   error=error+1;
  if (sf_write_parm_int   (diaref, "METHODS", "WAVEGUID", 
			   n, pRadSet->waveguide))  error=error+1;
  for(i=0; i<n; ++i)
    freq_LO_GHz[i] = pRadSet->freq_LO[i] *1e-9;
  if (sf_write_parm_float (diaref, "METHODS", "FREQLO", 
			   n, freq_LO_GHz)) error=error+1;

  n= MAX_IFGROUP;
  for(i=0; i<n; ++i)
   Attnif[i] = pRadSet->attnif[i];
  if (sf_write_parm_float (diaref, "METHODS", "ATTNIF", 
			   n, Attnif)) error=error+1;
  /*Write harmonic number used to calculate resonance position*/

  for(i=0; i<n; ++i)
    harm[i] = pRadSet->harmonic[i];	

	sprintf(msg, "Harmonic number: %u, %u, %u",
	  harm[0],harm[1],harm[2]);
  	log_msg(INFO, msg);


  if (sf_write_parm_int   (diaref, "METHODS", "HARMONIC", 
			   n, harm ))
  {
    sprintf(msg, "Error writing integer parameter METHODS/HARMONIC");
    log_msg(INFO, msg);
    error=error+1;
  }

 /*Write equilibrium name and edition from which R, Z were calculated. Lbarrera 05.2012*/
  int nl=3;
  if (sf_write_parm_char (diaref, "METHODS", "RZ_SRC", 
			   nl, pRadSet->RZ_SRC)) 
  {
    sprintf(msg, "Error writing char parameter METHODS/RZ_SRC");
    log_msg(INFO, msg);
    error=error+1;
  }
 nl=4;
  if (sf_write_parm_char (diaref, "METHODS", "EQEXP", 
			   nl, pRadSet->EQEXP)) 
 {
    sprintf(msg, "Error writing char parameter METHODS/EQEXP");
    log_msg(INFO, msg);
    error=error+1;
  }
 
 nl=1;
  if (sf_write_parm_int (diaref, "METHODS", "EQED", 
			   nl, &eqedit))
 {
    sprintf(msg, "Error writing integer parameter METHODS/EQED");
    log_msg(INFO, msg);
    error=error+1;
  }



  return(error);

} 


/* write "parms-A" parameter set */

int write_channel_settings (int diaref, 
			       struct RadiometerSettings *pRadSet)
{
  char msg[256];
  if (!pRadSet) return(0);
  int error=0;

  if (sf_write_parm_float  (diaref, "parms-A", "f", 
		            MAX_CHAN, pRadSet->freqrf)) error=error+1;
  if (sf_write_parm_float  (diaref, "parms-A", "df", 
		            MAX_CHAN, pRadSet->bif)) error=error+1;
  if (sf_write_parm_float  (diaref, "parms-A", "Btot", 
		            MAX_CHAN, pRadSet->Btot)) error=error+1;
  if (sf_write_parm_int    (diaref, "parms-A", "IFGROUP", 
		            MAX_CHAN, pRadSet->ifgroup)) error=error+1;
  
  if (sf_write_parm_int    (diaref, "parms-A", "AVAILABL", 
		            MAX_CHAN, pRadSet->have)) error=error+1;
  if (sf_write_parm_float  (diaref, "parms-A", "calfact", 
		            MAX_CHAN, pRadSet->calfact)) {
	sprintf(msg, "Failed to write calfact");
    	log_msg(ERROR, msg); 
	error=error+1;
  }
  /*New parameter: error including calibration error and SNR of the channel*/
  if (sf_write_parm_float  (diaref, "parms-A", "SNR_cali",                 
		            MAX_CHAN, pRadSet->SNR_calib)) {
 	sprintf(msg, "Failed to write SNR_cali");
    	log_msg(ERROR, msg); 
	error=error+1;
  }

 /* Modification: Not useful: not writing it, as mean value was obtain in all time base... 12.03.2013 */
 /* Change again for S. Rathgeber requirement 18.04.2013*/

 if (sf_write_parm_float  (diaref, "parms-A", "SNR",                 
		            MAX_CHAN, pRadSet->SNR)) 
  {
 	sprintf(msg, "Failed to write SNR");
    	log_msg(ERROR, msg); 
	error=error+1;
  }
  
	/*int ic;
	for(ic=0;ic<MAX_CHAN;ic++){
        sprintf(msg, "Channel %u SNR (total in methods) %f",ic+1,pRadSet->SNR[ic]);
	log_msg(INFO, msg);
	}*/
  
  return(error);
} 

/*write_channel_settings with new parameters written in RMD after shot*/
int write_channel_settings_rmd (int diaref, 
			       struct RadiometerSettings *pRadSet)
{
  char msg[256];
  if (!pRadSet) return(0);
  int error=0;

  if (sf_write_parm_float  (diaref, "parms-A", "f", 
		            MAX_CHAN, pRadSet->freqrf)) error=error+1;
  if (sf_write_parm_float  (diaref, "parms-A", "df", 
		            MAX_CHAN, pRadSet->bif)) error=error+1;
  if (sf_write_parm_float  (diaref, "parms-A", "Btot", 
		            MAX_CHAN, pRadSet->Btot)) error=error+1;
  if (sf_write_parm_int    (diaref, "parms-A", "IFGROUP", 
		            MAX_CHAN, pRadSet->ifgroup)) error=error+1;
  
  if (sf_write_parm_int    (diaref, "parms-A", "AVAILABL", 
		            MAX_CHAN, pRadSet->have)) error=error+1;
  /*New parameter: error including calibration error and SNR of the channel*/
  if (sf_write_parm_float  (diaref, "parms-A", "SNR_cali",                 
		            MAX_CHAN, pRadSet->SNR_calib)) {
 	sprintf(msg, "Failed to write SNR_cali");
    	log_msg(ERROR, msg); 
	error=error+1;
  }

  if (sf_write_parm_float  (diaref, "parms-A", "gainma2",                 
		            MAX_CHAN, pRadSet->gainma2)) {
 	sprintf(msg, "Failed to write gain");
    	log_msg(ERROR, msg); 
	error=error+1;
  }
  if (sf_write_parm_float  (diaref, "parms-A", "calfact", 
		            MAX_CHAN, pRadSet->calfact)) {
	sprintf(msg, "Failed to write calfact");
    	log_msg(ERROR, msg); 
	error=error+1;
  }
  if (sf_write_parm_float  (diaref, "parms-A", "shift", 
		            MAX_CHAN, pRadSet->offset)) {
		sprintf(msg, "Failed to write shift");
    	log_msg(ERROR, msg); 
	error=error+1;	
  }
  if (sf_write_parm_float  (diaref, "parms-A", "dTmin", 
		            MAX_CHAN, pRadSet->dTmin)) {
		sprintf(msg, "Failed to write dTmin");
    	log_msg(ERROR, msg); 
	error=error+1;	
  }
  
  return(error);
} 



int write_channel_positions (int diaref, 
				    struct RadiometerSettings *pRadSet,
				    struct ChannelPositions *pChanPos)
{
  int c;

  if (!pRadSet || !pChanPos) return(0);

  if (pChanPos->have_equilibrium_Rz)
  {
    if (sf_write_tbase_float (diaref, "rztime", 
		              pChanPos->ntimes, pChanPos->time)) return(-1);
 
    for (c=0; c<MAX_CHAN; ++c)
      if (pRadSet->have[c])
      {
        if (sf_insert_signal_float (diaref, "R-A", c,
		            pChanPos->ntimes, pChanPos->R[c])) return(-2);
        if (sf_insert_signal_float (diaref, "z-A", c,
		            pChanPos->ntimes, pChanPos->z[c])) return(-3);
      }
  }

  if (pChanPos->have_static_Rz)
  {
    if (sf_write_parm_float  (diaref, "parms-A", "Rs", 
		            MAX_CHAN, pChanPos->Rs)) return(-1);
    if (sf_write_parm_float  (diaref, "parms-A", "zs", 
		            MAX_CHAN, pChanPos->zs)) return(-1);
  }

  return(0);
} 


/* write "time-A", "Trad-A" signals */
int write_channel_data (int diaref, 
			       struct RadiometerSettings *pRadSet,
			       struct ChannelData *pChanData)
{
  int c;

  if (!pRadSet || !pChanData) return(0);

  if (sf_write_tbase_double (diaref, "time-A", 
		            pChanData->ntimes, pChanData->time)) return(-1);

  for (c=0; c<MAX_CHAN; ++c)	
    if (pRadSet->have[c])
    /*Note: If we write less channels than MAX_CHAN we will have a warning error:
	Error by DDBinsert (8.1): incorrect length/value of DIAG*/
    {
      if (sf_insert_signal_float (diaref, "Trad-A", c,
		            pChanData->ntimes, pChanData->Trad[c])) return(-2);
    }
  

  return(0);
} 


int write_sflist_equil(int diaref,int shot, struct RadiometerSettings *pRadSet)
{ 
  if (!pRadSet) return(0);
  //sprintf(msg, "Test %s",  pRadSet->EQEXP);
  //log_msg(INFO, msg);
  if( sf_insert_sf_list (diaref, "EQUIL", pRadSet->EQEXP, pRadSet->RZ_SRC, shot,pRadSet->EQED))return(-1);
  return(0);
}

