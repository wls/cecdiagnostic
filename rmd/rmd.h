/* cec.h definitions for CEC
   Wolfgang Suttrop, 25-Mar-2010 */


#ifndef _LIBECE_H_
#include "libece.h"
#endif


/* --------- function prototypes ----------- */

/* rawsignals_rmc.c */
struct RawDiagnostic *get_raw_diag_specs (int shot);

int    get_raw_signals_info_rmc (int shot, 
				 struct RawDiagnostic *pRawDiag,
				 struct RadiometerSettings *pRadSet);
int    read_signal_traces   (int shot, 
			     struct RawDiagnostic *pRawDiag,
			     struct RadiometerSettings *pRadSet,
			     struct ChannelData *pChanData,
			     struct SampleRateSettings *SRSet);
void   free_channel_data    (struct ChannelData *pChanData);
void   init_raw_signals	(void);



/* rmd_calibration.c */
void  init_calibration	      (void);
int   get_calibration_factors (int shot,
			   struct RadiometerSettings *rs,
			   struct ChannelData *cd);


/* main.c*/
void  init_write_rmd	(void);

/* rmdsh.c */
/* Functions in util/rmdsh.c (as RMD sfh is also writen when RMC is syncronized)*/

/* end of rmd.h */
