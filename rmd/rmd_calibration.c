/* from cec/calibration.c - ECE calibration for CEC 
 Modify to obtain calibration factors (S) and then calibrate RMD*/

#include <ctype.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "libece.h"

#include "param.h"
#include "util.h"
#include "eceradiometer.h"
#include "rmd.h"

#define GROUP "calibration"


#define BUFLEN 512
#define MAXFILELEN 256



/* ..... multiply in-place voltages with calibration factors .... */

static void  do_calibrate_rmd (struct RadiometerSettings *rs,
			   struct ChannelData *cd)
{
  int i, c;
  int nt = cd->ntimes;

  for (c=0; c<MAX_CHAN; ++c)
    if (rs->have[c])
    {
      float f = rs->calfact[c];
      for (i=0; i<nt; ++i) (cd->Trad[c])[i] *= f;
    }

}  /* do_calibrate */




/* ........ calibrate temperature  ........ */

int   get_calibration_factors (int shot,
			   struct RadiometerSettings *rs,
			   struct ChannelData *cd)
{
   
  PRM_HANDLE p;
  int stat, type, noff, ic;
  int nchoff;

  char *s, msg[BUFLEN], fn[MAXFILELEN], *valstr;
  int calibsrc = 0;
  struct caldata cal;

  /* determine calibration number to use */
  p = prm_get("calibsrc");
  if (prm_getval(p, &s, &type)) 
  {
    if (strcmp(s, "shotfile")==0)
    {
      //calibsrc = rs->calibsrc;
      sprintf(msg, "Calibration #%d as specified in raw file", calibsrc);
    }
    else
    {
      calibsrc = atoi(s);
      sprintf(msg, "Calibration #%d (overriding %d from shot file)", 
	      calibsrc, rs->calibsrc);
      rs->calibsrc=calibsrc;
    }
  }

  if (calibsrc==0)
  {
    log_msg(ERROR, "No temperature calibration defined - abort.");
    return(-1);
  }

  log_msg(INFO, msg);

  /* calibration file path */
  p = prm_get("calibpath");
  if (!(prm_getval(p, &s, &type)))
  {
    log_msg(ERROR, "No calibration path set (calibsrc)");
    return(-1);
  }

  /* build calibration file name */
  sprintf(fn, "%s/calrad.%d", s, calibsrc);

  sprintf(msg, "Calibration file: %s", fn);
  log_msg(DEBUG, msg);

  /* read calibration file and tabulate data */
  stat = read_cal_file(fn, &cal);
  if (stat != LIBECE_OK)
    {
      switch(stat)
	{
	case LIBECE_CALIBRATION_NOT_FOUND:
	  sprintf(msg, "Calibration file %s not found", fn);
	  break;
	case LIBECE_CALIBRATION_UNEXPECTED_EOF:
	  sprintf(msg, "Unexpected end-of-file in %s", fn);
	  break;
	case LIBECE_CALIBRATION_WRONG_FORMAT:
	  sprintf(msg, "Unexpected end-of-file in %s", fn);
	  break;
	case LIBECE_CALIBRATION_CHANNELS_OVERFLOW:
	  sprintf(msg, "Too many calibration channels in %s (check MAX_CAL_CHAN in libece.h)", fn);
	  break;
	case LIBECE_CALIBRATION_INCONSISTENT_CHNO:
	  sprintf(msg, "Inconsistent channel number in %s", fn);
	  break;
	case LIBECE_CALIBRATION_TOO_FEW:
	  sprintf(msg, "Too few entries for a channel in calibration file %s", fn);
	  break;
	}
      log_msg(ERROR, msg);
      return(-1);
    }

  if (calibsrc != cal.version)
  {
    sprintf(msg, "Inconsistent calibration numbers: request=%d file=%d", 
	    calibsrc, cal.version);
    log_msg(ERROR, msg);
    return(-1);
  }

  /* calculate calibration factors for each channel */
  if (calibration_factors_rmd(&cal, rs, &noff))
    return(-1);

  /*For shots gt 28430 and lt 28469 channels 
	1-12 off (power supply of 1st IF box wrongly connected)*/
  if((shot >=28430) && (shot <=28469))
	for (ic=0; ic < 12; ++ic){
	 rs->have[ic]=0;}

 /*Some channels off*/

  p=prm_get("chan_off");
  if (prm_getval (p,&valstr,&type))
  {

    if(strstr(valstr, "none")==0 || strstr(valstr, "NONE")==0) 
    {
	sprintf(msg, "Channel off: %u", atoi(valstr));
        log_msg(INFO, msg);

	nchoff=atoi(valstr);  //First read the number of channels off
 	for(ic=0; ic<nchoff && prm_getval (p,&valstr,&type); ++ic)
	  {
	    if ((atoi(valstr)-1 >= 0) && (atoi(valstr)-1 <=MAX_CHAN ))
	      {
		rs->have[atoi(valstr)-1] = 0;	
		sprintf(msg, "Channel off: %u", atoi(valstr));
		log_msg(INFO, msg);
	      }
	  }
     }

  }

  if (noff)
  {
    char buf[BUFLEN];
    sprintf(buf, "%d channels switched off for lack of calibration (rmd).", noff);
    log_msg(INFO, buf);    
  }

  /* can calibrate now */
  do_calibrate_rmd (rs, cd);

  return(0);
}   /* get_calibration_factors */





/* ........... initialise ........... */

void  init_calibration	(void)
{
  prm_group  (GROUP, "Calibration parameters");

  prm_define ("calibsrc", "(integer),shotfile", "shotfile",
              "\t#of calibration to be used",
              GROUP, NULL);
  prm_define ("calibpath", "(string)", CALIBRATION_PATH,
              "\tpath to calibration file",
              GROUP, NULL);
  prm_define ("chan_off", "none,(integer)", "none", 
              "selection of level-0 channels to be removed (number of channels off, channels off)",
              GROUP, NULL);
}


