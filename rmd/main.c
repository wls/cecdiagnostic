/*
       From main.c of RMD main program

       Wolfgang Suttrop,   suttrop@ipp.mpg.de

       23-Feb-1994  started as ece1.c
       25-Mar-2010  stripped, modernised
	Sept-2012  harmonic=array(N_IFGROUP) LBarrera 
	Okt 2012 Include function set_harmonic to correctly set the harmonic number to write CEC
	Feb 2013 Modification from CEC to RMD
	Mar 2013 Read gain settings from RMC sfh

	  

*/

#include <ctype.h>
#include <memory.h>
#include <setjmp.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#include "libece.h"

#include "param.h"
#include "util.h"
#include "eceradiometer.h" 
#include "rmd.h"


#define LOGSTRLEN	255      /* max. line length in log file */


static  void    process_shot(int shot); 
static  char   *defaultarg[2];



/* ==================== main program ===================== */

#ifdef FORMAIN

MAIN_ () {        /* This program works in a FORTRAN environment .. */

  int    argc;    /* .. hence command line arguments .. */
  char   **argv;  /* don't come as arguments */
  getargs ("RMD", &argc, &argv);  

#else

int main (int argc, char **argv)         /* .. no longer */
  {

#endif

  defaultarg[0] = "RMD";
  defaultarg[1] = strdup("cmd");

  init_logging       ();
  init_position      ();          //Change Okt-2012, get harmonic before procesing the shot...
  init_rmdsf	();
  init_write_rmd ();
  init_shots	     (process_shot);
  init_raw_signals   ();
  init_calibration   ();
  
  init_downsampling  ();


  if (argc>1)   prm_main (argc, argv);
  else          prm_main (2, defaultarg);

  return(0);
  }  /* main() */




/* ------  process a single shot ------- */


static void process_shot (int shot) 
{
  
  char  msg[LOGSTRLEN];
  int noff;
  
  struct RawDiagnostic *pRawDiag;
  struct RadiometerSettings RadSet;
  struct ChannelPositions ChanPos;
  struct ChannelData ChanData;
  struct SampleRateSettings SRSet;

  /* reset .. */
  set_radiometer_defaults (&RadSet);
  set_harmonic (&RadSet);

  memset(&ChanPos,  0, sizeof(ChanPos));
  memset(&ChanData, 0, sizeof(ChanData));
  memset(&SRSet,    0, sizeof(SRSet));

  /* .. greetings .. */
  log_timestamp();
  sprintf(msg, "Processing shot %d", shot);
  log_msg(INFO, msg);


  /* .. and go */
  /* Signal description: rawsignal.c*/

  if (!(pRawDiag = get_raw_diag_specs (shot))) 
    return;
  
  /*  read raw signal header information and information from ctrl word from RAD*/
  if (get_raw_signals_info_rmc(shot, pRawDiag, &RadSet))
    return;
  
 
  if ((shot==28136) || (shot==28137) || (shot==28381) || (shot==28773) || (shot==28775)) {
	int i;
	for (i=0; i<MAX_IFGROUP; ++i)   
    	RadSet.harmonic[i]= 3;
	} 
 
  /*Calculate frequencies*/
  calc_channel_frequencies (&RadSet, &noff);
  
  if (noff>0)
    {
      sprintf(msg, 
	    "%d channels disabled because they are not connected to a valid IF group", 
	      noff);
      log_msg(WARNING, msg);
    }


  /*Read signal traces, downconvert, extract infromation of the shift and calibrtion factors in RadSet*/
  if (read_signal_traces (shot, pRawDiag, &RadSet, 
			  &ChanData, &SRSet))
    goto cleanup22;

  /*Calculate positions*/
  if (calculate_positions (shot, &RadSet, &ChanPos, &ChanData))
    goto cleanup22;

 /* Obtain calibration factors --> set RadSet*/
 /* rmd/rmd_calibration.c*/
 /* In this soubrutine the gain is also read from */
  if(get_calibration_factors(shot,&RadSet,&ChanData))
	goto cleanup22;
	

 /* Obtain SNR Feb. 2013 after obtaining calibration factors!
  (calculate also dTmin)*/
 if(calculate_SNR_dT(shot,&RadSet, &ChanData)) 
	goto cleanup22;
 
 /*  Write RMD calibration factors and offset */
 /*  Write methods, parameters */
 /*  Write Trad-A calibrated*/ 

if (write_rmd(shot, &RadSet, &ChanPos,
	        &ChanData, &SRSet))
    goto cleanup22;

  log_timestamp();
  sprintf(msg, "Shot %d successfully processed", shot);
  log_msg(INFO, msg);

cleanup22:
  free_sr_settings  (&SRSet);
  free_channel_data (&ChanData);
  //cleanup11:
  free_positions (&ChanPos);
 
} /*process_shot*/


/* ........... initialise ........... */
#define GROUPw "writermd"

void  init_write_rmd	(void)
{
  prm_group  (GROUPw, "Level-1 shot file");

  prm_define ("writediag", "CEC,RMD", "RMD", 
              "\tlevel-1 diagnostic to write",
              GROUPw, NULL);
  /*defined in rmdsf rmd_exp:
	prm_define ("writeexp", "AUGD,(string)", "eced", 
              "\tlevel-1 experiment (repository) code",
              GROUPw, NULL);*/

}

/* end of main.c */
