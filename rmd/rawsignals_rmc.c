/* rawdata.c - raw (source) data for CEC:
   RAD, RMA, RMB shot files */
/* Modification to read RMC also Lbarrera Feb. 2013*/

#include <malloc.h>
#include <stdio.h>
#include <string.h>


#include "libece.h"
#include "param.h"
#include "util.h"
#include "eceradiometer.h"
//#include "cec.h"

#define GROUP "rawsignals"

#define LOGSTRLEN 256



/* ....... obtain signals descriptor ........ */

struct RawDiagnostic *get_raw_diag_specs (int shot)
{
  PRM_HANDLE p;
  char *rawexp, *rawdiag, msg[LOGSTRLEN];
  int type;

  struct RawDiagnostic *pRawDiag = NULL;

  p = prm_get("rawexp");
  if (!prm_getval(p, &rawexp, &type)) 
    rawexp="AUGD";

  p = prm_get("rawdiag");
  if (!prm_getval(p, &rawdiag, &type)) 
    {
      log_msg(WARNING, "No raw diagnostic (rawdiag) specified - abort."); 
      return(NULL);
    }

  if (strcmp(rawdiag, "RAD")==0)   /* RAD, always single */
    pRawDiag = rad_setup(shot);
  else 
    {   /* add RMA and RMB here */
    if (strcmp(rawdiag, "RMC")==0)   /* RMC */
     pRawDiag = rmc_setup(shot); 
    else{
       if (strcmp(rawdiag, "RMZ")==0)   /* RMC */
	 pRawDiag = rmz_setup(shot); 
    }
      
    }

  if (!pRawDiag)
  {
    sprintf(msg, "No signal specs for raw diag %s shot %d.", rawdiag, shot);
    log_msg(ERROR, msg);
  }

  return(pRawDiag);
}



/* ....... read method parameter set ........ */

static int read_methods(int diaref, 
			struct RawDiagnostic *pRawDiag,
			struct RadiometerSettings *pRadSet)
{
  int ifgroups, i;
#ifdef OBSOLETE
  char sbchar[MAX_IFGROUP];
#endif

  //if (sf_read_parm_int (diaref, "METHODS", "CALIBSRC", 
 //		        1, &(pRadSet->calibsrc))) return(-1);
  if (sf_read_parm_int (diaref, "METHODS", "IFGROUPS", 
		        1, &ifgroups)) return(-1);

  if (sf_read_parm_float (diaref, "METHODS", "ZLENS", 
		          1, &(pRadSet->zlens))) 
    /* pRadSet->zlens = 10.5;   fast dirty fix, really need to read MUM shot file 
			       and get position from calibrated potentiometer voltage */
    pRadSet->zlens = 7.5;   /* Change value 19 Feb.2013 */

  pRadSet->zlens /= 100;   /* cm -> m */

  if (ifgroups > MAX_IFGROUP)
  {
    char msg[128];
    sprintf(msg, "Length of parameter METHODS:IFGROUPS %d > %d.", 
	    ifgroups, MAX_IFGROUP);
    log_msg(ERROR, msg); 
    log_msg(DEBUG, "Adjust MAX_IFGROUP and recompile.");
    return(-2);
  }

  if (sf_read_parm_int (diaref, "METHODS", "WAVEGUID", 
		        ifgroups, &(pRadSet->waveguide[0]))) return(-1);

  if (sf_read_parm_float (diaref, "METHODS", "FREQLO", 
		          ifgroups, &(pRadSet->freq_LO[0]))) return(-1);

  for (i=0; i<ifgroups; ++i)
    pRadSet->freq_LO[i] *= 1e9;   /* GHz -> Hz*/

  if (sf_read_parm_float (diaref, "METHODS", "ATTNIF", 
		          ifgroups, &(pRadSet->attnif[0])))
    { 
      char msg[128];
      sprintf(msg, "Error in ATTNIF (rawsignals_rmc.c)"); 
	  log_msg(ERROR, msg);  
	  return(-1); 
    } 

  if (sf_read_parm_char (diaref, "METHODS", "SIDEBAND", 
		         ifgroups, &(pRadSet->sideband[0]))) return(-1);
  for (i=0; i<ifgroups; ++i)
  {
    switch (pRadSet->sideband[i])
    {
      case 'u': pRadSet->sideband[i]= 'U';
      case 'U': break;
      case 'n': pRadSet->sideband[i]= 'N';   //Nodth filter Feb. 2013
      case 'N': pRadSet->sideband[i]= 'N';   //
      case 'l': pRadSet->sideband[i]= 'L';
      case 'L': break;
      case '\000': pRadSet->sideband[i]= ' ';
      case ' ': break;
      default:
	{
	  char msg[128];
	  sprintf(msg, "IF group %d: Unknown sideband code", 
		  i+1 ); //, pRadSet->sideband[i]));
	  log_msg(WARNING, msg);  
	}
    }
  }



#ifdef OBSOLETE
  if (sf_read_parm_char (diaref, "METHODS", "SIDEBAND", 
		         ifgroups, &(sbchar[0]))) return(-1);
  for (i=0; i<ifgroups; ++i)
  {
    switch (sbchar[i])
    {
      case 'U': case 'u': pRadSet->sideband[i]= 1;
                        break;
      case 'L': case 'l': pRadSet->sideband[i]= -1;
                        break;
    }
  }
#endif

  return(0);
}  /* read_methods */



/* ....... read signal group parameter set ........ */
/* Different than for RAD diagnostic, parameter values not set in sfh
   Therefore, read radiometer default values instead*/

#ifdef OBSOLETE
static int read_sgroup_parms(int diaref, 
			struct RawSigGroup *sg,
			struct RadiometerSettings *rs)
{

  int sc = sg->startchno,
      nc = sg->nchans, i;

  PRM_HANDLE p;

  int type;
  char  *rawdiag;
    
  p = prm_get("rawdiag");
  if (!prm_getval(p, &rawdiag, &type)) 
    {
      log_msg(WARNING, "No raw diagnostic (rawdiag) specified - abort."); 
      return(0);
    }

  if (sf_read_parm_int (diaref, sg->device_name, "IFGROUP", 
		        nc, &(rs->ifgroup[sc]))) return(-1);
  if (sf_read_parm_float (diaref, sg->device_name, "FREQIF", 
		          nc, &(rs->freqif[sc]))) return(-1);
  if (sf_read_parm_float (diaref, sg->device_name, "BIF", 
		          nc, &(rs->bif[sc]))) return(-1);

  for (i=sc; i<sc+nc; ++i)
  {
    rs->freqif[i] *= 1e9;   /* GHz -> Hz*/
    rs->bif[i] *= 1e9;
  }


  
  if (strcmp(rawdiag, "RMC")==0) {
      if (sf_read_parm_float (diaref, sg->device_name, "GAINPA", 
		          nc, &(rs->gainpa[sc]))) return(-1);
      if (sf_read_parm_float (diaref, sg->device_name, "GAINMA1", 
			      nc, &(rs->gainma1[sc]))) return(-1);
      if (sf_read_parm_float (diaref, sg->device_name, "GAINMA2", 
			      nc, &(rs->gainma2[sc]))) return(-1);
      if (sf_read_parm_float (diaref, sg->device_name, "GAINLD", 
			    nc, &(rs->gainld[sc]))) return(-1);}
  
  if (sf_read_parm_float (diaref, sg->device_name, "BVD", 
		          nc, &(rs->bvd[sc]))) return(-1);

  for (i=sc; i<sc+nc; ++i) 
    rs->have[i] = 1;  /* so far */
  
  
  return(0);
}
#endif


/* ....... read raw signal header information ........ */

int    get_raw_signals_info_rmc (int shot, 
			     struct RawDiagnostic *pRawDiag,
			     struct RadiometerSettings *pRadSet)
{
  PRM_HANDLE p;
  char msg[LOGSTRLEN], *rawexp;
  int type, edition, diaref, have_switch_info=0;
  int diaref_rad,edition_rad;
  p = prm_get("rawexp");
  if (!prm_getval(p, &rawexp, &type)) rawexp="AUGD";


  /* loop through required raw diagnostics (RAD or RMA,RMB) */
  while (pRawDiag)
  { 
    sprintf(msg, "Obtaining info for diagnostic %s %s", rawexp, pRawDiag->name);
    log_msg(DEBUG, msg);

    edition = 0;

    if (sf_open_read(rawexp, pRawDiag->name, &shot, &edition, &diaref) == SF_ERROR)
      return(SF_ERROR);

    if (pRadSet && (pRawDiag->n_ifgroups) && (shot >= 28029))   /*Correction 2 Sept 2013 (only in sfh for shots>28029 are written METHODS parameters), else-->default values*/
    {
	sprintf(msg, "Reading methods for (%s) %s", rawexp, pRawDiag->name);
        log_msg(DEBUG, msg);
	if (read_methods(diaref, pRawDiag, pRadSet))
	  return(SF_ERROR);
    }

      /* loop through all (expected) signal groups) */
   
     //CHANNEL PARAMETERS (params-A) NOT SET IN RMC SFH UP TO SHOT 27621
    /*if (shot >=27622)
   {
    struct RawSigGroup *sg;
    sg = pRawDiag->sgroups;	
    while(sg)
     {
     // read actual channel parameters for this group 
      sprintf(msg, "Signal group %s (device %s) %d channels (%d-%d)", 
		sg->name, sg->device_name, sg->nchans, 
		sg->startchno+1, sg->startchno+sg->nchans);
      log_msg(DEBUG, msg);

      if (read_sgroup_parms(diaref, sg, pRadSet))
	  return(SF_ERROR); 
	
      
      sg=sg->next;
     }   
     }*/
  /*THEREFORE SET DEFAULT VALUES*/
   if (pRadSet) set_radiometer_defaults (pRadSet);

  /* Read actual gainma2 from RMC*/
   if (pRadSet && ((shot >=27622) && (shot <= 41575)) ) 
    {
      if(get_setgain(shot, diaref,pRadSet))return(-1);
    }  /*ELSE:*/
       /* set gainma2=1 for all channels. Optimization for gain was not available for shots <27622 */
       /* gainma2=1 is set in default values set_radiometer_defaults (pRadSet)*/
     
   sf_close_read(diaref);   //added from S. Denk to avoid memory leak 10.3.20   
  //If DIAG RMC but need RAD ctrl word to get switch information
   if  (!have_switch_info
     && pRawDiag->switch_info_source == sw_RAD
	&& ((strcmp(pRawDiag->name,"RMC")==0)||(strcmp(pRawDiag->name,"RMZ")==0)) )
     {
	//Open RAD 
	 edition_rad = 0;
	 // changed rawexp to "AUGD", since RAD is always in AUGD
    	 if (sf_open_read("AUGD", "RAD", &shot, &edition_rad, &diaref_rad) == SF_ERROR)
          return(SF_ERROR);
	//Get ctrlword from rad and get the settings for RMC diagnostic:  
	  if (rad_switchsettings (shot, diaref_rad, pRadSet))
          {
	   sf_close_read(diaref_rad);  /* error abort */
	   return(SF_ERROR);
          }
         have_switch_info = 1;
       
	 sf_close_read(diaref_rad);
      }
  
   
   pRawDiag = pRawDiag->next;
  } //endwhile (pRawDiag)
  return(0);
}  //end get_raw_signals_info


/* =============  read signal traces ================= */


/* ....... read timebase and set up downsampling ........ */

static int make_timebase (int shot, int diaref, char *name,
			  int *ntimes, float **rawbuf,
			  struct ChannelData *pChanData,
			  struct SampleRateSettings *SRSet)
{
  int    nrt;     /* length of raw time base */
  double *rtime;  /* raw time base */
  int    nt;      /* length of downsampled signal traces */

  /* check time base of this signal */
  if (sf_signal_ntpoints(diaref, name, &nrt))
  {
    char msg[256];
    sprintf(msg, "Cannot determine size of time base of %s.", name);
    log_msg(ERROR, msg); 
    return(-1);
  }

  /* have already a time base - check consistency */
  if (*ntimes)
  {
    if (nrt != *ntimes)
    {
      char msg[256];
      sprintf(msg, "Signal %s, time base length (%d) mismatch (previous %d).", 
	      name, nrt, *ntimes);
      log_msg(ERROR, msg); 
      return(-2);
    }

    return(0);   /* consistent with previous time base - done */
  }

  /* read new raw time base */
  rtime = (double *) calloc(nrt, sizeof(double));
  if (!rtime) 
  {
    char msg[256];
    sprintf(msg, "Cannot allocate memory for raw time base (%d doubles).", nrt);
    log_msg(ERROR, msg); 
    return(-3);
  }
  *ntimes = nrt;

  if (sf_read_tbase_double (diaref, name, nrt, rtime))
  {
    char msg[256];
    sprintf(msg, "Cannot read time base of signal %s.", name);
    log_msg(ERROR, msg); 
    free(rtime);
    return(-4);
  }

  /* allocate raw data buffer for downsampled signals */
  *rawbuf = (float *) calloc(nrt, sizeof(float));
  if (*rawbuf==NULL) 
  {
    char msg[256];
    sprintf(msg, "Cannot allocate raw signals buffer (%d floats).", nrt);
    log_msg(ERROR, msg); 
    free(rtime);
    return(-5);
  }

  /* calculate downsampled trace length */
  nt = set_reduction (SRSet, shot, rtime, nrt);
  if (nt<=0) 
  {
    free(*rawbuf);
    free(rtime);
    return(-6);
  }

  /* create time base for downsampled signals */
  pChanData->time = (double *) calloc(nt, sizeof(double));
  if (!pChanData->time) 
  {
    char msg[256];
    sprintf(msg, "Cannot allocate memory for downsampled time base (%d doubles).", nt);
    log_msg(ERROR, msg); 
    free(*rawbuf);
    free(rtime);
    return(-7);
  }
  pChanData->ntimes = nt;

  reduce_timebase(SRSet, rtime, pChanData->time);

  free(rtime);
  return(0);
}  /* make_timebase */




/* ....... read signal (one of a group) and store downsampled version ........ */

static int make_signal(int diaref, int ntimes, float *rawbuf, int c, 
		       struct RawSigGroup *sg, 
		       struct ChannelData *pChanData,
		       struct SampleRateSettings *SRSet)
{
  float *buf;
  

  /* read raw (full) signal into 'global' buffer */
   
  if (sf_extract_csignal_float (diaref, sg->name, c, ntimes, rawbuf))
  {
    return(-1);
  }
  
  /* allocate space for downsampled signal */
  buf = (float *) calloc(pChanData->ntimes, sizeof(float));
  if (!buf) 
  {
    char msg[256];
    sprintf(msg, "Cannot allocate memory for downsampled signal (%d floats).", 
	    pChanData->ntimes);
    log_msg(ERROR, msg); 
    return(-2);
  }

  /* downsampled signal */
  
  reduce_signal (SRSet, rawbuf, buf);  
  pChanData->Trad[sg->startchno + c] = buf;
  
  return(0);  
}   /* make_signal */





/* ....... external entry: read raw data ........ */


int    read_signal_traces   (int shot, 
			     struct RawDiagnostic *pRawDiag,
			     struct RadiometerSettings *pRadSet,
			     struct ChannelData *pChanData,
			     struct SampleRateSettings *SRSet)
{
  PRM_HANDLE p;
  char msg[LOGSTRLEN], *rawexp;
  int  type, edition, diaref, nrawtimes=0, c;
  float *rawbuf = NULL;    /*survives reading all channels to save time*/

  p = prm_get("rawexp");
  if (!prm_getval(p, &rawexp, &type)) rawexp="AUGD";


  /* loop through required raw diagnostics (RAD or RMA,RMB) */
  while (pRawDiag)
  { 
    sprintf(msg, "Reading data from diagnostic %s:%s %d", 
	    rawexp, pRawDiag->name, shot);
    log_msg(DEBUG, msg);

    edition = 0;

    if (sf_open_read(rawexp, pRawDiag->name, &shot, &edition, &diaref)!=SF_ERROR)
    {
      /* loop through all (expected) signal groups) */
      struct RawSigGroup *sg;
      sg = pRawDiag->sgroups;
      while(sg)
      {
	/* if this is first signal group (i.e. nrawtimes==0), then create time base, 
	   otherwise check consistency with previously read time base */
	if (make_timebase(shot, diaref, sg->name, &nrawtimes, &rawbuf,
			  pChanData, SRSet))
	{
	  sf_close_read(diaref);
	  return(-1);
	}

	sprintf(msg, "Signal group %s (device %s) %d channels (%d-%d)", 
		sg->name, sg->device_name, sg->nchans, 
		sg->startchno+1, sg->startchno+sg->nchans);
	log_msg(DEBUG, msg);


	/* read and downsample signals */
	for (c=0; c < sg->nchans; ++c)
	{
	  //sprintf(msg, "make signal for channel %d ...", c+1);
	  //log_msg(DEBUG, msg);

	  if (make_signal(diaref, nrawtimes, rawbuf, c, sg, pChanData, SRSet))
	  {
	    
	    free(rawbuf);
	    sf_close_read(diaref);
	    return(-2);
	  }
	  

	 
	//get offset and sign (inverted signal or not) and write it in pRadSet->offset LBarrera 12.03.2013:
	//in downsampling.c
	int chan=sg->startchno + c;
	get_offset_raw ( rawbuf, chan,
			SRSet, pRadSet);

	}

	sg=sg->next;
      }

      sf_close_read(diaref);

    }  /* if diaref */
    pRawDiag = pRawDiag->next;
  }

  if (rawbuf) free(rawbuf);
  return(0);
}  /* read_signal_traces */




/* clean up channel data structure */

void   free_channel_data    (struct ChannelData *pChanData)
{
  int i;
  if (!pChanData) return;
  if (pChanData->time) free(pChanData->time);
  for (i=0; i<MAX_CHAN; ++i)
    if ((pChanData->Trad)[i]) free((pChanData->Trad)[i]);
}




/* ........... initialise ........... */

void  init_raw_signals	(void)
{
  prm_group  (GROUP, "Signal source parameters");

  prm_define ("rawdiag", "RAD,RMA,RMB,RMC,RMZ", "RAD", 
              "\tlevel-0 diagnostic to process",
              GROUP, NULL);

  prm_define ("rawexp", "AUGD,(string)", "AUGD", 
              "\tlevel-0 experiment (repository) code",
              GROUP, NULL);

  prm_define ("channels", "all,off,on,-,(integer)", "all", 
              "selection of level-0 channels to process",
              GROUP, NULL);

  prm_define ("mixersoff", "(void),95,101,128,133,167", "", 
              "LO frequencies (GHz) of mixers to disable",
              GROUP, NULL);
}


