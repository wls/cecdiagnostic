/* eceradiometer.h 
   common definitions for ECE radiometer-specific data structures */


/* prototypes */
/*eceradiometer.c*/
void set_radiometer_defaults     (struct RadiometerSettings *pRadSet);
int  parse_radiometer_parameters (int shot, struct RadiometerSettings *pRadSet);
void init_radiometer             (void);
void set_harmonic (struct RadiometerSettings *pRadSet) ;
/* end of eceradiometer.h */
