/* util.h 
   various utility subroutines for AUG level-1 analysis 
   7.03.2013  (LBarrera) Change algorithm to find out if the signal is inverted or not (prvious one fails for RMD)*/
  

#ifndef _LIBECE_H_
#include "libece.h"
#endif

#define CALIBRATION_PATH "/shares/departments/AUG/users/eced/cal"


/* message types for log_msg and sf_status */
enum msg_type {ERROR, WARNING, INFO, DEBUG};


/* downsampl.c */
struct SampleRateSettings
{
  struct timeseg {
    struct timeseg *next;	/* linear list */
    double t_sample;	       	/* raw data sampling interval */
    int	   red,			/* reduction factor t_sample/t_s */
           istop;		/* last index in time base */

  } *tseglist;

  int	inull;		/* index time base index for t=OFFSET_TMAX */
  int	iaver_sig;	/* index time base index for t=OFFSET_TSIG */

  char info[256];       /*Information requested for the downsampling*/
};


void  free_sr_settings (struct SampleRateSettings *srs);
int   set_reduction   (struct SampleRateSettings *srs, 
		       int shot, double *time, int ntime);
void  print_tseglist  (struct SampleRateSettings *srs, 
		       double *time);
void  reduce_timebase (struct SampleRateSettings *srs, 
		       double *time, double *rtime);
void  reduce_signal   (struct SampleRateSettings *srs, 
		       float *sig, float *rsig);  
void  init_downsampling (void);
void  get_offset_raw ( float *sig, int c,
			struct SampleRateSettings *srs, 
			struct RadiometerSettings *pRadSet);

/* logging.c */
void  log_msg         (enum msg_type mt, char *msg);
void  log_timestamp   (void);
void  init_logging    (void);

/* rmdsf.c */
int   write_rmd_calib     (int shot, 
		       struct RadiometerSettings *rs, 
		       float shift[MAX_CHAN]);
int   write_rmd (int shot, 
			 struct RadiometerSettings *pRadSet,
			 struct ChannelPositions *pChanPos,
			 struct ChannelData *pChanData,
			 struct SampleRateSettings *SRSet);


int   update_rmd_sfh  (struct RadiometerSettings *rs, 
		       float shift[MAX_CHAN]);
void  init_rmdsf      (void);


/* write_param_rad.c */
int write_channel_settings (int diaref, 
			    struct RadiometerSettings *pRadSet);
int write_channel_settings_rmd (int diaref, 
			    struct RadiometerSettings *pRadSet);
int write_channel_positions (int diaref, 
			     struct RadiometerSettings *pRadSet,
			     struct ChannelPositions *pChanPos);
int write_channel_data (int diaref, 
			struct RadiometerSettings *pRadSet,
			struct ChannelData *pChanData);
int write_sflist_equil(int diaref, int shot,
		       struct RadiometerSettings *pRadSet);


/* shots.c */
void  init_shots      (void (*shot_proc)(int shot));



/*-----------------------Common soubrutines for CEC and RMD------------------------*/




/* --------- function prototypes ----------- */
/* rawsignals.c */
void   init_raw_signals     (void);
struct RawDiagnostic *get_raw_diag_specs (int shot);
int    get_raw_signals_info (int shot, 
			     struct RawDiagnostic *pRawDiag,
			     struct RadiometerSettings *pRadSet);
int    read_signal_traces   (int shot, 
			     struct RawDiagnostic *pRawDiag,
			     struct RadiometerSettings *pRadSet,
			     struct ChannelData *pChanData,
			     struct SampleRateSettings *SRSet);
void   free_channel_data    (struct ChannelData *pChanData);

/* rad_patches.c */
struct RawDiagnostic   *rad_setup(int shot);
struct RawDiagnostic   *rmc_setup(int shot);
struct RawDiagnostic   *rmz_setup(int shot);

void   rad_patches     (int shot, 
		        struct RadiometerSettings *rs);
int rad_switchsettings (int shot, int diaref, 
			struct RadiometerSettings *rs);

/* position.c */
void  init_position	  (void);
int   calculate_positions (int shot,
			   struct RadiometerSettings *rs,
			   struct ChannelPositions *cp,
			   struct ChannelData *pChanData);
void  free_positions      (struct ChannelPositions *cp);



/* end of util.h */

