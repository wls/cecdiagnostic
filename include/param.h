/*	param.h - Header-Datei fuer param.c
	13.09.93  Wolfgang Suttrop
	26-Mar-2010 modernised  W.S.
*/

/* sizes: */

#define	VALSIZE	40
#define BUFSIZE 128
#define PATHSIZE 64

/* data type codes: */

#define VOID    0
#define LITERAL 1
#define STRING  2
#define INTEGER 3
#define FLOAT   4

/* type definition: */

typedef void *PRM_HANDLE;   /* a truly opaque data type */


/* prototypes: */
void	prm_define	 (char *name, char *types, 
			  char *defaults, char *help,
			  char *group, void (*action)());
void	prm_define_check (char *name, char *types, 
			 char *defaults, char *help,
			  char *group, void (*action)(), int (*check)());
void	prm_group	 (char *name, char *help);
int	prm_set		 (char *name, char *value);
int	prm_getval	 (PRM_HANDLE p, char **value, int *type); 
PRM_HANDLE	prm_get	 (char *name); 
void	prm_main	 (int argc, char **argv);
int     prm_runscript    (char *name, PRM_HANDLE args);

