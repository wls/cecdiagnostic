/* libece.h 
   headers for libece - ECE subroutine library 
   Modifications: Include SNR for channels
		   harmonic number=array, one value for any receiver or IF group
	*/


#define _LIBECE_H_   1


/* physical constants */
#define ELECTRON_MASS              9.10938215e-31   /*kg*/
#define ELEMENTARY_CHARGE          1.602176487e-19  /*As*/
#define VAC_SPEED_OF_LIGHT         2.99792458e8    /*m/s*/
#define C2 (VAC_SPEED_OF_LIGHT*VAC_SPEED_OF_LIGHT)

#define BIT2VOLT 5./4096.   /*(1/V)*/
#define R_BTF 1.65 /*m*/

/* linked lists for expected raw diagnostic file structure, 
   therein: signal groups and individual signals */

struct RawDiagnostic 
{
  struct RawDiagnostic *next;
  char *name;
  int  n_ifgroups;
  enum switchinfosrc {sw_NONE, sw_RAD} switch_info_source;
  struct RawSigGroup *sgroups;
};

struct RawSigGroup 
{
  struct RawSigGroup *next;
  char *name;
  char *device_name;     /* name of related device (parameters) */
  int nchans;            /* for channels 0..nchans-1: */
  int startindx;   /* - starting index in shot file signal group (1..) */
  int startchno;   /* - starting hardware channel number (0..) */
};



/* actual radiometer settings */
#define MAX_IFGROUP 3    /* max #IF groups (ever in radiometer history) */
#define MAX_CHAN 60      /* max. #radiometer channels (ever in history) */
#define MAX_LO_CONNECTIONS 14  /* #(receivers+sideband) combinations available */

/*enum  LO_Connection {none, LO101L, LO101U, LO95U, 
		     LO133L, LO133U, LO128U, LO128N, LO167L, LO167U};Before Sept-2012*/
/*After Sept-2012, change wg 6-7 Mixer 101 USB and LSB, after */
enum  LO_Connection {none, LO101U, LO101L, LO95U, 
		     LO133L, LO133U, LO128U, LO128N, LO167L, LO167U, LO101S, LO91L, LO91U, LO105U}; 

struct RadiometerSettings
{
  /* "permanent" radiometer settings (unless hardware changed), 
     settings which have a default value */  
  float attnif   [MAX_IFGROUP];   /* variable IF attenuator setting (dB) */
  float freqif   [MAX_CHAN],   /* IF filter centre frequency (Hz) */
        bif      [MAX_CHAN],   /* IF filter bandwidth (Hz) */
        gainpa   [MAX_CHAN];   /* video pre-amplifier gain (factor) */

  /* if radiometer is/was set using a Bt value this is stored here: */
  float Bt;

  /* "primary" settings (independently chosen) */
  int   harmonic[MAX_IFGROUP];     /* harmonic number (for B field calculation) */
  int   calibsrc;     /* number (identifier) of calibration */
  float zlens;        /* lens position (m) */
  enum  LO_Connection LO[MAX_IFGROUP];   /* receiver, sideband for each IF */
  char *RZ_SRC;		     /* Equilibrium reconstruction used to calibration (used to write methods param.)*/
  char *EQEXP;		     /* Equilibrium reconstruction used to calibration*/
  int EQED;		     /* Edition of the equilibrium to calculate R,Z*/


  int   have   [MAX_CHAN];   /* 1: data available, 0: not */
  int   ifgroup[MAX_CHAN];   /* IF group channel is connected with */
  float gainma1[MAX_CHAN],   /* video main amp 1 gain */
        gainma2[MAX_CHAN],   /* video main amp 2 gain */
        gainld [MAX_CHAN],   /* video line driver gain */
        bvd    [MAX_CHAN];   /* video bandwidth (Hz) */

  /* "secondary" settings (calculated from the above */
  int   waveguide[MAX_IFGROUP];   /* 0, 4, 9, 10, 11 */
  float freq_LO  [MAX_IFGROUP];   /* local oscillator frequency (Hz) */
  char  sideband [MAX_IFGROUP];   /* ' ': n.c., 'U': upper, 'L': lower*/
  int   calchannel[MAX_IFGROUP];   /* 0: no notch filter, 1: with notch filter */

  float S[MAX_CHAN],         /* U_diode / intensity */
        dS[MAX_CHAN],        /* SNR of the above signal */
        freqrf [MAX_CHAN],   /* RF centre frequency of channel (Hz) */
        Btot   [MAX_CHAN],   /* cold resonance total magnetic field (T) */
        calfact[MAX_CHAN];   /* calibration factor (eV)/(Volt) */
   
  float SNR_calib[MAX_CHAN]; /*SNR of the calibration factor calfact.*/
  float SNR_channel[MAX_CHAN]; /*SNR of the channel.*/
  float SNR[MAX_CHAN];       /* Total SNR for the calculated Te of the channel (SQRT(1/SNR_calib+1/SNR_channel))*/
  float offset[MAX_CHAN];       /* Average Voltage calculated the first 0.1s*/
  int sign[MAX_CHAN];		/*+1 if the signal is not inverted. -1 if the signal is inverted*/

  float dTmin[MAX_CHAN];		/*bit2Volt*calfact/gainma2 (eV)(minimum delta Temperature resolved, digital effect)*/
 
  char sampl_rate[256]; //information of downsampling used to write calibrated data
 
};   /* struct RadiometerSettings */



/* shot file status */
#define SF_OK       0
#define SF_DEBUG   -1
#define SF_INFO    -2
#define SF_WARNING -3
#define SF_ERROR   -4

/* calibration error status */
#define LIBECE_OK   0
#define LIBECE_CALIBRATION_NOT_FOUND -1
#define LIBECE_CALIBRATION_UNEXPECTED_EOF -2
#define LIBECE_CALIBRATION_WRONG_FORMAT -3
#define LIBECE_CALIBRATION_CHANNELS_OVERFLOW -4
#define LIBECE_CALIBRATION_INCONSISTENT_CHNO -5
#define LIBECE_CALIBRATION_TOO_FEW -6


/* mask values to control which items to read from reference source */
/* global; from METHODS parameter set */
#define ECEREF_CALIBSRC  0x0001
#define ECEREF_HARMONIC  0x0002
#define ECEREF_ZLENS     0x0004

/* per IF group; from METHODS */
#define ECEREF_WAVEGUID  0x0010
#define ECEREF_RECEIVER  0x0020
#define ECEREF_ATTNIF    0x0040

/* per channel; from device / parms-A */
#define ECEREF_IFGROUP   0x0100
#define ECEREF_FREQIF    0x0200   /* and BIF */
#define ECEREF_VGAIN     0x0400   /* video gain: GAINPA, GAINMA1, GAINMA2 */
#define ECEREF_VBANDW    0x0800   /* video bandwidth */

/* error codes */
#define ECEREF_WRONG_PARS  0x10000   /* wrong parameters */
#define ECEREF_CTRL_ERRFLG 0x20000   /* RAD ctrl word error flag */
#define ECEREF_CTRL_SWPOS  0x40000   /* unkown switch position */



/* bcomponents.c */
int bcomponents     (int shot, float time, char *exper, char *diag, int *edition,
		     int nlos, float *Rlos, float *zlos,
		     float *Bt, float *BR, float *Bz,
		     float *pflux, float *Jp);

int get_bvac  (int shot, char *exper, char *diag, int *edition, int neqtime, float *eqtime,float *Bvac);


/* btot_los.c */
/* line-of-sight data */
#define N_LOS 200
#define R0 1.65   /* (m) geometrical centre */
struct LineOfSight
{
  int   n;   /* #pts on line of sight */
  float R[N_LOS], z[N_LOS], Btot[N_LOS];
};

void  btot_los      (int shot, 
		     int nlos, float *Rlos, float *zlos,
		     float *Bt, float *BR, float *Bz, float Bvac, float *Btot);
void  bt_ripplecorr (int nlos, float *Rlos, float *zlos,
		    float corrfact, float *Bt, float *Btcorr, float Bext0);
float bt_corrfact   (int shot);



/* calibration.c */
#define MAX_CAL_CHAN  2     /* max. number of diagnostic channels */
#define MAX_PAR  500     /* max. points in cal. spectrum per channel */

struct caldata 
{
  int  version,  /* calibration version */
       entries[MAX_CAL_CHAN];  /* #calibration entries */

  double f   [MAX_CAL_CHAN][MAX_PAR];   /* frequency  */
  float  s   [MAX_CAL_CHAN][MAX_PAR],   /* calibration factor */
         ds  [MAX_CAL_CHAN][MAX_PAR];   /* uncertainty */
};

int read_cal_file (char *fn, struct caldata *cal);
int calibration_factors (struct caldata *cal,
			 struct RadiometerSettings *rs,
			 int *noff);
int calibration_factors_rmd (struct caldata *cal,
			 struct RadiometerSettings *rs,
			 int *noff);

/* ecesettings.c */
void  set_radiometer_defaults   (struct RadiometerSettings *pRadSet);
int   read_ece_settings         (char *experiment, char *diagname, 
			         int *shot, int *edition,
			         int refmask  /* combinations of ECEREF_* */,
			         struct RadiometerSettings *pRadSet);
int get_setgain(int shot, int diaref, struct RadiometerSettings *rs); 
/*set default gains for RMD gainma2=1 : (2 Sept 2013)*/
int set_defaultgain(int shot, int diaref,struct RadiometerSettings *rs);


/* frequencies.c */
void setup_if_parameters        (struct RadiometerSettings *rs);
void calc_channel_frequencies   (struct RadiometerSettings *rs, 
				 int *noff);

/* findindex.c */
int   floorindex      (double *data, double val, int len);
int   ceilindex       (double *data, double val, int len);
int   closestindex    (double *data, double val, int len);
void  findindex       (double *data, double val, int leng, 
		       int *floori, int *ceili, int *closesti);
void  findindexf      (float *data,  float val, int leng, 
		       int *floori, int *ceili, int *closesti);

/* geo_los.c */
void  geo_los   (int shot, int nowg, float z_lens /*[m]*/, 
	         int nlos, float *Rlos, float *zlos);

/* linint.c */
int   linintf         (int n, float *x, float *y,
		       int m, float *r, float *s);

/* radsf.c */
int   radsf_switchsettings (int shot, int diaref, 
			    struct RadiometerSettings *rs);
struct RawDiagnostic       *radsf_setup(int shot);


/* sfread.c */
int   sf_status            (int error, char *msg1);
int   sf_open_read         (char *experiment, char *diagname, 
			    int *shot, int *edition, int *diaref);
void  sf_close_read        (int diaref);
int   sf_read_parm_int     (int diaref, char *psetname, char *name, 
		            int n, int *values);
int   sf_read_parm_float   (int diaref, char *psetname, char *name, 
		            int n, float *values);
int   sf_read_parm_char    (int diaref, char *psetname, char *name, 
		            int n, char *values);
int   sf_read_signal_int   (int diaref, char *name, 
			    int n, int *values);
int   sf_read_signal_float (int diaref, char *name, 
			    int n, float *values);
int   sf_extract_csignal_float (int diaref, char *name, 
				int c, int n, float *values);
int   sf_read_tbase_double (int diaref, char *name, 
			    int n, double *time);
int   sf_read_tbase_float (int diaref, char *name, 
			    int n, float *time);
int   sf_signal_ntpoints   (int diaref, char *name, int *length);


/* sfwrite.c */
int   sf_open_write          (char *experiment, char *diagname, 
			      int *shot, int *edition, int *diaref);
int   sf_close_write         (int diaref);
int   sf_write_parm_int      (int diaref, char *psetname, char *name, 
		              int n, int *values);
int   sf_write_parm_float    (int diaref, char *psetname, char *name, 
		              int n, float *values);
int   sf_write_parm_char     (int diaref, char *psetname, char *name, 
		              int n, char *values);
int   sf_write_tbase_float   (int diaref, char *name, 
			      int n, float *time);
int   sf_write_tbase_double  (int diaref, char *name, 
			      int n, double *time);
int   sf_insert_signal_float (int diaref, char *name, 
			      int c, int n, float *values);
int   sf_insert_sf_list (int diaref, char *name, 
			      char *experiment, char *diagname, int shot,int ed);

/* channel position data */
struct ChannelPositions
{
  int have_equilibrium_Rz;
  char *eqexp, *eqdiag;    /* equilibrium shot file used */
  char *Bvacexp, *Bvacdiag;    /* Bvac shot file used */
  int  eqshot, eqedition;
  int  Bvacedition;
  int   ntimes;            /* number of time points, if 0 then (R,z) are static values */
  float *time;             /* time base array */
  float *Bvac;             /* Bvac array */
  float *R[MAX_CHAN];      /* array [MAX_CHAN] of pointers to time traces [ntimes]  */
  float *z[MAX_CHAN];

  int have_static_Rz;
  float Rs[MAX_CHAN];
  float zs[MAX_CHAN];
};

/* channel (ECE) data */
struct ChannelData
{
  int   ntimes;            /* number of time points, if 0 then (R,z) are static values */
  double *time;            /* time base array */
  float *Trad[MAX_CHAN];   /* array [MAX_CHAN] of pointers to time traces [ntimes]  */
};

/*util/write_param_rad.c*/
int write_methods (int diaref, 
			  struct RadiometerSettings *pRadSet);
int write_channel_settings (int diaref, 
			       struct RadiometerSettings *pRadSet);
int write_channel_positions (int diaref, 
				    struct RadiometerSettings *pRadSet,
				    struct ChannelPositions *pChanPos);
int write_channel_data (int diaref, 
			       struct RadiometerSettings *pRadSet,
			       struct ChannelData *pChanData);
int write_sflist_equil(int diaref,int shot, struct RadiometerSettings *pRadSet);

/*radctrl/write_paramset.c*/
int write_channel_settings_sfh (int diaref, 
			       struct RadiometerSettings *pRadSet);
int write_methods_sfh (int diaref, 
			  struct RadiometerSettings *pRadSet);





/*SNR.c*/
int SNR_channel (struct RadiometerSettings *pRadSet,
		struct ChannelData *pChanData);
int SNR_calib (int shot, struct RadiometerSettings *pRadSet,
		struct ChannelData *pChanData);
int calculate_SNR (int shot, struct RadiometerSettings *pRadSet,
		struct ChannelData *pChanData);
int calculate_SNR_dT (int shot, struct RadiometerSettings *pRadSet,
		struct ChannelData *pChanData);
int calculate_dT(struct RadiometerSettings *pRadSet);

/*writecec.c*/
//int write_sflist_equil(int diaref, int shot, struct RadiometerSettings *pRadSet);

/* end of libece.h */
