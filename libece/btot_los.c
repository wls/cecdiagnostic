/* btot_los.c - total magnetic field along an ECE sightline

   input:  sightline cooridnates Rlos[nlos], zlos[nlos]
           magnetic field components Bt[nlos], BR[nlos], Bz[nlos]
   output: Btot[nlos]

   (including ripple correction and a correction 
   for systematic error of magnetic field measured
   as TF coil current (Rogowski coil) )


   W.S. 21-Jun-2010    from previous code
   L.Barrera Wed. 16 Jan.2013 change calibration factor (1.5 % error)
   M. Willensdorfer 17.4 use Bvac from MBI and changed correction factor, no correction factor needed
*/


#include <math.h>
#include "libece.h"
#include <stdio.h>

/* ------------------------------------------------------
   ripple correction for sightlines near mid-plane
   (works as well if Btcorr in place with Bt) 
   In addition, vacuum Bt is corrected by factor corrfact 
   (can be set tp 1.0 if this is not wanted) 
   ------------------------------------------------------ */


/* FORTRAN entry */

void bt_ripplecorr_ (int *nlos, float *Rlos, float *zlos,
		     float *corrfact, float *Bt, float *Btcorr, float Bext0)
{
  bt_ripplecorr (*nlos, Rlos, zlos,
		 *corrfact, Bt, Btcorr, Bext0);

}  /* bt_ripplecorr_ */



/* C entry */

void bt_ripplecorr (int nlos, float *Rlos, float *zlos,
		    float corrfact, float *Bt, float *Btcorr, float Bext0)
{
  int l;
  float R_0, B0;

  if (Rlos[0]<Rlos[nlos-1])    /* reference always on HFS */
    { R_0 = Rlos[0]; B0 = Bt[0]; }
  else
    { R_0 = Rlos[nlos-1]; B0 = Bt[nlos-1]; }

  for (l=0; l<nlos; ++l)
  {
    float ripple, Bvac, Bdia,Bvac_ext;

#ifdef HAVE_EXPF_SQRTF
      ripple = 1.00 
        - 0.01 * expf (21.2 * (sqrtf(Rlos[l]) - sqrtf(2.2)));
#else
      ripple = 1.00 
        - 0.01 * (float) exp (21.2 * ((float) sqrt(Rlos[l]) - (float) sqrt(2.2)));
#endif
    Bvac = B0 * R_0 / Rlos[l];             /* 1/R vacuum field  */
    Bvac_ext = Bext0 * R_BTF / Rlos[l];             /* 1/R vacuum field  */
    Bdia = Bt[l] - Bvac;                  /* diamagnetic field */
    //   printf("%d %f %f,%f   %f \n",l,Rlos[l],Bvac_ext,Bvac,(Bvac-Bvac_ext)/Bvac*100.);
    Btcorr[l] = (Bvac_ext*corrfact*ripple) + Bdia;     /* corrected toroidal field */
  }
}  /* bt_ripplecorr() */




/* --------------------------------------------------
   return appropriate correction factor for vacuum TF
   --------------------------------------------------
 */

/* FORTRAN entry */

void bt_corrfact_ (int *shot, float *corrfact)
{
  *corrfact = bt_corrfact(*shot);
}


/* C entry */

float bt_corrfact(int shot)
{
  if (shot<=8645)  return(1.0);     /* no correction for divertor I */
  else if (shot < 9219) return(0.98);
              /* MAI BTF factor 0.4260 but MAI BTF factor 0.4495 used in FPP */
  else if (shot < 10392) return (0.98);
              /* MAI BTF factor 0.4495 */
  else if (shot < 28524) return (1.0/0.99); // 1% error until Jun 2012. 
  else if (shot < 30161) return(1.015); //Changed Wed. 16 Jan.2013 (1.5 % error). Bt calibration pulse made 30th Nov.2012
  else if ((shot >= 31776) || (shot == 30839)) return (1.005);
  //apply no calibration factor for all the pulses of this campaign from Jan 2014
  else return(1.0); 

} /* bt_corrfact() */




/* ------------------------------------------
   total magnetic field on a sight-line, with
   all corrections applied 
   ------------------------------------------ */

/* FORTRAN entry */

void btot_los_  (int *shot, 
		 int *nlos, float *Rlos, float *zlos,
		 float *Bt, float *BR, float *Bz, float Bvac, 
		 float *Btot)
{
  btot_los (*shot, *nlos, Rlos, zlos, Bt, BR, Bz, Bvac, Btot);
}

/* C entry */

void btot_los    (int shot, 
		 int nlos, float *Rlos, float *zlos,
		 float *Bt, float *BR, float *Bz, float Bvac, 
		 float *Btot)
{
  int l;
  float corrfact = bt_corrfact(shot);


  /* in-place TF corrections */
  bt_ripplecorr (nlos, Rlos, zlos, corrfact, Bt, Bt, Bvac);

  for (l=0; l<nlos; ++l)
  {
#ifdef HAVE_EXPF_SQRTF
    Btot[l] = sqrtf( BR[l] * BR[l] + Bz[l] * Bz[l] + Bt[l] * Bt[l] );
#else
    Btot[l] = (float) sqrt( BR[l] * BR[l] + Bz[l] * Bz[l] + Bt[l] * Bt[l] );
#endif
  }

}  /* field_los */


/* end of btot_los.c */

