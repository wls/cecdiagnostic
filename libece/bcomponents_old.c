/* bcomponents.c

   read components of B (Bt, BR, Bz) 

   from equilibrium (using libkk's kkrzbrzt())
*/

#include <string.h>

#include "libece.h"

#include "libkk.h"
#include "param.h"

/* FORTRAN entry */

void bcomponents_ (int *status,
		   int *shot, float *time, char *exper, char *diag, int *edition,
		   int *nlos, float *Rlos, float *zlos,
		   float *Bt, float *BR, float *Bz,
		   float *pflux, float *Jp, 
		   int expstrlen, int diastrlen)
{
  char expstr[20], diastr[20];
  int stat;

  strncpy(expstr, exper, expstrlen);
  expstr[expstrlen] = '\0';

  strncpy(diastr, diag, diastrlen);
  diastr[diastrlen] = '\0';

  stat = bcomponents (*shot, *time, expstr, diastr, edition,
			 *nlos, Rlos, zlos, Bt, BR, Bz, pflux, Jp);

  if (status) *status = stat;
} /* bcomponents_ */




/* C entry */

int bcomponents (int shot, float time, char *exper, char *diag, int *edition,
		 int nlos, float *Rlos, float *zlos,
		 float *Bt, float *BR, float *Bz,
		 float *pflux, float *Jp)
{
  int iERR;

  kkrzbrzt_ (&iERR, exper, diag, &shot, edition,
             &time, Rlos, zlos, &nlos,
	     BR, Bz, Bt, pflux, Jp,
             strlen(exper), strlen(diag));

  if (iERR) 
    return(SF_ERROR);
  else 
    return(SF_OK);

}  /* bcomponents() */



/* end of bcomponents.c */
