/* geo_los.c

   construct a geometrical sightline 
   for a given wave guide
   through middle of the ECE lens

   W.S. 16-Jun-2010
*/

#include "libece.h"


/* radial range covered */
#define R_MAX 2.35
#define R_MIN 1.00

/* data for original ECE lens setup (before installation of ECE-I) */

/* #define R_LENS      3.860
   #define R_WGUIDE    5.663   */
#define R_LENS      3.993   /* radial position of the lens [m] */
#define R_WGUIDE    5.796   /* radial position of antennas [m] */

static float z_wguide [12] = {0.044, 0.044, 0.075, 0.075,
			      0.075, 0.044, 0.075, 0.044,
			      0.106, 0.106, 0.106, 0.106};

/* FORTRAN entry */
void  geo_los_ (int *shot, int *nowg, float *z_lens /*[m]*/, 
	        int *nlos, float *Rlos, float *zlos)

{
  geo_los (*shot, *nowg, *z_lens, *nlos,  Rlos, zlos);
}  /* geo_los_ */


/* C entry */

void  geo_los (int shot, int nowg, float z_lens /*[m]*/, 
	       int nlos, float *Rlos, float *zlos)
{
  int    i;
  float  R, dR, m, b;

  R  = R_MAX;                               /* outer radius */
  dR = (R_MIN-R_MAX) / (float) (nlos - 1);  /* step width */


  /* old lens setup (before installing ECE imaging apparatus) */
  if (shot <= 24202) 
  {
    m  = (z_wguide[nowg-1] - z_lens) / (R_WGUIDE - R_LENS);
    b  = z_lens - m*R_LENS;
    for (i=0; i<nlos; ++i) 
    {
      if (zlos) zlos[i] = m*R + b;
      if (Rlos) Rlos[i] = R;
      R += dR;
    }
  }

  /* new lens setup (with ECE imaging apparatus) 
     formulas provided by Ivo Classen */
  else 
  {
    for (i=0; i<nlos; ++i) 
    {
      float R1 = R-0.133; /* Correction 20.06.2012 (lbarrera) */
      if (zlos)
        switch(nowg)
        {
        case 9:  case 10: case 11: case 12:  /*top antenna row*/
          zlos[i] = (z_lens-0.075)*1.4 - 0.01534 - (z_lens-0.075)*R1*0.372 + R1*0.0254;
	  break;
        case 3:  case 4:  case 5:  case 7:  /*center antenna row*/
          zlos[i] = (z_lens-0.075)*1.4 + 0.075 - (z_lens-0.075)*R1*0.372;
	  break;
        case 1: case 2: case 6: case 8:    /*bottom antenna row*/
          zlos[i] = (z_lens-0.075)*1.4 + 0.16534 - (z_lens-0.075)*R1*0.372 - R1*0.0254;
	  break;
        }
      if (Rlos) Rlos[i] = R;
      R += dR;
    }
  }

}  /*geo_los*/

/* end of geo_los.c */
