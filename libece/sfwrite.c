/* sfread.c 
   read AUG shot file data types
   29-Mar-2010 W.S. */

#include <stdlib.h>
#include <memory.h>
#include <stdio.h>
#include <string.h>

#ifdef DDWW8
#include "ddwwansic8.h"
#else
#include "ddwwansic.h"
#endif

#include "libece.h"



/* ................ open / close shot file ............. */


/* open (create) a shot file (first or new edition) for writing.
   file specified by: experiment, diagname, shot

   Returns "diaref" (file handle) or 0 if unsuccessful.
   On exit, 
     shot containes actual shot number
     edition contains actual edition number
*/

int  sf_open_write (char *experiment, char *diagname, 
		    int *shot, int *edition, int *diaref)
{
  char timedate[19], *mode = "new";
  /* if (strcmp(experiment, "AUGD")) mode = "noed"; */

  if (!diagname) return(SF_ERROR);
  if (!experiment) experiment = "AUGD";

#ifdef DDWW8
  int32_t  error = 0, edtn = -1, i_diaref = 0;
  uint32_t u_shot = 0;
  if (shot) u_shot = (uint32_t) *shot;

  wwopen_ (&error, experiment, diagname, &u_shot, mode, &edtn, 
	   &i_diaref, timedate,
	   strlen(experiment), strlen(diagname), strlen(mode), 18);

  if (edition) *edition = edtn;
  if (diaref)  *diaref = i_diaref;
#else
  int error = 0, edtn = -1;
  wwopen_ (&error, experiment, diagname, shot, mode, &edtn, 
	   diaref, timedate,
	   strlen(experiment), strlen(diagname), strlen(mode), 18);
#endif


  if (error) 
  {
    char msg2[128];
    sprintf(msg2, "[wwopen %s:%s %d (%d), error= %d]", 
	    experiment, diagname, *shot, *edition,error);
    return(sf_status(error, msg2));
  }

  if (edition) *edition = edtn;

  return(SF_OK);
} /*sf_open_write*/


/* close a shot file after writing */
int  sf_close_write (int diaref)     //Change LBarrera: return value 18 Apr.
{
#ifdef DDWW8
  int32_t  error = 0, i_diaref = 0;           //18.Apr.13
  if (diaref) i_diaref = (uint32_t) diaref;
#else
 int error=0, i_diaref=0;
 if (diaref) i_diaref = diaref;
#endif
  
  char *disp = "lock", 
      *space= "maxspace";
    // *space= "compress";  //Change L.Barrera 18.06.2012 suggested by Annedore Buhler due to a bug in the library. 21.06.2012 solved

  wwclose_ (&error, &i_diaref, disp, space, strlen(disp), strlen(space));
  if (error) 
    {
    char msg1[128];
    sprintf(msg1, "[wwclose, error= %d]", 
	    error);
    return(sf_status(error, msg1));
   }
 return(SF_OK);

} /*sf_close_write*/


/* .............. write parameter sets ................. */

/* write integer parameter */
int   sf_write_parm_int    (int diaref, char *psetname, char *name, 
		            int n, int *values)
{
 
#ifdef DDWW8
  int32_t error=0;
  uint32_t inttype = 1, u_n = n, stride = 1;
#else
   int   error = 0;
  int inttype = 1, u_n = n, stride = 1;
#endif

  wwparm_ (&error, &diaref, psetname, name, &inttype, &u_n,
           values, &stride, strlen(psetname), strlen(name) );

  if (error) 
  {
    char msg[256];
    sprintf(msg, "[wwparm (float) %s %s]", psetname, name);
    return(sf_status(error, msg));
  }
  return(SF_OK);

} /* sf_write_parm_int */

/* write floating point parameter */
int   sf_write_parm_float  (int diaref, char *psetname, char *name, 
		            int n, float *values)
{
  
#ifdef DDWW8
  int32_t error=0;
  uint32_t floattype = 2, u_n = n, stride = 1;
#else
  int   error = 0;
  int floattype = 2, u_n = n, stride = 1;
#endif

  wwparm_ (&error, &diaref, psetname, name, &floattype, &u_n,
           values, &stride, strlen(psetname), strlen(name) );

  if (error) 
  {
    char msg[256];
    sprintf(msg, "[wwparm (float) %s %s]", psetname, name);
    return(sf_status(error, msg));
  }
  return(SF_OK);

} /* sf_write_parm_float */

/* write character array parameter */
int   sf_write_parm_char  (int diaref, char *psetname, char *name, 
		           int n, char *values)
{
  
#ifdef DDWW8
  int32_t error=0;
  uint32_t chartype = 6, u_n = n, stride = 1;
#else
  int   error = 0;
  int chartype = 6, u_n = n, stride = 1;
#endif
//WWPARM (error, diaref, name, parm, type, lbuf, buffer, stride)
 wwparm_ (&error, &diaref, psetname, name, &chartype, &u_n,
           values, &stride, strlen(psetname), strlen(name) );

  if (error) 
  {
    char msg[256];
    sprintf(msg, "[wwparm (char) %s %s]", psetname, name);
    return(sf_status(error, msg));
  }
  return(SF_OK);

} /* sf_write_parm_char */


/* ................ write time bases ..................... */

/* write time base (single precision) */
int   sf_write_tbase_float   (int diaref, char *name, 
			      int n, float *time)
{
  
#ifdef DDWW8
  int32_t error=0;
  uint32_t floattype = 2, u_n = n, stride = 1;
#else
  int   error=0;
  int floattype = 2, u_n = n, stride = 1;
#endif

  wwtbase_ (&error, &diaref, name, &floattype, &u_n, time, &stride, 
	    strlen(name) );
  if (error) 
  {
    char msg[256];
    sprintf(msg, "[wwtbase (float) %s]", name);
    return(sf_status(error, msg));
  }
  return(SF_OK);

} /* sf_write_tbase_float */

/* .................. write signals ...................... */

/* write time base (double precision) */
int   sf_write_tbase_double  (int diaref, char *name, 
			      int n, double *time)
{
  
#ifdef DDWW8
  int32_t error=0;
  uint32_t doubletype = 3, u_n = n, stride = 1;
#else
  int   error=0;
  int doubletype = 3, u_n = n, stride = 1;
#endif

  wwtbase_ (&error, &diaref, name, &doubletype, &u_n, time, &stride, 
	    strlen(name) );
  if (error) 
  {
    char msg[256];
    sprintf(msg, "[wwtbase (double) %s]", name);
    return(sf_status(error, msg));
  }
  return(SF_OK);

} /* sf_write_tbase_double */


/* insert a signal of a signel group */
int   sf_insert_signal_float (int diaref, char *name, 
			      int c, int n, float *values)
{
  
#ifdef DDWW8
  int32_t error=0;
  uint32_t floattype = 2, u_n = n, stride = 1, indices[3] = {1,1,1};
#else
  int error=0;
  int floattype = 2, u_n = n, stride = 1, indices[3] = {1,1,1};
#endif

  indices[0] = c+1;
  wwinsert_  (&error, &diaref, name, &floattype, &u_n, values, 
	      &stride, indices, strlen(name));
  if (error) 
  {
    char msg[256];
    sprintf(msg, "[wwinsert (float) %s %d]", name, indices[0]);
    return(sf_status(error, msg));
  }
  return(SF_OK);

} /* sf_insert_signal_float */

/* write shot_list signal (for the equilibrium used) */

int   sf_insert_sf_list (int diaref, char *name, char *experiment,
			      char *diagname,int shot, int ed)
{
  
#ifdef DDWW8
  int32_t error=0;
  uint32_t pshot;
  uint64_t lexpert=4, ldiag=3;
#else
  int error=0;
  int pshot;
#endif
  
  pshot = shot;  
  wwpred_(&error, &diaref, experiment, diagname, &pshot, &ed,lexpert,ldiag);
  
  if (error) 
  {
    char msg[256];
    sprintf(msg, "[wwpred %s]", name);
    return(sf_status(error, msg));
  }
  return(SF_OK);

} /* sf_insert_sf_list */

