/* radsf.c - RAD shot file-specific subroutines 
   20-Jul-2010   wls   from older code
 */

#include <stdio.h>
#include <string.h>

#include "libece.h"



/* -------------------------
   RAD signals from shot ...
   ------------------------- */

static struct RawSigGroup SI_MI_D = 
{
  NULL, "SI-MI-D", "D-RAD-D", 15, 1, 45
};

static struct RawSigGroup SI_MI_C = 
{
  &SI_MI_D, "SI-MI-C", "D-RAD-C", 15, 1, 30
};

static struct RawSigGroup SI_MI_B = 
{
  &SI_MI_C, "SI-MI-B", "D-RAD-B", 15, 1, 15
};

static struct RawSigGroup SI_MI_A = 
{
  &SI_MI_B, "SI-MI-A", "D-RAD-A", 15, 1, 0
};

static struct RawDiagnostic RAD_1 =
{
  NULL, "RAD", 3, sw_RAD, &SI_MI_A
};



/* ---------------------------------
   return RAD setup for a given shot 
   --------------------------------- */

struct RawDiagnostic *radsf_setup(int shot)
{
  return(&RAD_1);
}



/* --------------------------------
   shot file per-channel parameters
   -------------------------------- */



/* ------------------------------
   subroutines for mixer settings 
   ------------------------------ */

static void set_lo_95 (int shot, int ctrl, int nif,
		       struct RadiometerSettings *rs)
{
  --nif;
  rs->LO[nif] = LO95U;
  rs->freq_LO[nif] = 95.0e9;      /* Hz */
  rs->waveguide[nif]=10;          /* always on 10 */
  rs->sideband[nif]= 'U';         /* always USB */
  if ((ctrl & 0x0004 ) == 0) {
      rs->LO[nif] = none;
      rs->sideband [nif] = ' ';
      rs->waveguide[nif] = 0;
    }  
}       /* set_lo_95 */



static void set_lo_101(int shot, int ctrl, int nif,
		       struct RadiometerSettings *rs)
{
  --nif;
  rs->freq_LO[nif] = 101.0e9; /* Hz */
 /* 101L was out of order in this week */
  if (shot>=36906 && shot<=37001){
	rs->LO[nif] = none;
	rs->sideband[nif] = ' ';
	rs->waveguide[nif] = 0;
	return;
  }

  // First old configuration
  if (shot < 33733){ 
 /* 1-JUL-1997 RAD was set to 101U, 101U, 133L  */
  
    if (ctrl & 0x0080 && shot!= 9001) {  /* monomode wg switch set to LSB */     
      rs->LO[nif] = LO101L;
      rs->sideband [nif] = 'L';
      if (shot < 7823) 
	rs->waveguide[nif] = 10;
      else
	if (ctrl & 0x40) 
	  rs->waveguide[nif] =  9;
	else 
	  {
	    rs->LO[nif] = none;
	    rs->sideband [nif] = ' ';
	    rs->waveguide[nif] = 0;
	  }
    }

    else {                /* USB */
      rs->LO[nif] = LO101U;
      rs->sideband [nif] = 'U';  
      if (shot < 7823) 
	{
	  if ((ctrl & 0x0002) == 0) 
	    {
	      rs->LO[nif] = none;
	      rs->sideband[nif] = ' ';
	      rs->waveguide[nif] = 0;
	    }
	  else
	    rs->waveguide[nif] = 12;
	}
      else 
	{
	  if ((ctrl & 0x0060) != 0x0020) 
	    {
	      rs->LO[nif] = none;
	      rs->sideband [nif] = ' ';
	      rs->waveguide[nif] = 0;
	    }
	  else
	    rs->waveguide[nif] = 9;
	}
    }

    // New configuration
  } else if (shot>33733 && shot<=36954) {

    // only lower side bands
    rs->LO[nif] = LO101L;
    rs->sideband [nif] = 'L'; 
    rs->waveguide[nif] = 12;

    // in the case switch one was closed
    if ((ctrl & 0x0001) == 0) 
      {
	rs->LO[nif] = none;
	rs->sideband[nif] = ' ';
	rs->waveguide[nif] = 0;
      }
    
  } else if (shot>36954)  {
        if (ctrl & 0x0040)   /* oversized wg switch 6 (101LSB) on */     
	{
	  rs->sideband [nif] = 'L';
	  rs->waveguide[nif] = 11;
	  rs->LO[nif] = LO101L;
	}
      else
	{
	  rs->sideband [nif] = 'U';  
	  rs->LO[nif] = LO101U;
	}

      return; 

  }
  

}       /* set_lo_101 */



static void set_lo_128(int shot, int ctrl, int nif,
		       struct RadiometerSettings *rs)
{
  --nif;
  rs->freq_LO[nif] = 128.0e9;       /* Hz */
  rs->sideband[nif]= 'U';           /* always USB */



  if (shot < 33733){ 
    // Old configuration
    if (shot < 8724) rs->waveguide[nif]=10; 
    else             rs->waveguide[nif]=12;

    if ((ctrl & 0x0001) == 0) 
      {
	rs->LO[nif] = LO128N;
	rs->sideband[nif]= 'N';           /* wg switch say notch filter */
	rs->calchannel[nif] = 1;          /* use notch filter calibration */
      }
    else
      {
	rs->LO[nif] = LO128U;
	rs->sideband[nif]= 'U';           /* USB, oherwise */
	rs->calchannel[nif] = 0;
      }
  } else {
    // new configuration
    rs->LO[nif] = LO128N;
    rs->sideband[nif]= 'N';           /* wg switch say notch filter */
    rs->calchannel[nif] = 0;  /* use notch filter calibration */
    rs->waveguide[nif] = 3;
    // switch 5
    if ((ctrl & 0x0010) == 0) {
      rs->LO[nif] = none;
      rs->sideband[nif] = ' ';
      rs->waveguide[nif] = 0;
      rs->calchannel[nif] = 0;   
    }
  }


}       /* set_lo_128 */



static void set_lo_133(int shot, int ctrl, int nif,
		       struct RadiometerSettings *rs)
{
  --nif;
  rs->freq_LO[nif] = 133.0e9; /* Hz */
  if (shot < 7823) 
  {   /* oversized wg sw determines sideband */
    rs->waveguide[nif] = 9;
    if      (ctrl & 0x0040) 
      {
	rs->LO[nif] = LO133L;
	rs->sideband[nif] = 'L';
      }
    else if (ctrl & 0x0020) 
      {
	rs->LO[nif] = LO133U;
	rs->sideband[nif] = 'U';
      }
    else  
      {
	rs->LO[nif] = none;
	rs->sideband[nif] = ' ';
	rs->waveguide[nif] = 0;
      }
  }
  else 
  {                 /* LSB only after #7823 */
    rs->LO[nif] = LO133L;
    rs->sideband[nif]= 'L'; 
    if (shot<8724) 
      rs->waveguide[nif]=10; /* no automatic waveguide switch */ 
    else 
    {       
      if ((ctrl & 0x0002) == 0) 
	{
	  rs->LO[nif] = none;
	  rs->sideband[nif]= ' '; 
	  rs->waveguide[nif]=0;
	}
      else
	rs->waveguide[nif]=4;
    }
  }
}       /* set_lo_133 */



static void set_lo_167(int shot, int ctrl, int nif,
		       struct RadiometerSettings *rs)
{
  --nif;
  rs->freq_LO[nif] = 167.0e9; /* GHz */
  rs->waveguide[nif] = 9;

  /*monomode switch is manually operated,
    oversized wg switches determine sideband */
  if      ((ctrl & 0x0070)==0x0010) 
    {
      rs->LO[nif] = LO167L;
      rs->sideband[nif] = 'L';
    }
  else if ((ctrl & 0x0078)==0x0008) 
    {
      rs->LO[nif] = LO167U;
      rs->sideband[nif] = 'U';
    }
  else 
    {
      rs->LO[nif] = none;
      rs->sideband[nif] = ' ';
      rs->waveguide[nif] = 0;
    }
}       /* set_lo_167 */


/* new mixer on 22.11.21 */
static void set_lo_105(int shot, int ctrl, int nif,
		       struct RadiometerSettings *rs)
{
    --nif;
    rs->freq_LO[nif] = 104.403e9;   /* Hz */
    if ((ctrl & 0x0068)==0x0008) 
    {
        rs->LO[nif] = LO105U;
        rs->waveguide[nif]=11;          /* always on 11 */
        rs->sideband[nif]= 'U';         /* always USB */
        rs->calchannel[nif] = 0;
    }       /* new mixer on 12.6.13 */
    else
    {
        rs->LO[nif] = none;
        rs->sideband[nif] = ' ';
        rs->waveguide[nif] = 0;
        rs->calchannel[nif] = 0;
    }

}




/* new mixer on 12.6.13 */
static void set_lo_101S (int shot, int ctrl, int nif,
		       struct RadiometerSettings *rs)
{
    --nif;
    rs->freq_LO[nif] = 100.92e9;   /* Hz */
    if ((ctrl & 0x0068)==0x0008) 
    {
        rs->LO[nif] = LO101S;
        rs->waveguide[nif]=11;          /* always on 9 */
        rs->sideband[nif]= 'S';         /* always USB */
        rs->calchannel[nif] = 0;
    }       /* new mixer on 12.6.13 */
    else
    {
        rs->LO[nif] = none;
        rs->sideband[nif] = ' ';
        rs->waveguide[nif] = 0;
        rs->calchannel[nif] = 0;
    }

    if (shot>36826){
      rs->LO[nif] = none;
      rs->sideband [nif] = ' ';
      rs->waveguide[nif] = 0;
    }
}


/* new mixer on 20.3.17 from #33733 */
static void set_lo_91 (int shot, int ctrl, int nif,
		       struct RadiometerSettings *rs)
{
    --nif;
    rs->freq_LO[nif] = 91.00e9;   /* Hz */
    if (shot < 36799){
      if ((ctrl & 0x0080)==0)
	{
      /* switch 7 on 0 and 6 on  */
	  if ((ctrl & 0x0060) == 0x0020) 
	    {
	      rs->LO[nif] = LO91U;
	      rs->waveguide[nif]=11;          /* always on 111 */
	      rs->sideband[nif]= 'U';         /* always USB */
	      rs->calchannel[nif] = 0;
	    }       /* new mixer on 12.6.13 */
	  else
	    {
	      rs->LO[nif] = none;
	      rs->sideband[nif] = ' ';
	      rs->waveguide[nif] = 0;
	      rs->calchannel[nif] = 0;
	    }
	} else {
	if ((ctrl & 0x0078)==0x0008) 
	  {
	    rs->LO[nif] = LO91L;
	    rs->waveguide[nif]=11;          /* always on 9 */
	    rs->sideband[nif]= 'L';         /* always USB */
	    rs->calchannel[nif] = 0;
	  }       /* new mixer on 12.6.13 */
	else
	  {
	    rs->LO[nif] = none;
	    rs->sideband[nif] = ' ';
	    rs->waveguide[nif] = 0;
	    rs->calchannel[nif] = 0;
	  }

	// rs->LO[nif] = LO91L;
	//   rs->waveguide[nif]=11;          /* always on 9 */
	//    rs->sideband[nif]= 'L'; 
      }
    } else if (shot>=33724 && shot<=36954) {

	if ((ctrl & 0x0078)==0x0008) 
	  {
	    rs->LO[nif] = LO91L;
	    rs->waveguide[nif]=11;          /* always on 9 */
	    rs->sideband[nif]= 'L';         /* always USB */
	    rs->calchannel[nif] = 0;
	  }       /* new mixer on 20.3.17 from #33733  */
	else
	  {
	    rs->LO[nif] = none;
	    rs->sideband[nif] = ' ';
	    rs->waveguide[nif] = 0;
	    rs->calchannel[nif] = 0;
	  }

    } else  if (shot>=36954){
	if ((ctrl & 0x0001)== 1) 
	  {
	    rs->LO[nif] = LO91L;
	    rs->waveguide[nif]=12;          /* always on 9 */
	    rs->sideband[nif]= 'L';         /* always USB */
	    rs->calchannel[nif] = 0;
	  }       /* new mixer on 12.6.13 */
	else
	  {
	    rs->LO[nif] = none;
	    rs->sideband[nif] = ' ';
	    rs->waveguide[nif] = 0;
	    rs->calchannel[nif] = 0;
	  }
    }
}






/* ----------------------------------------------
   read and evaluate radiometer control word 
   from RAD shotfile
   (coded mixer and IF assignments)
   ---------------------------------------------- */

int radsf_switchsettings (int shot, int diaref, struct RadiometerSettings *rs)
{

  /* read control word (1 word signal in RAD shot file */
  int ctrl, i;

  int error = sf_read_signal_int (diaref, "SI-CTRL", 1, &ctrl);
  if (error) return(error);

  for (i=0; i<MAX_IFGROUP; ++i)
    {
      rs->LO[i] = none;
      rs->waveguide[i] = none;
      rs->sideband[i] = none;
    }

  if ((ctrl & 0x8000)==0) 
    return(ECEREF_CTRL_ERRFLG);

  /* SP4T  coax switch position. */
  switch (ctrl & 0x6000) 
  {
    case 0x2000:
      if (shot < 8724) 
	error |= ECEREF_CTRL_SWPOS;
      else
        set_lo_101(shot, ctrl, 2, rs);
      break;

    case 0x4000:
      if (shot < 7823)  {set_lo_101(shot, ctrl, 1, rs); break;}
      if (shot < 8724)  {set_lo_167(shot, ctrl, 1, rs); break;}
                        {set_lo_133(shot, ctrl, 2, rs); break;}
      break;

    case 0x6000:
      if (shot < 7823)  {set_lo_133(shot, ctrl, 1, rs); break;}
      if (shot < 8724)  {set_lo_101(shot, ctrl, 1, rs); break;}
                        {set_lo_128(shot, ctrl, 2, rs); break;}
      break;

    case 0x0000:
        /*mwillens replace 167 with set_lo_101S*/
      if (shot >= 39795) {set_lo_105(shot, ctrl, 2, rs); break;}
      if (shot >= 31788) {set_lo_101S(shot, ctrl, 2, rs); break;}  
      if (shot >= 8724) {set_lo_167(shot, ctrl, 2, rs); break;}
      if (shot >= 7823) {set_lo_128(shot, ctrl, 1, rs); break;}

    default:
      error |= ECEREF_CTRL_SWPOS;
  }

/* SPDT #1 coax switch */

  switch (ctrl & 0x0800) 
  { 
    case 0x0800:        
      if (shot < 8724) {set_lo_133(shot, ctrl, 2, rs); break;}
                       {set_lo_133(shot, ctrl, 3, rs); break;}
    case 0x0000:
      if (shot < 7823)  {set_lo_101(shot, ctrl, 2, rs); break;}
      if (shot < 8724)  {set_lo_167(shot, ctrl, 2, rs); break;}
                        {set_lo_95 (shot, ctrl, 3, rs); break;}
      break;
  }

/* SPDT #2 coax switch - used only between 7823 and 8724 */

  if (shot >= 7823  && shot < 8724)
  {
    switch (ctrl & 0x1000) 
    {
      case 0x1000: {set_lo_133(shot, ctrl, 3, rs); break;}
      case 0x0000: {set_lo_128(shot, ctrl, 3, rs); break;}
    }
  }

/* SP6T  coax switch - not installed before 8724 */
/*
Truth table for SP6T coax switch status (channels 1-36)

Bit 10  Bit 9   Bit 8   mixer selected 
--------------------------------------
  0       0       0     none
  0       0       1     101 GHz
  0       1       0      95 GHz
  0       1       1     128 GHz
  1       0       0     105 GHz
  1       0       1     91 GHz
  1       1       0       "
  1       1       1       "
-------------------------------------- */

  if (shot >= 8724)
    {
      // select Bit8-Bit10
    switch (ctrl & 0x0700) 
       {
      case 0x0100: {set_lo_101(shot, ctrl, 1, rs); break;}
      case 0x0200: {set_lo_95 (shot, ctrl, 1, rs); break;}
      case 0x0300: {set_lo_128(shot, ctrl, 1, rs); break;}
      /*mwillens replace 167 with set_lo_101S*/
      case 0x0400: {
	if(shot >= 39795)
	  {set_lo_105(shot, ctrl, 1, rs); break;} //new mixer:
	else
	  {
                if(shot <= 31788)
                        {set_lo_167(shot, ctrl, 1, rs); break;}
                else    
                        {set_lo_101S(shot, ctrl, 1, rs); break;}
	  }
      }
	//new mixer:
      case 0x0500: { 
	if(shot >= 33733){
	  set_lo_91(shot, ctrl, 1, rs); break;}
	else
	  {error |= ECEREF_CTRL_SWPOS;}
      }	
    /* higher codes not yet used */
      default:
        error |= ECEREF_CTRL_SWPOS;
      }
    }

  return(error);
} /* radsf_switchsettings */

