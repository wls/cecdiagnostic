/*
    linint.c - 1-dimensional linear interpolation

    05-Nov-1993  Wolfgang Suttrop 
    01-Jun-2010  W.S. modernised
	       
*/

#include <stdio.h>
#include "libece.h"



int  linintf (int n, float *x, float *y,  int m, float *r, float *s)
/*
  int   n;             number of reference data points 
  float *x, *y;        [n] coordinates of reference data points,
		       x[n] must be either in ascending or 
		       descending order
  int   m;             number of result data points 
  float *r,            [m] prescribed result abscissa values,
		       in any order 
        *s;            [m] resulting ordinate values 
*/

{
  int   i1, i2, i;
  float denom;

/* treat trivial cases first */

  if (n<=0 || m<=0) return(-1);

  if (n==1) 
  {                   /* assume constant value */
    for (i=0; i<m; ++i) *s++ = *y;
    return (0);
  }

/* go through all data points */

  for (i=0; i<m; ++i) 
  {
    findindexf (x, *r, n, &i1, &i2, NULL);
    if (i2==0)   i2=1;       /* outside range ? */
    if (i1==n-1) i1=n-2;     /* extrapolate from last interval */
    if ((denom = x[i2] - x[i1]) != 0.0)
      *s++ = (  y[i1] * ( x[i2] - *r )    /* weighted average */
              + y[i2] * ( *r - x[i1] ) )
	    / denom;
    else
      *s++ = 0.5 * ( y[i1] + y[i2] );     /* arithm. average */
    ++r;
  }

  return(0);
}  /* linintf */


/* end of linint.c */
