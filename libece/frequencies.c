/* frequencies.c - obtain channel frequencies */
/* Changes: Sept 2012: USB and LSB for mixer 101 LBarrera*/
/* Modifications:
	19 Okt 2012: calculate all the frequencies and not only those of the good channels 
	(sfh RMC have errors otherwise)
	30 April 2023 change frequency of 105 from 105.02 to 104.403 GHz*/

#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include "libece.h"


/* ------- setup_if_parameters ----------------- */

/*  From LO (LO_connection), look up for each IF band:
    waveguide, freq_LO, sideband, calchannel
 */

/* enum  LO_Connection {none, LO101U, LO101L, LO95U, 
   LO133L, LO133U, LO128U, LO128N, LO167L, LO167U}; */

// change waveguide settings 
static float freq_LO [MAX_LO_CONNECTIONS] = 
  {0.0, 101.0e9, 101.0e9, 95e9, 133e9, 133e9, 128e9, 128e9, 167e9, 167e9, 100.92e9, 91e9, 91e9, 104.403e9};

static int waveguide [MAX_LO_CONNECTIONS] = 
  {  0,    11,   11 ,   10,    4,     0,     0,    3,    0,      0,      11,      12,   0, 11};

/*static int sideband [MAX_LO_CONNECTIONS] = 
  {  ' ', 'L', 'U',  'U',   'L',   'U',   'U',   'U',   'L',  'U'};*/
/*From Sept-2012: change in USB and LSB for 101 mixer*/

static int sideband [MAX_LO_CONNECTIONS] = 
  {  ' ', 'U', 'L',  'U',   'L',   'U',   'U',   'U',   'L',  'U',  'U' ,     'L',  'U', 'U'};


static int calchannel [MAX_LO_CONNECTIONS] = 
  { 0,     0,   0,     0,    0,     0,       0,     0,    0,    0,   0,   0,    0, 0};





void setup_if_parameters(struct RadiometerSettings *rs)
{
  int i;
  for (i=0; i<MAX_IFGROUP; ++i)
    {
      enum LO_Connection lo = rs->LO[i];
      rs->freq_LO[i]    = freq_LO[lo];
      rs->waveguide[i]  = waveguide[lo];
      rs->sideband[i]   = sideband[lo];
      rs->calchannel[i] = calchannel[lo];
    }
}



/* ------- calc_channel_frequencies ----------------- */

/* 
  Calculate "freqrf" for each active channel ("have"!=0)
  from:
    ifgroup, sideband, freq_LO, freqif

  If a channel has undefined ifgroup, sideband or waveguide,
  switch it off ("have=0").
  Number of disabled channels is returned in "noff"
 */

void calc_channel_frequencies(struct RadiometerSettings *rs, int *noff)
{
  double m_e = ELECTRON_MASS / ELEMENTARY_CHARGE;
  float sbfact;
  int i;
  if (noff) *noff=0;
  for (i=0; i<MAX_CHAN; ++i)
  {
    //if (shot rs->have[i])   //LBarrera: calculate the frequencies always, and after that remove bad channel-> set rs->have=0
    //{
      int nif = rs->ifgroup[i] -1;
      if ( (nif < 0) || (nif >= MAX_IFGROUP) 
	    || rs->sideband[nif]==0 
	    || rs->waveguide[nif]==0 )
      {
	rs->have[i]=0;
	if (noff) ++(*noff);
	rs->freqrf[i] = 0;
	rs->Btot[i]=0;
	continue;
      }     
      switch (rs->sideband[nif])
        {
        case 'U': case 'u': 
	  sbfact = 1; 
	  break;
        case 'N': case 'n': 
	  sbfact = 1; 
	  break;  /* 128USB, with notch filter */
        case 'S': case 's': 
	  sbfact = 1; 
	  break;  /* new 101USB, -> S for superadvanced, mwillens  */
        case 'L': case 'l': 
	  sbfact = -1; 
	  break;
	default:    /* not a valid sideband - ignore channel */            
	  sbfact = 0;
	  rs->have[i]=0;
	  if (noff) ++(*noff);
	  rs->freqrf[i] = 0;
	  rs->Btot[i]=0;
	  continue;
        }

      rs->freqrf[i] = rs->freq_LO[nif] 
	              + (sbfact * rs->freqif[i]);
      rs->Btot[i] = (float) (rs->freqrf[i] * 2 * M_PI * m_e / rs->harmonic[nif]);
    //}
  }

}  /* calc_channel_frequencies */

