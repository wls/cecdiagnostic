/* calibration.c - subroutines for ECE calibration factors */
/* LBarrera: inlcude also calibration factors for RMD (different gainld, gainma2)
 	13.03.2013 save minimum delta Temperature that can be measured to include in sfh*/

#include <ctype.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "libece.h"


/* calibration data structure
   stripped down version for radiometer:
   Use only two calibration "channels":
     1. No notch filter
     2. With notch filter (option for ECRH suppression)
   Radiometer channels are identified by their RF frequency (freqrf).
*/

#define STRLEN 128
#define BUFLEN 256

/* ...........  read calibration file ................ */

/* 
   read calibration table from file "fn" into structure "cal"
*/


int read_cal_file (char *fn, struct caldata *cal)
{
  char str1[STRLEN], str2[STRLEN],
       targetname[STRLEN], refname[STRLEN], varname[STRLEN],
       buf[BUFLEN];
  int c, e, nchannels, chan;
  double f;
  float s, ds;
  FILE *fp;

  if (!(fp=fopen(fn, "r"))) 
  {
    return(LIBECE_CALIBRATION_NOT_FOUND);
  }

/* read calibration file header */

  if (!fgets(buf, BUFLEN, fp)) goto error2;   /*line1*/
  sscanf(buf, "%s%d", str1, &(cal->version));
  if (strcmp(str1, "CAL=")) goto error3;

  if (!fgets(buf, BUFLEN, fp)) goto error2;   /*line2*/
  sscanf(buf, "%s%s%s%d", str1, targetname,
         str2, &nchannels);

  if (nchannels>MAX_CAL_CHAN)
  {
    fclose(fp);
    return(LIBECE_CALIBRATION_CHANNELS_OVERFLOW);
  }

  if (strcmp(str1, "TARGET=")) goto error3;
  if (strcmp(str2, "CHANNELS=")) goto error3;

  if (!fgets(buf, BUFLEN, fp)) goto error2;   /*line3*/
  sscanf(buf, "%s%s%s%s", str1, refname, str2, varname);
  if (strcmp(str1, "REF=")) goto error3;
  if (strcmp(str2, "DELTAT=")) goto error3; 


  for (c=0; c<nchannels; ++c)
  {

    if (!fgets(buf, BUFLEN, fp)) goto error2;
    sscanf(buf, "%s%d%s%d", str1, &chan, str2, &(cal->entries[c]));
    if (strcmp(str1, "CHANNEL=")) goto error3;
    if (strcmp(str2, "ENTRIES=")) goto error3;

    if (chan!=c+1)
    {
      fclose(fp);
      return(LIBECE_CALIBRATION_INCONSISTENT_CHNO);
    }

    if (cal->entries[c] <2)
    {
      fclose(fp);
      return(LIBECE_CALIBRATION_TOO_FEW);
    }

/* loop through all entries of this one channel */
    for (e=0; e < cal->entries[c]; ++e) 
    {
      if (!fgets(buf, BUFLEN, fp)) goto error2;
      sscanf(buf, "%lf%e%e", &f, &s, &ds);
      cal->f  [c][e] = f * 1e9;
      cal->s  [c][e] = s;
      cal->ds [c][e] = ds;
    } 

  }  /* for c */

/* success exit */  
  fclose(fp);
  return(LIBECE_OK);

/* error exits */
error2:
  fclose(fp);
  return(LIBECE_CALIBRATION_UNEXPECTED_EOF);

error3:
  fclose(fp);
  return(LIBECE_CALIBRATION_WRONG_FORMAT);

}  /* read_cal_file */



/* ........ calculate actual calibration factors ....... */

/* 
  Preconditions:
    - must have read calibration data from calrad.* into "cal"
    - the following "rs" fields must be set:
      ifgroup, calchannel, freqrf, 
      gainpa, gainma1, gainma2, gainld,
      attnif, bif.
  As a result, "calfact" will be set for each active channel ("have" !=0) 
 */


int calibration_factors (struct caldata *cal,
			 struct RadiometerSettings *rs,
			 int *noff)
{
  int i, cc, nif, ci;
  float video_gain, denom;
   
  if (noff) *noff = 0;
  
  for (i=0; i<MAX_CHAN; ++i)
  {
    if (rs->have[i])
    {
      nif = rs->ifgroup[i] - 1;
      cc = rs->calchannel[nif];

     // printf("%f %d \n", rs->freqrf[i], cc)   ;
      ci = closestindex (&(cal->f[cc][0]), 
			 (double) rs->freqrf[i], cal->entries[cc]);

      video_gain = rs->gainpa[i] * rs->gainma1[i] 
	         * rs->gainma2[i] * rs->gainld[i]
	         * ( (float) pow ((double) 10.0, (double) (-0.1*rs->attnif[nif])) );

      rs-> S[i] = cal-> s[cc][ci];   /* calibration factors Udet/intensity */
      rs->dS[i] = cal->ds[cc][ci];   /* calibration uncertainty (SNR of the calibration procedure)*/
      
      denom = ELEMENTARY_CHARGE * video_gain * rs->S[i]
	    * rs->freqrf[i] * rs->freqrf[i] * rs->bif[i];
      //printf("%f %f %f %d \n",rs->S[i],rs->freqrf[i],rs->bif[i], cc)    ;
      if (rs->S[i] && denom>0){
	rs->calfact[i] = C2 / denom;
	}
      else 
      {
	rs->calfact[i] =0;
	rs->have[i] = 0;    /* switch off channel */
	if (noff) ++(*noff);
      }
    }
  }

  return(0);
}  /* calibration_factors */

int calibration_factors_rmd (struct caldata *cal,
			 struct RadiometerSettings *rs,
			 int *noff)
{
  int ch, cc, nif, ci;
  float video_gain;

  if (noff) *noff = 0;
  for (ch=0; ch<MAX_CHAN; ++ch)
   {
   if (rs->have[ch])
      {
	nif = rs->ifgroup[ch] - 1;
        cc = rs->calchannel[nif];
        ci = closestindex (&(cal->f[cc][0]), 
			 (double) rs->freqrf[ch], cal->entries[cc]);
	rs-> S[ch] = cal-> s[cc][ci];   /* calibration factors Udet/intensity */
        rs->dS[ch] = cal->ds[cc][ci];   /* calibration uncertainty (SNR of the calibration procedure)*/


      /* gainma2 and gainld absorbed in RMC bits-to-volts calibration */
	float denom=0.0;
	video_gain = rs->gainpa[ch] * rs->gainma1[ch] 
	         * ( (float) pow ((double) 10.0, 
				  (double) (-0.1*rs->attnif[nif])) );

	denom = ELEMENTARY_CHARGE * video_gain * rs->S[ch]
		* rs->freqrf[ch] * rs->freqrf[ch] * rs->bif[ch];

	if (rs->S[ch] && denom>0) {
		rs->calfact[ch] = C2 / denom;  //Change 20 Marz 2013. Signal in RMC are always inverted! calibration factor >0 -->change in MULTIA00 shf
	}
	else 
        {
	 rs->calfact[ch] =0;
	 rs->have[ch] = 0;    /* switch off channel */
	 if (noff) ++(*noff);
        }
      } //endif rs->have
  } //end for

  return(0);
}  /* calibration_factors _rmd*/


/* end of libece/calibration.c */
