/*
    findindex.c - binary search for closest, nearest lower
                  and nearest upper element in a sorted
		  (ascending or descending) double precision 
		  array.

       Wolfgang Suttrop suttrop@ipp.mpg.de

       06-Aug-1993 wls  new
       22-Jul-2009 nkh  Change float to double 
       25-Mar-2010 wls  modernised
*/

#include <stdio.h>

#include "libece.h"


int  floorindex (double *data, double val, int len)
    {
    int   indx;
    findindex(data, val, len, &indx, NULL, NULL);
    return(indx);
    }


int  ceilindex (double *data, double val, int len)
    {
    int   indx;
    findindex(data, val, len, NULL, &indx, NULL);
    return(indx);
    }


int  closestindex (double *data, double val, int len)
    {
    int   indx;
    findindex(data, val, len, NULL, NULL, &indx);
    return(indx);
    }



void  findindex (double *data, double val, int leng, 
		 int *floori, int *ceili, int *closesti)
    {
    int l, m, u;

    l=0;
    u=leng-1;

    if (leng>2) {

      if ( data[u] > data[l] ) 
      { /* acending order */
        while (u>l+1)     /* binary search for ... */
	{
	  m = (l+u)/2;     /* .. interval around val */
	  if (val > data[m])  l=m;
	  else                u=m;
	}
        if (closesti)
	{
          if (val-data[l] < data[u]-val)
	    *closesti = l;
          else
 	    *closesti = u;
	}
      }
      else 
      {               /* descending order */
        while (u>l+1) 
	{
	  m = (l+u)/2;
	  if (val <= data[m])  l=m;
	  else                 u=m;
	}
        if (closesti)
	{
          if (data[l]-val <  val-data[u])
	    *closesti = l;
          else
 	    *closesti = u;
	}
      }
    } 
    if (floori) *floori = l;
    if (ceili)  *ceili  = u;

    }     /*findindex*/




/* single precision version */

void  findindexf (float *data, float val, int leng, 
		 int *floori, int *ceili, int *closesti)
    {
    int l, m, u;

    l=0;
    u=leng-1;

    if (leng>2) {

      if ( data[u] > data[l] ) 
      { /* acending order */
        while (u>l+1)     /* binary search for ... */
	{
	  m = (l+u)/2;     /* .. interval around val */
	  if (val > data[m])  l=m;
	  else                u=m;
	}
        if (closesti)
	{
          if (val-data[l] < data[u]-val)
	    *closesti = l;
          else
 	    *closesti = u;
	}
      }
      else 
      {               /* descending order */
        while (u>l+1) 
	{
	  m = (l+u)/2;
	  if (val <= data[m])  l=m;
	  else                 u=m;
	}
        if (closesti)
	{
          if (data[l]-val <  val-data[u])
	    *closesti = l;
          else
 	    *closesti = u;
	}
      }
    }
    if (floori) *floori = l;
    if (ceili)  *ceili  = u;

    }     /*findindexf*/


/* Ende findindex.c */
