/* Soubrutine to calculate the intrinsic error of the ECE data 
 SNR= Signal to Noise Ratio
*/
#include <malloc.h>
#include <math.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <time.h>
#include <sys/time.h>


#include "param.h"
#include "libece.h"
#include "util.h"
#include "eceradiometer.h" 


/* ---------------------------------------------------------------
   Calculate SNR ratio of channels
------------------------------------------------------------------ */
/*  error_bars- calculate error bars of all the channels and write them in sfh. 
    float SNR_calib[MAX_CHAN]; SNR of the calibration factor calfact.
    float SNR_channel[MAX_CHAN]; SNR of the channel.
    float SNR[MAX_CHAN]; 

    1. SNR channel: calculated at the begining of the time trace for the sample rate choosen
    2. SNR_calib: in the future -> write SNR in calib. file. For shots before xxx estimated to be error 10%
	   error=error_calib(given by SNR_calibration)+7% systematic error(alignment, insertion loss, ...)
	   sigma=7% --> SNR_systematic=100./7.
           1/SNR_calib=SQRT((1/SNR_calibration)^2 +(1/SNR_systematic)^2)  ;Correction 29 Nov. 2012
    3. 1/SNR=SQRT(SNR_channel^-2 +SNR_calib^-2)

*/ 
int SNR_channel (struct RadiometerSettings *pRadSet,
		struct ChannelData *pChanData)
{

  
  int c, it;
  int nt;
  nt=pChanData->ntimes;
  float Te_aux [nt];
  int nt2=0;   //index for time interval to calculate SNR
  float tim2=0.1;  //calculate SNR the first 0.1 seconds.
  it=0;
  while(pChanData->time[it] <tim2)
  { 
   nt2=it;
    ++it;
  }
 //sprintf(msg, "Number of points nt2 %u", nt2);
 //   log_msg(INFO, msg);
  float sum, sum2, aver, aver2, mean,noise, aux;
  double SNR ;

  if (!pRadSet || !pChanData) return(1);

  for (c=0; c<MAX_CHAN; ++c)
  {
     sum=0;
     sum2=0;
     aver=0;
     aver2=0;
     mean=0;
   //  offset=0;
     if (pRadSet->have[c])
     {	
	mean=0;
	for (it=0; it<nt; ++it)  //Calculate SNR of the channel
	{
	 aux=pChanData->Trad[c][it];
	 mean=mean+aux;
	}
	 mean=mean/nt;
	for (it=0; it<nt2; ++it)  //Calculate SNR of the channel
	{
	 Te_aux[it]=pChanData->Trad[c][it];
	 aux=Te_aux[it];
	 sum=sum+aux;
	 sum2=sum2+aux*aux;
	}
	 aver=sum/nt2;
	 aver2=sum2/nt2;
	 noise=fabs(aver2-aver*aver);
	 noise=sqrt(noise);
	if (noise>0) SNR =fabs((double)mean/noise);  //SNR=mean(of total)/noise(of background)
	else SNR = 1E31;
	 pRadSet->SNR_channel[c]=SNR;
	
      }else 
	{
	pRadSet->SNR_channel[c]=0;
	}  
   
  }
   return(0);       
}


int SNR_calib (int shot, struct RadiometerSettings *pRadSet,
		struct ChannelData *pChanData)
{
 float snr_c,a,b,denom;
 //estimated error bar for shots without temperature control: 18% 
 //(asuming variations of Te_peltier=+-4degree)
 // otherwiwe estimated error bar=10% 
  if (!pRadSet || !pChanData) return(1);
  
  int c;
  if (shot < 27658){   
	for (c=0; c<MAX_CHAN; ++c){
	pRadSet->SNR_calib[c]=100./18.;
	/* For other calibration implement this part...
	When temperature of the peltier include error +2% per degree*/
        //sprintf(msg, "Channel %u SNR  (calib 1)%f",c+1,pRadSet->SNR_calib[c]);
	//log_msg(INFO, msg); 
	}
 }else{
	if (shot <28079){
	/* Set higher error bars for those pulses as there were an error in the 
	temperature notes and in the settings for mixers 133L and 128 (4 degree difrerence):*/
	for (c=0; c<MAX_CHAN; ++c){pRadSet->SNR_calib[c]=100./16.;}
	}else{
	 for (c=0; c<MAX_CHAN; ++c){
	/*if the signal to noise ratio in the calibration procedure is higher than 10% assume this as error, otherwise, due to errors in alignement, variations, transmision losses... assume systematic 10% error:
	snr_c=pRadSet->dS[c];
	if (snr_c <= 10.) pRadSet->SNR_calib[c]=snr_c;
	else pRadSet->SNR_calib[c]=100./10.;
	sprintf(msg, "Channel %u SNR (calib 2) %f",c+1,pRadSet->SNR_calib[c]);
	log_msg(INFO, msg); 
	S.Rathgeber & LBarrera Change 23. Nov. 2012: 
	assume as error: pRadSet->SNR_calib[c] + 7% 
	(systematic error = 7%)
        --> good channels would differ from worse channels and that is an important input for IDA. 
	7% --> 10% - (2 or 3%) (error variation signal - the intrinsic scatter of channel data) */
	snr_c=pRadSet->dS[c];
	//snr_c=snr_c+(100./7.); 
	/*Correction 29 Nov.2012 
	-->sigma_calib=7%+error due to SNR channel in calibration(dS)
	SNR_calib=S/sigma_calib=S/((S/dS+7*S/100)=1/((1/dS)+7/100)
	<--*/
        
	if (snr_c >=0.) 
	{
	 a=(1./snr_c);
	 b=(7./100.);  
	 denom=a+b;
	 if(denom>0) snr_c=1./denom;
      	 else snr_c=0;

	 pRadSet->SNR_calib[c]=snr_c;
	} 
	else pRadSet->SNR_calib[c]=0;

	}
      }
}
 return(0);	   
 
}

int calculate_SNR (int shot, struct RadiometerSettings *pRadSet,
		struct ChannelData *pChanData)
{
  if (!pRadSet || !pChanData) return(1);
  
  if (SNR_calib (shot,pRadSet,pChanData)==0 && SNR_channel (pRadSet,pChanData)==0){	
	int c;
	for (c=0; c<MAX_CHAN; ++c){
     	 if (pRadSet->have[c]){
      		float denom=0.;
		float a=0.;
		float b=0.;
      		float SNR_total=0.;
      		if (pRadSet->SNR_calib[c] && pRadSet->SNR_channel[c]){
		a=(1./pRadSet->SNR_calib[c])*(1./pRadSet->SNR_calib[c]);
		b=(1./pRadSet->SNR_channel[c])*(1./pRadSet->SNR_channel[c]);
		denom=fabs(a+b);
       		denom=sqrt(denom);
       		}else {denom=1E31;}

      		if(denom>0) SNR_total=1./denom;
      		else SNR_total=0;

      		pRadSet->SNR[c]=SNR_total;
  
     	}else pRadSet->SNR[c]=0.;
         } //endfor
    
 return(0);
 } else return(2);
 
}

int calculate_SNR_dT (int shot, struct RadiometerSettings *pRadSet,
		struct ChannelData *pChanData)
{
  //Get dT in pRadSet	
  if (calculate_dT(pRadSet))return(1);
  //Calculate SNR:
  if (!pChanData) return(1);
  if (!calculate_SNR(shot,pRadSet,pChanData)==0)return(2);

  return(0);
	
 
}

int calculate_dT(struct RadiometerSettings *pRadSet){
 
 int ic;
 if (!pRadSet) return(1);	
 for (ic=0;ic<MAX_CHAN;ic++) 
	pRadSet->dTmin[ic]=BIT2VOLT*pRadSet->calfact[ic];
 return(0);

}

/*****end Error bars  */
