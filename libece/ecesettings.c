/* ecesettings.c - AUG ECE radiometer settings 

default settings for 2012 campaign
- changes of IF frequencies compared to 2011
- default gains for "fast" SIO-based DAQ 

 13 marz 2013 read gain from rmc sfh
*/

#include <memory.h>
#include <math.h>
#include <stdio.h>
#include <string.h>

#include "libece.h"
#include "param.h"
#include "util.h"
#include "eceradiometer.h"



/* default IF filter bandwidth (GHz) */
static float b_if[MAX_CHAN] = 
       { 0.3, 0.3, 0.3, 0.3, 0.3, 0.3,
	 0.3, 0.3, 0.3, 0.3, 0.3, 0.3,
	 0.3, 0.3, 0.3, 0.3, 0.3, 0.3,
	 0.3, 0.3, 0.3, 0.3, 0.3, 0.3,
	 0.3, 0.3, 0.3, 0.3, 0.3, 0.3,
	 0.3, 0.3, 0.3, 0.3, 0.3, 0.3,
         0.6, 0.6, 0.6, 0.6, 0.6, 0.6,     
         0.6, 0.6, 0.6, 0.6, 0.6, 0.6,  
         0.6, 0.6, 0.6, 0.6, 0.6, 0.6,
         0.6, 0.6, 0.6, 0.6, 0.6, 0.6 };

/* default IF filter center frequencies (GHz) */
static float   f_if [MAX_CHAN] = 
       { 3.125,  3.408,  3.691,  3.974,  4.257,  4.540,
         4.823,  5.389,  5.955,  6.238,  6.804,  7.370,
         7.936,  8.219,  8.502,  8.785,  9.068,  9.351,
         9.917, 10.200, 10.766, 11.049, 11.332, 11.898, 
        12.200, 12.500, 12.800, 13.400, 13.700, 14.000,
        14.300, 14.600, 14.900, 15.500, 16.100, 16.700,
         2.300,  3.250,  4.384,  5.518,  6.652,  7.786,
         8.920, 10.054, 11.188, 12.35 , 13.55 , 15.350,
         2.90,   3.817,  4.951,  6.085,  7.219,  8.353, 
         9.487, 10.621, 11.755, 12.95 , 14.15 , 15.950 };



void set_radiometer_defaults (struct RadiometerSettings *pRadSet)
{
  int i, c;


  if (!pRadSet) return;

  memset(pRadSet,   0, sizeof(struct RadiometerSettings));

  pRadSet->Bt = 0.0;
  

  for (i=0; i<MAX_IFGROUP; ++i) 
  {
    pRadSet->LO[i] = none;
    pRadSet->attnif[i] = 20.0; /* dB */	
    pRadSet->harmonic[i] = 2;  //Default
  }
  
 

  for (c=0; c<MAX_CHAN; ++c)
  {
    pRadSet->have[c]   = 1;   /* hope for the best */
    pRadSet->bif[c]    = b_if[c] * 1e9;
    pRadSet->freqif[c] = f_if[c] * 1e9;
    pRadSet->gainpa[c] = 200.0;   /* x100 x2 (differential output) */
    pRadSet->gainma1[c] = 2.0;    /* fast channel, through "CAMAC" 
				     main amp differential output */
    pRadSet->gainma2[c] = 1.0;    /* minimum gain in "SIO" main amps */
    pRadSet->gainld[c] = 1.0;     /* not used (no separate line driver 
				     between amp and ADC) */
    pRadSet->bvd[c]    = 500e3;   /* 500 kHz videobandwidth 
				     for 1 MHz sample rate */
  }

  for (c=0; c<36 && c<MAX_CHAN; ++c) pRadSet->ifgroup[c]=1;
  for ( ; c<48 && c<MAX_CHAN; ++c) pRadSet->ifgroup[c]=2;
  for ( ; c<MAX_CHAN; ++c) pRadSet->ifgroup[c]=3;

  pRadSet->zlens=7.5/100.; //default value (m)  include parameter for RMC 12.03.2013
 
}  /*set_radiometer_defaults*/

int get_setgain(int shot, int diaref,struct RadiometerSettings *rs)
{

 /*Read params-A;gainma2*/
   if (rs)
    {	
	int i;
	for (i=0; i<MAX_CHAN; ++i)
	    {
		if (sf_read_parm_float (diaref, "parms-A", "gainma2", 
				  MAX_CHAN, &(rs->gainma2[0])))
			return(-1);
		printf("gainma2 for channel %u %f\n",i+1,rs->gainma2[i]);
      
    	    }
   }

return(0);
}



/* --- reach channel specific parameters from a parameter set ------------ */

static int read_ece_channel_settings (int shot, int diaref, int refmask,			   
				  struct RawDiagnostic *pRawDiag,
				  struct RadiometerSettings *rs)
{
  int error = 0; 
  struct RawSigGroup *sg;

  if (!rs || !pRawDiag ) return(ECEREF_WRONG_PARS);

  {
    /* loop through all (expected) signal groups) */
    sg = pRawDiag->sgroups;
    while(sg)
    {
      int sc = sg->startchno,
	  nc = sg->nchans, i;

      if (refmask & ECEREF_IFGROUP)
	{
	  if (sf_read_parm_int (diaref, sg->device_name, "IFGROUP", 
				nc, &(rs->ifgroup[sc])))
		error |= ECEREF_IFGROUP;
	}

      if (refmask & ECEREF_FREQIF)
	{
	  if (sf_read_parm_float (diaref, sg->device_name, "FREQIF", 
				  nc, &(rs->freqif[sc]))) 
	    error |= ECEREF_FREQIF;
	  if (sf_read_parm_float (diaref, sg->device_name, "BIF", 
				  nc, &(rs->bif[sc]))) 
	    error |= ECEREF_FREQIF;

	  for (i=sc; i<sc+nc; ++i)
	    {
	      rs->freqif[i] *= 1e9;   /* GHz -> Hz*/
	      rs->bif[i] *= 1e9;
	      if (rs->sideband[i] == 'U')    /* RF frequencies */
		rs->freqrf[i] = rs->freq_LO[i] + rs->freqif[i];
	      else
		rs->freqrf[i] = rs->freq_LO[i] - rs->freqif[i];		
	    }
        }

      if (refmask & ECEREF_VGAIN)
	{
	  if (sf_read_parm_float (diaref, sg->device_name, "GAINPA", 
				  nc, &(rs->gainpa[sc])))
	    error |= ECEREF_VGAIN;
	  if (sf_read_parm_float (diaref, sg->device_name, "GAINMA1", 
				  nc, &(rs->gainma1[sc]))) 
	    error |= ECEREF_VGAIN;
	  if (sf_read_parm_float (diaref, sg->device_name, "GAINMA2", 
				  nc, &(rs->gainma2[sc]))) 
	    error |= ECEREF_VGAIN;
	  if (sf_read_parm_float (diaref, sg->device_name, "GAINLD", 
				  nc, &(rs->gainld[sc]))) 
	    error |= ECEREF_VGAIN;
	}

      if (refmask & ECEREF_VBANDW)
	{
	  if (sf_read_parm_float (diaref, sg->device_name, "BVD", 
				  nc, &(rs->bvd[sc]))) 
	    error |= ECEREF_VBANDW;
	}

      for (i=sc; i<sc+nc; ++i) 
	rs->have[i] = 1;  /* so far */

      sg=sg->next;
    }   

  return(error);
  }

}  /* read_ece_channel_settings() */




/* ----------- read ECE settings from a reference shot file -------------- */

int   read_ece_settings       (char *experiment, char *diagname, 
			       int *shot, int *edition,
			       int refmask  /* combinations of ECEREF_* */,
			       struct RadiometerSettings *pRadSet)
{
  int n_ifgroups = 0, diaref;
  int error = 0;  /* errors, bit-coded as in "refmask" */

  struct RawDiagnostic *pDiag;


  /* obtain diagnostic descriptor (=shot file structure info) */
  if ( strcmp(diagname, "RAD")==0 )     /* RAD shotfile */
    {
      pDiag = radsf_setup (*shot);
      if (!pDiag) return(SF_ERROR);
    }

  n_ifgroups = pDiag->n_ifgroups;


  if (sf_open_read(experiment, diagname, shot, edition, &diaref) == SF_ERROR)
    return(SF_ERROR);

  if (refmask & ECEREF_CALIBSRC)
    {
      if (sf_read_parm_int (diaref, "METHODS", "CALIBSRC", 1, &(pRadSet->calibsrc))) 
	error |= ECEREF_CALIBSRC;
    }

  if (refmask & ECEREF_HARMONIC)
    {
      if (sf_read_parm_int (diaref, "METHODS", "HARMONIC", 1, &(pRadSet->harmonic[0]))) 
	error |= ECEREF_HARMONIC;
    }

  if (refmask & ECEREF_ZLENS)
    {
      /* adjustable lens position until 2008 */
      if ( strcmp(diagname, "RAD")==0 && *shot <= 23664)
	{
	  /* add code here to read and avaluate lens position signal from RAD */
	  error |= ECEREF_ZLENS;
	}

      /* fixed lens position (or position as stored in a level-1 file) */
      else
	{
          if (sf_read_parm_float (diaref, "METHODS", "ZLENS", 1, &(pRadSet->zlens))) 
	    error |= ECEREF_ZLENS;
          else
            pRadSet->zlens /= 100;   /* cm -> m */
	}
    }

  if (refmask & ECEREF_RECEIVER)
    {
      if ( strcmp(diagname, "RAD")==0 )
	{
	  int err = radsf_switchsettings (*shot, diaref, pRadSet);
	  if (err) error |= ECEREF_RECEIVER;
	  /* if RAD switch settings are given, 
	     explicit WAVEGUID parameters are not used */
	}
      else
	{
	  int i;

	  if (sf_read_parm_float (diaref, "METHODS", "FREQLO", 
		          n_ifgroups, &(pRadSet->freq_LO[0]))) return(-1);

	  if (sf_read_parm_char (diaref, "METHODS", "SIDEBAND", 
		         n_ifgroups, &(pRadSet->sideband[0]))) return(-1);

	  /* find LO connection */
	  for (i=0; i<n_ifgroups; ++i)
	    {
	      int sb = -1; 
	      switch (pRadSet->sideband[i])
		{
		case 'u': pRadSet->sideband[i]= 'U'; 
		case 'U': sb = 1; break;
		case 'l': pRadSet->sideband[i]= 'L';
		case 'L': sb = 0; break;
		case '\000': pRadSet->sideband[i]= ' ';
		case ' ': break;
		default:
		  {
		    pRadSet->sideband[i] = ' ';
		    /* pRadSet->waveguide[i] = ' '; */
		    pRadSet->LO[i] = none;
		  }
		}

	      if (sb>=0)
		{
		  if (pRadSet->freq_LO[i] < 96)
		    pRadSet->LO[i] = LO95U;
		  else if (pRadSet->freq_LO[i] < 102)
		    pRadSet->LO[i] = LO101L + sb;
		  else if (pRadSet->freq_LO[i] < 129)
		    pRadSet->LO[i] = LO128U;
		  else if (pRadSet->freq_LO[i] < 133)
		    pRadSet->LO[i] = LO133L + sb;
		  else 
		    pRadSet->LO[i] = LO167L + sb;
		}
	      else
		pRadSet->LO[i] = none;

	      pRadSet->freq_LO[i] *= 1e9;  /* GHz -> Hz */
	    }

	  if (refmask & ECEREF_WAVEGUID)
	    if (sf_read_parm_int (diaref, "METHODS", "WAVEGUID", 
			     n_ifgroups, &(pRadSet->waveguide[0])))
	      error |= ECEREF_WAVEGUID;      
	}
    }

  else if (refmask & ECEREF_WAVEGUID)
    {
       if (sf_read_parm_int (diaref, "METHODS", "WAVEGUID", 
			     n_ifgroups, &(pRadSet->waveguide[0])))
	 error |= ECEREF_WAVEGUID;
    }

  if (refmask & ECEREF_ATTNIF)
    {
      if (sf_read_parm_float (diaref, "METHODS", "ATTNIF", 
			      n_ifgroups, &(pRadSet->attnif[0]))) 
	error |= ECEREF_ATTNIF;

    }

  /* obtain channel specific data */

  error |= 
    read_ece_channel_settings (*shot, diaref, refmask, pDiag, pRadSet);

  /* Obtain gainma2 read from RMC*/
  error |=
    get_setgain(*shot, diaref,pRadSet);

  

  sf_close_read(diaref);


  return(error);
}  /*sf_read_ecesettings*/
