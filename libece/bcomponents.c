/* bcomponents.c

   read components of B (Bt, BR, Bz) 

   from equilibrium (using libkk's kkrzbrzt())
*/
#include <malloc.h>
#include <stdio.h>
#include <string.h>


#include "libece.h"
#include "libkk.h"
#include "param.h"






/* FORTRAN entry */

void bcomponents_ (int *status,
		   int *shot, float *time, char *exper, char *diag, int *edition,
		   int *nlos, float *Rlos, float *zlos,
		   float *Bt, float *BR, float *Bz,
		   float *pflux, float *Jp, 
		   int expstrlen, int diastrlen)
{
  char expstr[20], diastr[20];
  int stat;

  strncpy(expstr, exper, expstrlen);
  expstr[expstrlen] = '\0';

  strncpy(diastr, diag, diastrlen);
  diastr[diastrlen] = '\0';

  stat = bcomponents (*shot, *time, expstr, diastr, edition,
			 *nlos, Rlos, zlos, Bt, BR, Bz, pflux, Jp);

  if (status) *status = stat;
} /* bcomponents_ */




/* C entry */

int bcomponents (int shot, float time, char *exper, char *diag, int *edition,
		 int nlos, float *Rlos, float *zlos,
		 float *Bt, float *BR, float *Bz,
		 float *pflux, float *Jp)
{
  int iERR;

  kkrzbrzt_ (&iERR, exper, diag, &shot, edition,
             &time, Rlos, zlos, &nlos,
	     BR, Bz, Bt, pflux, Jp,
             strlen(exper), strlen(diag));

  if (iERR) 
    return(SF_ERROR);
  else 
    return(SF_OK);

}  /* bcomponents() */



int get_bvac(int shot, char *exper, char *diag, int *edition, int neqtime, float *eqtime, float *Bvac)
{
  
  /*define time_out and Bvac out*/
  float *time_out,*Bvac_out;

  char  msg[256];
  int diaref,nt;

  
  if (sf_open_read (exper, diag, &shot, edition, &diaref) == SF_ERROR)
  {
    sprintf(msg, "Cannot open DIAG: MBI(AUGD)");
    return(-1);
  }
 
  if (sf_signal_ntpoints(diaref, "timeB   ", &nt))
  {
    sprintf(msg, "Cannot determine number of MAI time points."); 
    sf_status(1, msg);
    sf_close_read(diaref);
    return(-2);
  }

  time_out = (float *) calloc(nt, sizeof(float));
  if (!time_out) 
  {
    sprintf(msg, "Cannot allocate %d floats for BTF time base.", nt);
    sf_status(1, msg); 
    sf_close_read(diaref);
    return(-3);
  }
  
  if (sf_read_tbase_float(diaref, "timeB", nt, time_out))
  {
    sprintf(msg, "Cannot read BTF time base."); 
    sf_status(1, msg);
    sf_close_read(diaref);
    return(-4);
  }
  
  Bvac_out = (float *) calloc(nt, sizeof(float));
  if (sf_read_signal_float (diaref, "BTFABB", nt, Bvac_out))
  {
    sprintf(msg, "Cannot read MBI: BTFABB, try to read BTF"); 
    sf_status(1, msg);
    // for old shot not BTFABB is needed
    if (shot > 30161)
      {
	sprintf(msg, "No MBI: BTFABB for an old discharge"); 
	sf_status(1, msg);
	return(-5);
      }
    if (sf_read_signal_float (diaref, "BTF", nt, Bvac_out))
      {
	sprintf(msg, "Cannot read MBI: BTF and BTFABB");
	sf_status(1, msg);
	sf_close_read(diaref);
	return(-6);
      }
  }
  
 
  linintf(nt, time_out, Bvac_out, neqtime , eqtime, Bvac);
  
  sf_close_read(diaref);
  return(0);

}



/* end of bcomponents.c */
