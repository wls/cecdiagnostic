/* sfread.c 
   read AUG shot file data types
   29-Mar-2010 W.S. 
   libddww8 prototypes  23-Sep-2011 W.S. 
   change some parameter type for ddcxsig function, new library
      (uint64_t lname, uint64_t lphysdim)  4-Sept-2013 lbarrera*/
   

#include <memory.h>
#include <stdio.h>
#include <string.h>

#ifdef DDWW8
#include "ddwwansic8.h"
#else
#include "ddwwansic.h"
#endif

#include "libece.h"

/* check status of ddww call */
int sf_status(int error, char *msg1)
{
  if (error==0) return(0);

#ifdef DDWW8
  uint32_t ctrl=3; int32_t unit= -1; 
#else
  int ctrl=3, unit= -1;
#endif
  int level=-1;
  char msg[256];

  memset(msg, '\0', 256);
  xxerrprt_ (&unit, msg, &error, &ctrl, msg1, 255, strlen(msg1)); 
  xxerror_(&error,&ctrl, " ", 1);
  if (xxsev_ (&error)) level = SF_ERROR;
  else if (xxwarn_ (&error)) level = SF_WARNING;
  else if (xxnote_ (&error)) level = SF_INFO;
  else level = SF_DEBUG;    /* debug if error!=0 and no category assigned */

  if (level==SF_ERROR)
    printf("%s\n", msg);

  return(level);
}



/* open a shot file: 
   experiment, diagname, shot, edition

   On exit, 
     shot containes actual shot number
     edition contains actual edition number
*/

int  sf_open_read(char *experiment, char *diagname, 
		  int *shot, int *edition, int *diaref)
{
  char timedate[19];

#ifdef DDWW8
  int32_t  i_error = 0, i_edition = 0, i_diaref = 0;
  uint32_t u_shot;
  uint64_t lexp, ldiag, ltime;

  if (!diagname) return(SF_ERROR);
  if (!experiment) experiment = "AUGD";
  if (shot) u_shot = (uint32_t) *shot;
  if (edition) i_edition = (int32_t) *edition;
  lexp = strlen(experiment);
  ldiag = strlen(diagname);
  ltime = 18;

  ddopen_ ( &i_error, experiment, diagname, &u_shot, &i_edition, &i_diaref, timedate, 
	    lexp, ldiag, ltime );

  *shot = u_shot;
  if (edition) *edition = i_edition;
  if (diaref)  *diaref = i_diaref;
#else
  int i_error = 0;
  ddopen_ (&i_error, experiment, diagname, shot, edition, diaref, timedate, 
	   strlen(experiment), strlen(diagname), (int) 18);
#endif

  if (i_error) 
  {
    char msg2[128];
    sprintf(msg2, "[ddopen %s:%s %d (%d)]", 
	    experiment, diagname, *shot, *edition);
    return(sf_status((int) i_error, msg2));
  }

  return(SF_OK);
}

/* close a shot file via ddclose() */
void  sf_close_read(int diaref)
{
  int error;

  ddclose_ (&error, &diaref);
  if (error) 
  {
    sf_status(error, "[ddclose]");
  }
}

/* read a parameter set (int, float, or char array) */
int  sf_read_parm_int(int diaref, char *psetname, char *name, 
		      int n, int *values)
{
#ifdef DDWW8
  uint32_t inttype = 1, u_n = n;
#else
  int inttype = 1, u_n = n;
#endif
  int error, physunit;

  ddparm_ (&error, &diaref, psetname, name, &inttype,
           &u_n, values, &physunit, 
	   strlen(psetname), strlen(name) );

  if (error)
  {
    char msg[256];
    sprintf(msg, "[ddparm (int) %s %s]", psetname, name);
    sf_status(error, msg);
    return(error);
  }
  return(0);
}

int  sf_read_parm_float(int diaref, char *psetname, char *name, 
		        int n, float *values)
{
#ifdef DDWW8
  uint32_t floattype = 2, u_n = n;
#else
  int floattype = 2, u_n = n;
#endif
  int error, physunit;

  ddparm_ (&error, &diaref, psetname, name, &floattype,
           &u_n, values, &physunit, 
	   strlen(psetname), strlen(name) );

  if (error)
  {
    char msg[256];
    sprintf(msg, "[ddparm (float) %s %s]", psetname, name);
    sf_status(error, msg);
    return(error);
  }
  return(0);
}

int  sf_read_parm_char(int diaref, char *psetname, char *name, 
		        int n, char *values)
{
#ifdef DDWW8
  uint32_t chartype = 6, u_n = n;
#else
  int chartype = 6, u_n = n;
#endif
  int error, physunit;

  ddparm_ (&error, &diaref, psetname, name, &chartype,
           &u_n, values, &physunit, 
	   strlen(psetname), strlen(name) );

  if (error)
  {
    char msg[256];
    sprintf(msg, "[ddparm (char) %s %s]", psetname, name);
    sf_status(error, msg);
    return(error);
  }
  return(0);
}

/* read integer signal (n values) */
int  sf_read_signal_int (int diaref, char *name, int n, int *values)
{
#ifdef DDWW8
  int32_t error, i_diaref = diaref;
  uint32_t inttype = 1, leng, u_n = n, k1=1, k2=n;
#else
  int error, i_diaref = diaref;
  int inttype = 1, leng, u_n = n, k1=1, k2=n;
#endif
  ddsignal_ (&error, &i_diaref, name, &k1, &k2, &inttype,
	     &u_n, values, &leng, strlen(name) );

  if (error)
  {
    char msg[256];
    sprintf(msg, "[ddsignal (int) %s]", name);
    sf_status(error, msg);
    return(error);
  }
  return(0);
}

/* read float signal (n values) */
int  sf_read_signal_float (int diaref, char *name, int n, float *values)
{
#ifdef DDWW8
  int32_t error, i_diaref = diaref;
  uint32_t floattype = 2, leng, u_n = n, k1=1, k2=n;
#else
  int error, i_diaref = diaref;
  int floattype = 2, leng, u_n = n, k1=1, k2=n;
#endif
  ddsignal_ (&error, &i_diaref, name, &k1, &k2, &floattype,
	     &u_n, values, &leng, strlen(name) );

  if (error)
  {
    char msg[256];
    sprintf(msg, "[ddsignal (float) %s]", name);
    sf_status(error, msg);
    return(error);
  }
  return(0);
}


/* extract a calibrated signal out of a signal group */
int   sf_extract_csignal_float (int diaref, char *name, 
				int c, int n, float *values)
{
  char physdim[19];
  // change bz S.Denk, 13.2
#ifdef DDWW8
   int32_t error, i_diaref = diaref, ncal;
   uint32_t floattype = 2, leng, u_n = n, k1=1, k2=n, indices[3]={1,1,1};
   uint64_t lname=strlen(name);
   uint64_t lphysdim=18;
#else
   int error, i_diaref = diaref, ncal;
   int floattype = 2, leng, u_n = n, k1=1, k2=n, indices[3]={1,1,1};
#endif

   indices[0] = c+1;
#ifdef DDWW8
   ddcxsig_ (&error, &i_diaref, name, &k1, &k2, indices, &floattype,
                 &u_n, values, &leng, &ncal, physdim,
                   lname,lphysdim);
#else
   ddcxsig_ (&error, &i_diaref, name, &k1, &k2, indices, &floattype,
                 &u_n, values, &leng, &ncal, physdim,
                   strlen(name),18);       //Before 3.Spept.2013
#endif
   // end of change
  /*#ifdef DDWW8
  int32_t error, i_diaref = diaref, ncal;
  uint32_t floattype = 2, leng, u_n = n, k1=1, k2=n, indices[3]={1,1,1};
  uint64_t lname=strlen(name), lphysdim=18;   
#else
  int error, i_diaref = diaref, ncal;
  int floattype = 2, leng, u_n = n, k1=1, k2=n, indices[3]={1,1,1};
#endif

  indices[0] = c+1;*/
  /*extract calibrated data */
    
    //	ddcxsig_ (&error, &i_diaref, name, &k1, &k2, indices, &floattype, 
    //	    &u_n, values, &leng, &ncal, physdim,
    //		  strlen(name),18);       //Before 3.Spept.2013
  //	ddcxsig_ (&error, &i_diaref, name, &k1, &k2, indices, &floattype, 
  //	    &u_n, values, &leng, &ncal, physdim,
  //		  lname,lphysdim);      
    
  if (error)
  {
    char msg[256];
    sprintf(msg, "[ddcxsig (float) %s %d]", name, c+1);
    sf_status(error, msg);
    return(error);
  }
  return(0);
}



/* read time base in double precision (n values) */
int  sf_read_tbase_double (int diaref, char *name, int n, double *time)
{
#ifdef DDWW8
  int32_t error, i_diaref = diaref;
  uint32_t doubletype = 3, leng, u_n = n, k1=1, k2=n;
#else
  int error, i_diaref = diaref;
  int doubletype = 3, leng, u_n = n, k1=1, k2=n;
#endif

  ddtbase_ (&error, &i_diaref, name, &k1, &k2, &doubletype,
	    &u_n, time, &leng, strlen(name) );
  if (error)
  {
    char msg[256];
    sprintf(msg, "[ddtbase (double) %s]", name);
    sf_status(error, msg);
    return(error);
  }
  return(0);
}

/* read time base in double precision (n values) */
int  sf_read_tbase_float (int diaref, char *name, int n, float *time)
{
#ifdef DDWW8
  int32_t error, i_diaref = diaref;
  uint32_t doubletype = 2, leng, u_n = n, k1=1, k2=n;
#else
  int error, i_diaref = diaref;
  int doubletype = 2, leng, u_n = n, k1=1, k2=n;
#endif

  ddtbase_ (&error, &i_diaref, name, &k1, &k2, &doubletype,
	    &u_n, time, &leng, strlen(name) );
  if (error)
  {
    char msg[256];
    sprintf(msg, "[ddtbase (double) %s]", name);
    sf_status(error, msg);
    return(error);
  }
  return(0);
}

/* get time trace length (number of time points) */
int  sf_signal_ntpoints (int diaref, char *name, int *length)
{
  char tname[9];
#ifdef DDWW8
  int32_t error, i_diaref = diaref, typ;
  uint32_t ind[4];
#else
  int error, i_diaref = diaref, typ;
  int ind[4];
#endif

  ddsinfo_ (&error, &i_diaref, name, &typ, tname, &(ind[0]),
	    strlen(name), 8);

  if (length) *length = ind[0];

  if (error)
  {
    char msg[256];
    sprintf(msg, "[ddsinfo %s]", name);
    sf_status(error, msg);
    return(error);
  }
  return(0);
}


