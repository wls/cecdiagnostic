# Makefile for CEC diagnostic
# Wolfgang Suttrop, 21 Jun 2010

PACKAGE = `basename $(PWD)`

ARCHIVE = $(PACKAGE)-`date +%Y%m%d`

# list of files in source distribution
SRCFILES =  \
  Makefile include/Makefile include/Make.* include/*.h \
  libece/Makefile libece/*.c \
  util/Makefile util/*.c  \
  radctrl/Makefile radctrl/*.h radctrl/*.c  \
  cec/Makefile cec/*.h cec/*.c cec/CEC00000.sfh \
  rmd/Makefile rmd/*.h rmd/*.c rmd/RMD00000.sfh	

# ditto with full path
PSRCFILES = $(SRCFILES:%=$(PACKAGE)/%)

all:
	-@(( echo "--- include ---" ; cd include; $(MAKE) ) ; \
	( echo "--- libece ----" ; cd libece; $(MAKE) ) ; \
	( echo "---- util -----" ;cd util; $(MAKE) ) ; \
	( echo "--- radctrl ---" ;cd radctrl; $(MAKE) ) ; \
	( echo "----- cec  ----" ;cd cec; $(MAKE) ) ; \
	( echo "----- rmd  ----" ;cd rmd; $(MAKE) ) ) 

clean:
	( cd include; $(MAKE) clean ) ; \
	( cd libece; $(MAKE) clean ) ; \
	( cd util; $(MAKE) clean ) ; \
	( cd radctrl; $(MAKE) clean ) ; \
	( cd cec; $(MAKE) clean ) ; \
	( cd rmd; $(MAKE) clean ) 

distclean:
	( cd include; $(MAKE) distclean ) ; \
	( cd libece; $(MAKE) distclean ) ; \
	( cd util; $(MAKE) distclean ) ; \
	( cd radctrl; $(MAKE) distclean ) ; \
	( cd cec; $(MAKE) distclean ) ; \
	( cd rmd; $(MAKE) distclean )

srcdist:
	(cd .. ; \
	tar cf $(ARCHIVE).tar  $(PSRCFILES) ; \
	gzip -f $(ARCHIVE).tar )

# ---------------------------------------------
