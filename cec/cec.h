/* cec.h definitions for CEC
   Wolfgang Suttrop, 25-Mar-2010 */

#ifndef _LIBECE_H_
#include "libece.h"
#endif


/* --------- function prototypes ----------- */

/* calibration.c */
void  init_calibration	      (void);
int   temperature_calibration (int shot,
			       struct RadiometerSettings *rs,
			       struct ChannelData *cd);


/* writecec.c */
void  init_write_cec	(void);
int   write_cec         (int shot, 
			 struct RawDiagnostic *pRawDiag,
			 struct RadiometerSettings *pRadSet,
			 struct ChannelPositions *pChanPos,
			 struct ChannelData *pChanData,
			 struct SampleRateSettings *SRSet);

/* end of cec.h */
