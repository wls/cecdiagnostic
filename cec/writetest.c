/* test write shot files */ 


#include <memory.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ddwwansic8.h"


static void sfstatus(int error, char *msg1)
{
  char msg[256];
  int ctrl=3, unit= -1, level=-1;

  memset(msg, '\0', 256);
  xxerrprt_ (&unit, msg, &error, &ctrl, msg1, 
	     255, strlen(msg1)); 
  printf("%s\n", msg);
}



/* sample "data" */
#define NDATA 41
int   n = NDATA;
float fdata[NDATA];


static void write_ftime(int diaref, char *name)
{
  int   error=0, floattype=2, stride=1;

  wwtbase_ (&error, &diaref, name, &floattype, &n, fdata, &stride, 
	    strlen(name) );
  if (error) 
    sfstatus(error, "wwtbase");
}


static void insert_signal(int diaref, char *name, int c)
{
  int   error=0, floattype=2, stride=1, indices[3] = {1,1,1};
  indices[0] = c+1;

  wwinsert_  (&error, &diaref, name, &floattype, &n, fdata, 
	      &stride, indices, strlen(name));
  if (error) 
    sfstatus(error, "wwinsert");
}


main ()
{
  int diaref, shot = 2000, i, c;
  char *experiment = "WLS", *diagname = "CEC";

  for (i=0; i<NDATA; ++i) fdata[i] = i/10.0;

  /* open shot file */
  {
    int error=0, edtn = -1;
    char timedate[19], *mode = "new";

    wwopen_ (&error, experiment, diagname, &shot, mode, &edtn, 
	     &diaref, timedate,
	     strlen(experiment), strlen(diagname), strlen(mode), 18);

    if (error) 
    {
      sfstatus(error, "wwopen");
      exit(1);
    }

    printf("edition = %d\n", edtn);
  }


  write_ftime(diaref, "rztime");

  for (c=0; c<60; ++c)
    insert_signal(diaref, "R-A", c);


  /* close shot file */
  {
    int error;
    char *disp = "lock", *space= "compress";

    wwclose_ (&error, &diaref, disp, space, strlen(disp), strlen(space));
    if (error) 
    {
      sfstatus(error, "wwclose");
      exit(-1);
    }    
  }
  

} /* main */
