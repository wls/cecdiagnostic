/* writecec.c - write CEC (CED, ...) shot file */
/* Modification: LBarrera 2012
   Include subroutine to write the SNR of the channels
   Correction to include 10% systematic error in SNR_calib 29 Nov.2012
*/
#include <malloc.h>
#include <math.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <time.h>
#include <sys/time.h>

#include "param.h"
#include "libece.h"
#include "util.h"
#include "eceradiometer.h" 
#include "cec.h"

#define GROUP "writecec"

#define LOGSTRLEN 256
char msg[128];

/* write "methods" parameter set */
/* NOW WRITTEN IN util/write_param_rad.c */
/*static int write_methods (int diaref, 
			  struct RadiometerSettings *pRadSet)
{
  int i, n;
  float zlens_cm, freq_LO_GHz[MAX_IFGROUP];

  if (!pRadSet) return(0);

  n = MAX_IFGROUP;
  if (sf_write_parm_int   (diaref, "METHODS", "CALIBSRC", 
			   1, &(pRadSet->calibsrc))) return(-1);

  zlens_cm = pRadSet->zlens * 100;
  if (sf_write_parm_float (diaref, "METHODS", "ZLENS", 
			   1, &zlens_cm)) return(-1);
  if (sf_write_parm_int   (diaref, "METHODS", "IFGROUPS", 
			   1, &n ))   return(-1);
  if (sf_write_parm_char  (diaref, "METHODS", "SIDEBAND", 
			   n, pRadSet->sideband))   return(-1);
  if (sf_write_parm_int   (diaref, "METHODS", "WAVEGUID", 
			   n, pRadSet->waveguide))   return(-1);
  for(i=0; i<n; ++i)
    freq_LO_GHz[i] = pRadSet->freq_LO[i] *1e-9;

  if (sf_write_parm_float (diaref, "METHODS", "FREQLO", 
			   n, freq_LO_GHz)) return(-1);
  if (sf_write_parm_float (diaref, "METHODS", "ATTNIF", 
			   n, pRadSet->attnif)) return(-1);
  return(0);
} 
*/

/* write "parms-A" parameter set */
/*static int write_channel_settings (int diaref, 
			       struct RadiometerSettings *pRadSet)
{
  if (!pRadSet) return(0);

  if (sf_write_parm_float  (diaref, "parms-A", "f", 
		            MAX_CHAN, pRadSet->freqrf)) return(-1);
  if (sf_write_parm_float  (diaref, "parms-A", "df", 
		            MAX_CHAN, pRadSet->bif)) return(-1);
  if (sf_write_parm_float  (diaref, "parms-A", "Btot", 
		            MAX_CHAN, pRadSet->Btot)) return(-1);
  if (sf_write_parm_int    (diaref, "parms-A", "IFGROUP", 
		            MAX_CHAN, pRadSet->ifgroup)) return(-1);
  if (sf_write_parm_float  (diaref, "parms-A", "calfact", 
		            MAX_CHAN, pRadSet->calfact)) return(-1);
  if (sf_write_parm_int    (diaref, "parms-A", "AVAILABL", 
		            MAX_CHAN, pRadSet->have)) return(-1);
  return(0);
} 
*/



/* ........ exported entry .......... */

int write_cec (int shot, 
	       struct RawDiagnostic *pRawDiag,
	       struct RadiometerSettings *pRadSet,
	       struct ChannelPositions *pChanPos,
	       struct ChannelData *pChanData,
	       struct SampleRateSettings *SRSet)
{
  PRM_HANDLE p;
  char msg[LOGSTRLEN], *diag, *exper;
  int type, diaref, stat, 
      nshot = shot, 
      edition = 0;

  p = prm_get("writediag");
  if (!prm_getval(p, &diag, &type)) diag="CEC";

  p = prm_get("writeexp");
  if (!prm_getval(p, &exper, &type)) exper="AUGD";

  stat = sf_open_write(exper, diag, &nshot, &edition, &diaref);
  if (stat==SF_ERROR) 
    {
      sprintf(msg, "cannot open to write %s:%s %d (%d)", 
	      exper, diag, nshot, edition);
      log_msg(ERROR, msg);
      return(-1);
    }

  sprintf(msg, "Writing to %s:%s %d (%d)", exper, diag, nshot, edition);
  log_msg(INFO, msg);

  if (write_channel_data(diaref, pRadSet, pChanData))
  {
    sf_close_write (diaref);
    return(-5);
  }
  /*Calculate and write SNR before writing the methods and channel settings and after the channel data is already available*/
  if (calculate_SNR (shot, pRadSet, pChanData))
  {
    sprintf(msg, "Error writing SNR of the channels: %u", calculate_SNR (shot,pRadSet, pChanData));
    log_msg(ERROR, msg);
    sf_close_write (diaref);
    return(-6);
  }
  if (write_methods(diaref, pRadSet))
  {
    sf_close_write (diaref);
    return(-2);
  }

    if (write_channel_settings(diaref, pRadSet))
  {
    sf_close_write (diaref);
    return(-3);
  }

  if (write_channel_positions(diaref, pRadSet, pChanPos))
  {
    sf_close_write (diaref);
    return(-4);
  }

  if (strcmp(pRadSet->EQEXP, "n.a."))
  {
    sprintf(msg, "R,z source: %s:%s(%d)", pRadSet->EQEXP, pRadSet->RZ_SRC, pRadSet->EQED);
    log_msg(INFO, msg);

    if (write_sflist_equil(diaref, shot, pRadSet))
    {
      sf_close_write (diaref);
      return(-7);
    }
   //Write shot_list for EQUIL:
   if(write_sflist_equil(diaref,shot,pRadSet))
    {
      sprintf(msg, "Error in writing sf_list EQUIL");
      log_msg(ERROR, msg);
      sf_close_write (diaref);
      return(-8);
    }
  }

  sf_close_write (diaref);
  return(0);
}




/* ........... initialise ........... */

void  init_write_cec	(void)
{
  prm_group  (GROUP, "Level-1 shot file");

  prm_define ("writediag", "CEC,CED, RMD", "CEC", 
              "\tlevel-1 diagnostic to write",
              GROUP, NULL);
  prm_define ("writeexp", "AUGD,(string)", "AUGD", 
              "\tlevel-1 experiment (repository) code",
              GROUP, NULL);

}
