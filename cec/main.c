/*
       main.c - CEC main program

       Wolfgang Suttrop,   suttrop@ipp.mpg.de

       23-Feb-1994  started as ece1.c
       25-Mar-2010  stripped, modernised
	Sept-2012  harmonic=array(N_IFGROUP) LBarrera 
	Okt 2012 Include function set_harmonic to correctly set the harmonic number to write CEC

	  
c
*/

#include <ctype.h>
#include <memory.h>
#include <setjmp.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#include "libece.h"

#include "param.h"
#include "util.h"
#include "eceradiometer.h" 
#include "cec.h"


#define LOGSTRLEN	255      /* max. line length in log file */

static  void    process_shot(int shot); 
static  char   *defaultarg[2];



/* ==================== main program ===================== */

#ifdef FORMAIN

MAIN_ () {        /* This program works in a FORTRAN environment .. */

  int    argc;    /* .. hence command line arguments .. */
  char   **argv;  /* don't come as arguments */
  getargs ("CEC", &argc, &argv);  

#else

int main (int argc, char **argv)         /* .. no longer */
  {

#endif

  defaultarg[0] = "CEC";
  defaultarg[1] = strdup("cmd");

  init_logging       ();
  init_position      ();          //Change Okt-2012, get harmonic before procesing the shot...
  init_shots	     (process_shot);
  init_raw_signals   ();
  init_calibration   ();
  
  init_downsampling  ();
  init_write_cec     ();

  if (argc>1)   prm_main (argc, argv);
  else          prm_main (2, defaultarg);

  return(0);
  }  /* main() */




/* ------  process a single shot ------- */


static void process_shot (int shot) 
{
  
  char  msg[LOGSTRLEN];
  int noff;
  
  struct RawDiagnostic *pRawDiag;
  struct RadiometerSettings RadSet;
  struct ChannelPositions ChanPos;
  struct ChannelData ChanData;
  struct SampleRateSettings SRSet;

  /* reset .. */
  set_radiometer_defaults (&RadSet);
  set_harmonic (&RadSet);

  memset(&ChanPos,  0, sizeof(ChanPos));
  memset(&ChanData, 0, sizeof(ChanData));
  memset(&SRSet,    0, sizeof(SRSet));

  /* .. greetings .. */
  log_timestamp();
  sprintf(msg, "Processing shot %d", shot);
  log_msg(INFO, msg);

  //RadSet.harmonic = 2;   /* temporary, to be removed later */
  //if ((shot==28136) || (shot==28137) || (shot==28381)) RadSet.harmonic = 3; 

  /* .. and go */
  if (!(pRawDiag = get_raw_diag_specs (shot))) 
    return;

  if (get_raw_signals_info (shot, pRawDiag, &RadSet))
    return;
  
 
  if ((shot==28136) || (shot==28137) || (shot==28381) || (shot==28773) || (shot==28775)) {
	int i;
	for (i=0; i<MAX_IFGROUP; ++i)   
    	RadSet.harmonic[i]= 3;
	} 
 
  
  calc_channel_frequencies (&RadSet, &noff);
  
  if (noff>0)
    {
//      int i;
      sprintf(msg, 
	    "%d channels disabled because they are not connected to a valid IF group", 
	      noff);
      log_msg(WARNING, msg);
//      for(i=0; i < MAX_CHAN; i++){
//	sprintf(msg, 
//	    "Channel %d, side band %c", i, RadSet.sideband[i]);
//      	log_msg(WARNING, msg);
//      }
    }



  if (read_signal_traces (shot, pRawDiag, &RadSet, 
			  &ChanData, &SRSet))
    goto cleanup2;

  if (calculate_positions (shot, &RadSet, &ChanPos, &ChanData))
    goto cleanup2;

  if (temperature_calibration (shot, &RadSet, &ChanData))
    goto cleanup2;


  if (write_cec(shot, pRawDiag, &RadSet, &ChanPos,
	        &ChanData, &SRSet))
    goto cleanup2;

  log_timestamp();
  sprintf(msg, "Shot %d successfully processed", shot);
  log_msg(INFO, msg);



cleanup2:
  free_sr_settings  (&SRSet);
  free_channel_data (&ChanData);
  //cleanup1:
  free_positions (&ChanPos);
 
} /*process_shot*/



/* end of main.c */
