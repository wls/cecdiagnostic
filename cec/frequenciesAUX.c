/* frequencies.c - obtain channel frequencies */
/* Subroutine not used: changed in util/frequencies.c*/

#include <math.h>
#include <stdlib.h>
#include <stdio.h>

#include "libece.h"
#include "util.h"
#include "param.h"
#include "eceradiometer.h"
#include "cec.h"

#define GROUP "eceparameters"


void calc_channel_frequencies(struct RadiometerSettings *rs)
{
  int i, harmonic, type;
  char msg[256], 
       *harmstr = "2";
  double m_e = ELECTRON_MASS / ELEMENTARY_CHARGE;
  float sbfact;

  prm_getval (prm_get("harmonic"), &harmstr, &type);
  harmonic = atoi(harmstr);
  sprintf(msg, 
	    "Harmonic number read:", harmonic);
 log_msg(INFO, msg);
  for (i=0; i<MAX_CHAN; ++i)
  {
    //if (rs->have[i])  //Read all the frequencies, if channel off then have[i]=0 
    //{
      int nif = rs->ifgroup[i] -1;
      if ( (nif < 0) || (nif >= MAX_IFGROUP) 
	    || rs->sideband[nif]==0 
	    || rs->waveguide[nif]==0 )
      {
	sprintf(msg, 
	    "IF channel %d not connected to a valid IF group - disabled.", i+1);
	log_msg(WARNING, msg);
	rs->have[i]=0;
	continue;
      }

      switch (rs->sideband[nif])
        {
        case 'U': case 'u': sbfact = 1; break;
        case 'N': case 'n': sbfact = 1; break;  /* 128USB, with notch filter */
        case 'L': case 'l': sbfact = -1; break;
	default: sbfact = 0;
        }

      rs->freqrf[i] = rs->freq_LO[nif] 
	              + (sbfact * rs->freqif[i]);
      rs->Btot[i] = (float) (rs->freqrf[i] * 2 * M_PI * m_e / harmonic[nif]);
    //}
  }

}  /* calc_channel_frequencies */




/* ........... initialise ........... */

void  init_eceparameters	(void)
{
  prm_group  (GROUP, "ECE parameters");

  //prm_define ("harmonic", "1,2,3,4", "2",
  //            "\tECE harmonic number (3 mixers)",
  //            GROUP, NULL);
}  /* init_eceparameters */

